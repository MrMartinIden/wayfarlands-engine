#include <gtest/gtest.h>

#include <generator/generator.h>

#include <TerrainCollisionSystem.h>

#include <glm/gtx/string_cast.hpp>

#include <iostream>

TEST(TerrainCollision, UpdateST)
{
    entt::registry registry;

    WFL::generate_entities<false>(1ul << 20, registry);

    WFL::TerrainCollisionSt terrCollision{ registry };

    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    terrCollision.update(registry);
}

TEST(TerrainCollision, UpdateMT)
{
    entt::registry registry;

    WFL::generate_entities<false>(1ul << 20, registry);

    WFL::TerrainCollisionMt terrCollision{ registry };

    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    terrCollision.update(registry);
}

TEST(TerrainCollision, SerializeAndDeserialize)
{
    entt::registry registry;

    WFL::TerrainCollisionMt terrCollision{ registry };
    auto &height_map = terrCollision.m_height_map;
    height_map.push_back(10.f);
    height_map.push_back(11.f);
    height_map.push_back(12.f);
    height_map.push_back(13.f);
    height_map.push_back(14.f);

    ASSERT_TRUE(height_map.size() == 5);

    terrCollision.m_resolution = 1;

    const auto file_name = "test_terrain_collision.bin";

    WFL::save_vector(file_name, terrCollision);

    auto [res, heights] = WFL::read_vector(file_name);

    ASSERT_TRUE(terrCollision.m_resolution == res);

    ASSERT_TRUE(height_map.size() == heights.size());
    ASSERT_TRUE(height_map[0] == heights[0]);
    ASSERT_TRUE(height_map[1] == heights[1]);
    ASSERT_TRUE(height_map[2] == heights[2]);
}
