#include <gtest/gtest.h>

#include <generator/generator.h>

#include <BroadPhaseSystem.h>
#include <DebugPairSystem.h>
#include <NarrowPhase/MPR.h>
#include <NarrowPhaseSystem.h>

#include <glm/gtx/string_cast.hpp>

#include <iostream>

void recPen(const WFL::contact_info_t& contact, FILE *out, const char *note)
{
    fprintf(out, "# %s: depth: %lf\n", note, contact.m_depth);
    fprintf(out, "# %s: dir:   [%lf %lf %lf]\n", note, contact.m_normal.x, contact.m_normal.y, contact.m_normal.z);
    fprintf(out, "# %s: pos:   [%lf %lf %lf]\n", note, contact.m_pos.x, contact.m_pos.y, contact.m_pos.z);
    fprintf(out, "#\n");
}

using vec3 = WFL::vec3;

inline constexpr auto PI = glm::pi<vec3::value_type>();

TEST(NarrowPhase, mprBoxboxAlignedX)
{
    WFL::BoxCollider box1, box2;
    bool res;

    box1.m_extent = vec3{ 1, 2, 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 2, 1, 2 } * vec3::value_type{ 0.5 };

    box1.m_position = { -5., 0., 0. };
    box2.m_position = vec3{ 0 };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };

    for (auto i = 0; i < 100; i++)
    {
        res = WFL::ccdMPRIntersect(box1, box2);
        if (i < 35 || i > 65)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 35 && i != 65)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.x += 0.1;
    }

    box1.m_extent = vec3{ 0.1, 0.2, 0.1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 0.2, 0.1, 0.2 } * vec3::value_type{ 0.5 };

    box1.m_position = { -0.5, 0., 0. };
    box2.m_position = { 0., 0., 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };
    for (auto i = 0; i < 100; i++)
    {
        res = WFL::ccdMPRIntersect(box1, box2);

        if (i < 35 || i > 65)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 35 && i != 65)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.x += 0.01;
    }

    box1.m_extent = vec3{ 1, 2, 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 2, 1, 2 } * vec3::value_type{ 0.5 };

    box1.m_position = { -5., -0.1, 0. };
    box2.m_position = { 0., 0., 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };
    for (auto i = 0; i < 100; i++)
    {
        res = WFL::ccdMPRIntersect(box1, box2);

        if (i < 35 || i > 65)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 35 && i != 65)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.x += 0.1;
    }
}

TEST(NarrowPhase, mprBoxboxAlignedY)
{
    WFL::BoxCollider box1, box2;
    int res;

    box1.m_extent = vec3{ 1, 2, 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 2, 1, 2 } * vec3::value_type{ 0.5 };

    box1.m_position = { 0., -5., 0. };
    box2.m_position = { 0., 0., 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };
    for (auto i = 0; i < 100; i++)
    {
        res = WFL::ccdMPRIntersect(box1, box2);

        if (i < 35 || i > 65)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 35 && i != 65)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.y += 0.1;
    }
}

TEST(NarrowPhase, mprBoxboxAlignedZ)
{
    WFL::BoxCollider box1, box2;
    int res;

    box1.m_extent = vec3{ 1, 2, 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 2, 1, 2 } * vec3::value_type{ 0.5 };

    box1.m_position = { 0., 0., -5.f };
    box2.m_position = { 0., 0., 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };
    for (auto i = 0; i < 100; i++)
    {
        res = WFL::ccdMPRIntersect(box1, box2);

        if (i < 35 || i > 65)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 35 && i != 65)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.z += 0.1;
    }
}

TEST(NarrowPhase, mprBoxboxRot)
{
    WFL::BoxCollider box1, box2;
    WFL::vec3 axis;
    WFL::vec3::value_type angle;

    box1.m_extent = vec3{ 1, 2, 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 2, 1, 2 } * vec3::value_type{ 0.5 };

    box1.m_position = { -5., 0.5, 0. };
    box2.m_position = { 0., 0., 0. };
    box2.m_rotation = { 0., 0., 0., 1. };
    axis = { 0., 1., 0. };
    angle = PI / 4.0;
    box1.m_rotation = glm::angleAxis(angle, axis);

    for (auto i = 0; i < 100; i++)
    {
        auto res = WFL::ccdMPRIntersect(box1, box2);

        if (i < 33 || i > 67)
        {
            ASSERT_FALSE(res);
        }
        else if (i != 33 && i != 67)
        {
            ASSERT_TRUE(res);
        }

        box1.m_position.x += 0.1;
    }

    box1.m_extent = box2.m_extent = vec3{ 1 } * vec3::value_type{ 0.5 };

    box1.m_position = { -1.01, 0., 0. };
    box2.m_position = { 0., 0., 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };

    axis = { 0., 1., 0. };
    angle = 0.f;
    for (auto i = 0; i < 30; i++)
    {
        auto res = WFL::ccdMPRIntersect(box1, box2);

        if (i != 0 && i != 10 && i != 20)
        {
            ASSERT_TRUE(res);
        }
        else
        {
            ASSERT_FALSE(res);
        }

        angle += PI / 20.;
        box1.m_rotation = glm::angleAxis(angle, axis);
    }
}

TEST(NarrowPhase, mprBoxboxPenetration)
{
    WFL::BoxCollider box1, box2;

    fprintf(stderr, "\n\n\n---- boxboxPenetration ----\n\n\n");

    box1.m_extent = vec3{ 1 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 0.5, 1, 1.5 } * vec3::value_type{ 0.5 };

    {
        box1.m_position = WFL::vec3{ 0 };
        box2.m_position = { 0.1f, 0, 0 };
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 1");
        //TOSVT();
    }

    {
        box1.m_position = { -0.3, 0.5, 1. };
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 2");
        //TOSVT();
    }


    box1.m_extent = box2.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    WFL::vec3 axis = { 0., 0., 1. };
    constexpr vec3::value_type angle = PI / 4.0;
    box1.m_rotation = glm::angleAxis(angle, axis);
    box1.m_position = { 0.1, 0., 0.1 };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 3");
        //TOSVT();
    }

    box1.m_extent = box2.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    axis = { 0., 0., 1. };
    box1.m_rotation = glm::angleAxis(angle, axis);
    box1.m_position = { -0.5, 0., 0. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 4");
        //TOSVT();
    }

    box1.m_extent = box2.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    axis = { 0., 0., 1. };
    box1.m_rotation = glm::angleAxis(angle, axis);
    box1.m_position = { -0.5, 0.5, 0. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 5");
        //TOSVT();
    }

    box1.m_extent = box2.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    box2.m_position = { 0.1, 0., 0. };

    axis = { 0., 1., 1. };
    box1.m_rotation = glm::angleAxis(angle, axis);
    box1.m_position = { -0.5, 0.1, 0.4 };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 6");
        //TOSVT();
    }

    box1.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    axis = { 0., 1., 1. };
    box1.m_rotation = glm::angleAxis(angle, axis);
    axis = { 1., 1., 1. };

    glm::quat rot = glm::angleAxis(angle, axis);
    box1.m_rotation = rot * rot;
    box1.m_position = { -0.5, 0.1, 0.4 };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 7");
        //TOSVT();
    }

    box1.m_extent = vec3{ 1.0 } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 1.0 } * vec3::value_type{ 0.5 };

    axis = { 0., 0., 1. };
    box1.m_rotation = glm::angleAxis(angle, axis);
    axis = { 1., 0., 0. };
    rot = glm::angleAxis(angle, axis);

    box1.m_rotation *= rot;
    box1.m_position = { -1.3, 0., 0. };

    box2.m_position = { 0., 0., 0. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 8");
        //TOSVT();
    }

    box1.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 0.5 } * vec3::value_type{ 0.5 };
    box1.m_position = { 0., 0., 0. };
    box2.m_position = { 0., 0.73, 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 9");
        //TOSVT();
    }

    box1.m_extent = vec3{ 1. } * vec3::value_type{ 0.5 };
    box2.m_extent = vec3{ 0.5 } * vec3::value_type{ 0.5 };

    box1.m_position = { 0., 0., 0. };
    box2.m_position = { 0.3, 0.738, 0. };
    box1.m_rotation = box2.m_rotation = { 0., 0., 0., 1. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box1, box2);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 10");
        //TOSVT();
    }
}

TEST(NarrowPhase, mprBoxcapIntersect)
{
    WFL::BoxCollider box;
    WFL::CapsuleCollider cap;
    int res;
    WFL::vec3 axis;

    box.m_extent = vec3{ 0.5, 1.f, 1.5 } * vec3::value_type{ 0.5 };

    cap.m_radius = 0.4;
    cap.m_halfHeight = 0.7;

    cap.m_position = { 0.1, 0., 0. };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    cap.m_position = { .6, 0., 0. };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    cap.m_position = { .6, 0.6, 0. };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    cap.m_position = { .6, 0.6, 0.5 };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    axis = { 0., 1., 0. };
    WFL::vec3::value_type angle = PI / 3.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0.6, 0.5 };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    axis = { 0.67, 1.1, 0.12 };
    angle = PI / 4.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    axis = { -0.1, 2.2, -1. };
    angle = PI / 5.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    axis = { 1., 1., 0. };
    angle = -PI / 4.;
    box.m_rotation = glm::angleAxis(angle, axis);
    box.m_position = { .6, 0., 0.5 };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);

    axis = { -0.1, 2.2, -1. };
    angle = PI / 5.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    axis = { 1., 1., 0. };
    angle = -PI / 4.;
    box.m_rotation = glm::angleAxis(angle, axis);
    box.m_position = { .9, 0.8, 0.5 };
    res = WFL::ccdMPRIntersect(box, cap);
    ASSERT_TRUE(res);
}

TEST(NarrowPhase, mprBoxcapPen)
{
    WFL::BoxCollider box;
    WFL::CapsuleCollider cap;
    WFL::vec3 axis;

    box.m_extent = { 0.5, 1.0, 1.5 };

    cap.m_radius = 0.4;
    cap.m_halfHeight = 0.7;
    cap.m_position = { 0.1, 0., 0. };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 1");
        //TOSVT();
    }

    cap.m_position = { .6, 0., 0. };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 2");
        //TOSVT();
    }

    cap.m_position = { .6, 0.6, 0. };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 3");
        //TOSVT();
    }

    cap.m_position = { .6, 0.6, 0.5 };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 4");
        //TOSVT();
    }

    axis = { 0., 1., 0. };
    WFL::vec3::value_type angle = PI / 3.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0.6, 0.5 };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 5");
        //TOSVT();
    }

    axis = { 0.67, 1.1, 0.12 };
    angle = PI / 4.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 6");
        //TOSVT();
    }

    axis = { -0.1, 2.2, -1. };
    angle = PI / 5.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    axis = { 1., 1., 0. };
    angle = -PI / 4.;
    box.m_rotation = glm::angleAxis(angle, axis);
    box.m_position = { .6, 0., 0.5 };

    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 7");
        //TOSVT();
    }

    axis = { -0.1, 2.2, -1. };
    angle = PI / 5.;
    cap.m_rotation = glm::angleAxis(angle, axis);
    cap.m_position = { .6, 0., 0.5 };
    axis = { 1., 1., 0. };
    angle = -PI / 4.;
    box.m_rotation = glm::angleAxis(angle, axis);
    box.m_position = { .9, 0.8, 0.5 };
    {
        auto [contact_info, res] = WFL::ccdMPRPenetration(box, cap);
        ASSERT_TRUE(res);
        recPen(contact_info, stdout, "Pen 8");
        //TOSVT();
    }
}

TEST(NarrowPhase, Update)
{
    entt::registry registry;

    using namespace WFL;

    constexpr auto count = 1u << 18;

    constexpr auto use_local = true;
    generate_entities<!use_local>(count, registry);

    BroadPhaseMt bp{ registry };
    NarrowPhaseMt np { registry };

    bp.update(registry);

    DebugPairSystem debugPair;
    auto countPair = debugPair.debug(registry);

    std::cout << "Count of pair: " << countPair << " for total entities = " << count << std::endl;

    np.update(registry);
}
