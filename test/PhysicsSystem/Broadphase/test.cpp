#include <gtest/gtest.h>

#include <generator/generator.h>

#include <BroadPhaseSystem.h>

#include "DebugPairSystem.h"

#include <cassert>

using namespace WFL;

#if 1
TEST(BroadPhase, Update)
{
    entt::registry registry;

    max_character = 8000;
    constexpr auto count = 1 << 16;

#if 1
    generate_entities<false>(count, registry);

    {
        BroadPhaseSystemNaive bp{ registry};
        bp.update(registry);
    }
#endif

    DebugPairSystem debugPair;
    auto countPair1 = debugPair.debug(registry);

    registry.clear();
    generate_entities<true>(count, registry);

    BroadPhaseSt bp { registry };
    bp.update(registry);

    auto countPair2 = debugPair.debug(registry);

    std::cout << countPair1 << std::endl;

    //ASSERT_EQ(countPair1, max_character * number_of_pairs);
    ASSERT_EQ(countPair1, countPair2);

    registry.clear();
}
#endif

#if 0
TEST(BroadPhase, Check)
{
    entt::registry registry;

    vec3 pos = vec3{ 20.0, 150.0, 20.0 };

    WFL::spawn_capsule(registry, pos, entt::null, 1.0f, 2.0f);

    pos = vec3(20.0, 148.0, 20.0);
    WFL::spawn_box(registry, pos, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });
    WFL::spawn_box(registry, pos, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });

    BroadPhaseMt bp{ registry, g_AABB };
    bp.update(registry);

    auto maxOffset = bp.m_maxOffset;

    DebugPairSystem debugPair;
    auto countPair = debugPair.debug(registry);

    ASSERT_EQ(maxOffset, (vec3{ 2.0, 3.0, 2.0 }));

    ASSERT_EQ(countPair, 2);
}
#endif

#if 0
TEST(BroadPhase, Check)
{
    entt::registry registry;

    const vec3 capsule_pos0 = vec3 { 2048.0, 2.0, 2048.0};
    WFL::spawn_capsule<true>(registry, capsule_pos0);

    const vec3 capsule_pos1 = vec3 { 2048.0, 2, 6144.0};
    WFL::spawn_capsule<true>(registry, capsule_pos1);

    const quat rot = glm::identity<quat>();
    const vec3 extent{ 50, 2, 50 };

    WFL::spawn_box(registry, capsule_pos0, rot, extent);
    WFL::spawn_box(registry, capsule_pos0, rot, extent);
    WFL::spawn_box(registry, capsule_pos1, rot, extent);
    WFL::spawn_box(registry, capsule_pos1, rot, extent);

    {
        BroadPhaseSystemNaive bp{ registry };
        bp.update(registry);
    }

    DebugPairSystem debugPair;
    auto countPair1 = debugPair.debug(registry);

    {
        registry.clear();
        WFL::spawn_capsule<false>(registry, capsule_pos0);
        WFL::spawn_capsule<false>(registry, capsule_pos1);

        WFL::spawn_static_box(registry, capsule_pos0, rot, extent);
        WFL::spawn_static_box(registry, capsule_pos0, rot, extent);
        WFL::spawn_static_box(registry, capsule_pos1, rot, extent);
        WFL::spawn_static_box(registry, capsule_pos1, rot, extent);

        BroadPhaseSt bp{ registry };

        const auto& group = bp.m_static_group;

        std::cout << "Debug: " << std::endl;

        for (auto entity : group)
        {
            const auto &offset = group.get<Offset2>(entity).m_value;
            std::cout << offset << ' ';
        }

        std::cout << std::endl;

        {
            std::cout << "Begin: " << group.get<Offset2>(group.front()).m_value << std::endl;
        }


        bp.update(registry);
    }

    auto countPair2 = debugPair.debug(registry);

    std::cout << countPair1 << std::endl;

    ASSERT_EQ(countPair1, countPair2);
}
#endif

#if 0
TEST(BroadPhase, LowerBound)
{
    entt::registry registry;

    const vec3 capsule_pos0 = vec3{ chunk_size + 0.2, 2.0, chunk_size + 0.2 };

    const quat rot = glm::identity<quat>();
    const vec3 extent{ 50, 2, 50 };

    WFL::spawn_capsule<false>(registry, capsule_pos0);

    const vec3 static_pos0 = vec3{ chunk_size - 0.5, 2.0, chunk_size - 0.5 };
    const vec3 static_pos1 = vec3{ chunk_size + 0.5, 2.0, chunk_size - 0.5 };
    const vec3 static_pos2 = vec3{ chunk_size - 0.5, 2.0, chunk_size + 0.5 };
    const vec3 static_pos3 = vec3{ chunk_size - 0.1, 2.0, chunk_size + 1.5 };

    WFL::spawn_static_box(registry, static_pos0, rot, extent);
    WFL::spawn_static_box(registry, static_pos1, rot, extent);
    WFL::spawn_static_box(registry, static_pos2, rot, extent);
    WFL::spawn_static_box(registry, static_pos3, rot, extent);

    BroadPhaseSt bp{ registry };

    ASSERT_EQ(bp.m_character_group.size(), 1);
    ASSERT_EQ(bp.m_static_group.size(), 4);

    auto character_entity = bp.m_character_group.front();
    auto &character_morton_prim = bp.m_character_group.get<MortonPrimitive>(character_entity);

    std::cout << "character_morton_prim: offset = " << character_morton_prim.m_offset << ", value = " << character_morton_prim.m_value << std::endl;

    {
        const auto &character_offset = character_morton_prim.m_offset;

        const auto &character_aabb = bp.m_character_group.get<AABB>(character_entity);
        const auto &m_maxOffset = bp.m_maxOffset;

        const auto character_morton_end = bp.m_compute_morton(character_aabb.max() + m_maxOffset, std::greater<>{});
        const auto character_morton_start = bp.m_compute_morton(character_aabb.min() - m_maxOffset, std::less<>{});

        ASSERT_TRUE(inRange(character_morton_start, character_morton_end, character_morton_prim.m_value));

        std::cout << "character_morton_range: start = " << character_morton_start << ", end = " << character_morton_end << std::endl;

        std::vector<std::pair<entt::entity, MortonPrimitive::value_type>> vec;
        vec.reserve(bp.m_static_group.size());

        const auto &static_group = bp.m_static_group;
        for (auto entity : static_group)
        {
            const auto &[static_aabb, static_morton] = static_group.get<AABB, MortonPrimitive>(entity);

            const AABB global_aabb = to_global_aabb(static_aabb, character_offset, static_morton.m_offset);

            const auto code = bp.m_compute_morton(global_aabb.center());

            vec.emplace_back(entity, code);
        }

        std::sort(vec.begin(), vec.end(), [](auto &lhs, auto &rhs) {
            return lhs.second < rhs.second;
        });

        std::cout << "Iteration of the vector:" << std::endl;
        for (auto &[entity, code] : vec)
        {
            const auto &static_morton = static_group.get<MortonPrimitive>(entity);

            std::cout << "entity = " << entt::to_integral(entity)
                      << ", offset = " << static_morton.m_offset
                      << ", local_code = " << static_morton.m_value
                      << ", global_code = " << code << std::endl;

            auto local_offset = Offset2::to_offset(static_morton.m_value);
            auto global_offset = Offset2::to_offset(static_morton.m_offset);

            uint32_t offset_x = (global_offset[0] + 1) * half_chunk_size + local_offset[0];
            uint32_t offset_z = (global_offset[1] + 1) * half_chunk_size + local_offset[1];

            ASSERT_TRUE(offset_x < std::numeric_limits<uint32_t>::max());
            ASSERT_TRUE(offset_z < std::numeric_limits<uint32_t>::max());

            std::cout << "Generated morton code = " << offset_x << ' ' << offset_z << std::endl;
        }

        auto lower = std::lower_bound(vec.rbegin(), vec.rend(), character_morton_prim.m_value, [](auto &pair, uint32_t limit) {
            const auto &code = pair.second;
            return code < limit;
        });

        std::cout << "lower = " << ((lower != vec.rend()) ? lower->second : (lower - 1)->second) << std::endl;
    }

    auto it = bp.lower_bound(character_morton_prim);
    auto &static_morton_prim = bp.m_static_group.get<MortonPrimitive>(*it);

    std::cout << "Found static_morton_prim = " << static_morton_prim.m_offset << ' ' << static_morton_prim.m_value << std::endl;
}
#endif

#if 0
TEST(Broadphase, OffsetFeature)
{
    entt::registry registry;

    constexpr auto pos1 = vec3 { chunk_size + 1, 10, chunk_size + 1 };
    constexpr auto pos2 = vec3 { chunk_size - 1, 10, chunk_size - 1 };
    constexpr quat rotation = glm::identity<glm::quat>();

    vec3 pos = vec3{ chunk_size - 1, 10.0, chunk_size - 1 };
    WFL::spawn_capsule<false>(registry, pos, entt::null, 1.0f, 2.0f);

    constexpr vec3 extent = vec3{ 2, 3, 2 };

    spawn_static_box(registry, pos1, rotation, extent);
    spawn_static_box(registry, pos2, rotation, extent);

    BroadPhaseMt bp{ registry };
    bp.update(registry);

    DebugPairSystem debugPair;
    auto count_pair = debugPair.debug(registry);
    std::cout << "count_pair = " << count_pair << '\n';
}
#endif

#if 1
TEST(Broadphase, CheckOverlap)
{
    entt::registry registry;

    constexpr auto capsule_position = vec3{ 60, 30, 60 };

    auto [local_pos, offset] = to_local(capsule_position);
    WFL::spawn_capsule(registry, Offset2::encode(offset[0], offset[1]), local_pos);

    const vec3 half_size{ 50, 1, 50 };
    const quat rotation = glm::identity<glm::quat>();
    constexpr auto static_position = vec3{ 60, 30, 60 };

    {
        auto [local_pos, offset] = WFL::to_local(static_position);

        WFL::spawn_static_box(registry, Offset2::encode(offset[0], offset[1]), local_pos, rotation, half_size);
    }

    BroadPhaseSt bp{ registry };
    bp.update(registry);

    DebugPairSystem debugPair;
    auto count_pair = debugPair.debug(registry);

    ASSERT_TRUE(glm::all(glm::equal(half_size, bp.m_maxOffset)));
    ASSERT_EQ(count_pair, 1);
}
#endif

#if 1
TEST(Broadphase, AabbOverlap)
{
    auto capsule_aabb_min = vec3{ 117.821518, 26.984440, 91.233269 };
    auto capsule_aabb_max = vec3{ 118.821518, 28.984440, 92.233269 };

    AABB capsule_aabb;
    capsule_aabb.m_half_size = (capsule_aabb_max - capsule_aabb_min) * vec3::value_type{ 0.5 };
    capsule_aabb.m_position = capsule_aabb_max - capsule_aabb.m_half_size;

    auto terrain_aabb_min = vec3{ 117.000000, 29.524460, 90.500000 };
    auto terrain_aabb_max = vec3{ 119.548538, 32.567383, 93.097076 };

    AABB terrain_aabb;
    terrain_aabb.m_half_size = (terrain_aabb_max - terrain_aabb_min) * vec3::value_type{ 0.5 };
    terrain_aabb.m_position = terrain_aabb_max - capsule_aabb.m_half_size;

    auto overlaps = [](const AABB &a, const AABB &b) {
        std::array<bool, 2> result = { glm::all(glm::greaterThanEqual(a.max(), b.min())),
            glm::all(glm::lessThanEqual(a.min(), b.max())) };

        return result;
    };

    auto result = overlaps(capsule_aabb, terrain_aabb);

    //std::cout << glm::all(glm::greaterThanEqual(a.max(), b.min())) << ' ' << result[1] << std::endl;
    //std::cout << result[0] << ' ' << result[1] << std::endl;

    ASSERT_TRUE(!result[0]);
    ASSERT_TRUE(result[1]);
}
#endif
