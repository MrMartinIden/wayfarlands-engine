#include <glm/gtx/string_cast.hpp>

#include <filesystem>
#include <fstream>
#include <queue>
#include <thread>

#include <generator/generator.h>

#include "common.hpp"

#include "CheckCapsuleOnPlatform.hpp"
#include "MovementSystem.h"
#include "PhysicsSystem.h"
#include "Timer.h"
#include "UpdateAabbSystem.hpp"
#include "ring_array.hpp"
#include "utility.hpp"

namespace WFL
{

    using tuple = std::tuple<MovementSystem,
            UpdateAabbSystemMt,
            CheckCapsuleOnPlatformMt,
            PhysicsSystem>;

    struct Server
    {
        Server(entt::registry &registry) :
                m_server(create_server(registry)), m_systems({ MovementSystem{}, UpdateAabbSystemMt{}, CheckCapsuleOnPlatformMt{}, PhysicsSystem{ registry } })
        {
            game_adapter.m_server = &m_server;
        }

        ~Server()
        {
            ShutdownYojimbo();
        }

        void stop()
        {
            m_server.Stop();
        }

        void start()
        {

            // start the server
            m_server.Start(yojimbo::MaxClients);

            // print the port we got in case we used port 0
            char buffer[256];
            m_server.GetAddress().ToString(buffer, sizeof(buffer));
            std::cout << "Server address is " << buffer << std::endl;
        }

        void ProcessMessages()
        {
            for (int i = 0; i < yojimbo::MaxClients; ++i)
            {
                if (m_server.IsClientConnected(i))
                {
                    if (!m_server.CanSendMessage(i, yojimbo::UNRELIABLE_UNORDERED_CHANNEL))
                    {
                        continue;
                    }

                    {

                        auto entity = array_entities[i].m_entity;

                        auto capsule_view = game_adapter.m_registry->view<CapsuleCollider>();
                        const auto &capsule = capsule_view.get<CapsuleCollider>(entity);

                        auto *unreliable_message = (yojimbo::MessageGhostPosition *)m_server.CreateMessage(i, yojimbo::MESSAGE_GHOST_POSITION);
                        if (unreliable_message != nullptr)
                        {
                            unreliable_message->m_position = { capsule.m_position[0],
                                capsule.m_position[1],
                                capsule.m_position[2] };
                            printf("Sending ghost position = %f, %f, %f\n",
                                    capsule.m_position[0],
                                    capsule.m_position[1],
                                    capsule.m_position[2]);
                            m_server.SendMessage(i, yojimbo::UNRELIABLE_UNORDERED_CHANNEL, unreliable_message);
                        }
                    }

                    while (true)
                    {
                        [[maybe_unused]] static auto num_debug = 0ul;

                        yojimbo::Message *unreliable_message = m_server.ReceiveMessage(i, yojimbo::UNRELIABLE_UNORDERED_CHANNEL);
                        if (unreliable_message == nullptr)
                        {
                            num_debug = 0;
                            break;
                        }

                        switch (unreliable_message->GetType())
                        {

                            case yojimbo::MESSAGE_INPUT:
                            {
                                yojimbo::MessageInputCommand *inputCommand = (yojimbo::MessageInputCommand *)unreliable_message;
#if 1
                                auto ent = array_entities[i].m_entity;
                                auto &direction = game_adapter.m_registry->get<DirectionMovement>(ent);

                                direction = { inputCommand->m_value[0], 0, inputCommand->m_value[1] };

                                if constexpr (MSG_INP_CMD_DEBUG)
                                {
                                    auto sequence = inputCommand->m_sequence;

                                    printf("Received direction = %f, %f, sequence = %lu\n",
                                            direction.x,
                                            direction.z,
                                            sequence);
                                }
                                else
                                {
                                    printf("Received direction = %f, %f\n",
                                            direction.x,
                                            direction.z);
                                }

#else
                                auto sequence = inputCommand->m_sequence;

                                printf("Received direction = %f, %f, sequence = %lu\n",
                                        inputCommand->m_value[0],
                                        inputCommand->m_value[1],
                                        sequence);

                                //printf("%lu\n", num_debug++);
                                m_received_input_commands[i].push(std::make_tuple(inputCommand->m_value[0], inputCommand->m_value[1], sequence));
#endif
                                break;
                            }
                            case yojimbo::MESSAGE_POSITION_AFTER_STOP:
                            {
                                auto *message_position = (yojimbo::MessagePositionAfterStop *)unreliable_message;

                                const auto &pos = message_position->m_position;

                                WFL::vec3 received_position{ pos[0], pos[1], pos[2] };

                                auto ent = array_entities[i].m_entity;
                                auto &cap_position = game_adapter.m_registry->get<CapsuleCollider>(ent).m_position;

                                const auto squared_distance = glm::distance2(cap_position, received_position);

                                if (std::isless(squared_distance, limit_squared_distance))
                                {
                                    cap_position = received_position;
                                }

                                break;
                            }
                            default:
                                break;
                        }

                        m_server.ReleaseMessage(i, unreliable_message);
                    }
                }
            }
        }

        void update_input()
        {
#if 0
            for (uint32_t i{ 0 }; i < m_received_input_commands.size(); ++i)
            {
                auto &queue = m_received_input_commands[i];
                if (!queue.empty())
                {
                    auto ent = array_entities[i];
                    auto &direction = game_adapter.m_registry->get<Direction>(ent);

                    static size_t last_sequence = -1;
                    const auto [forward, left, seq] = queue.front();

                    if (last_sequence != static_cast<size_t>(-1))
                    {
                        last_sequence = seq;
                        direction = { forward, 0, left };
                        queue.pop();
                    }
                    else if (seq - last_sequence == 1)
                    {
                        last_sequence = seq;
                        direction = { forward, 0, left };
                        queue.pop();
                    }
                    else
                    {
                        last_sequence = seq;
                    }

                    if (forward != 0 || left != 0)
                    {
                        std::printf("Apply input %f %f %u\n", forward, left, seq);
                    }
                }
            }
#endif
        }

        void move_players()
        {
#if 0
            const auto direction_view = game_adapter.m_registry->view<Direction>();

            for (auto entity : direction_view)
            {
                if (entity != array_entities[0])
                {
                    auto &direction = direction_view.get<Direction>(entity);
                    direction.x = 1;
                }
            }
#endif
        }

        void send_to_clients()
        {
            for (size_t index_client{ 0 }; index_client < yojimbo::MaxClients; ++index_client)
            {
                if (m_server.IsClientConnected(index_client))
                {
                    if (!m_server.CanSendMessage(index_client, yojimbo::UNRELIABLE_UNORDERED_CHANNEL))
                    {
                        continue;
                    }

                    if (const auto entity = array_entities[index_client].m_entity; entity != entt::null)
                    {

                        const auto capsule_view = game_adapter.m_registry->view<CapsuleCollider>();

#if ENABLE_SENDING_GHOST
                        const auto &capsule = capsule_view.get<CapsuleCollider>(entity);

                        {
                            auto *unreliable_message = (yojimbo::MessageGhostPosition *)m_server.CreateMessage(index_client, yojimbo::MESSAGE_GHOST_POSITION);
                            if (unreliable_message != nullptr)
                            {
                                unreliable_message->m_position = { capsule.m_position[0],
                                    capsule.m_position[1],
                                    capsule.m_position[2] };
                                printf("Sending ghost position = %f, %f, %f\n",
                                        capsule.m_position[0],
                                        capsule.m_position[1],
                                        capsule.m_position[2]);
                                m_server.SendMessage(index_client, yojimbo::UNRELIABLE_UNORDERED_CHANNEL, unreliable_message);
                            }
                        }
#endif

                        {

                            for (auto ent : capsule_view)
                            {
                                if (ent != entity)
                                {
                                    auto *unreliable_message = (yojimbo::PositionPlayersMessage *)m_server.CreateMessage(index_client, yojimbo::MESSAGE_POSITION_PLAYERS);
                                    if (unreliable_message != nullptr)
                                    {
                                        const auto &cap = capsule_view.get<CapsuleCollider>(ent);
                                        auto &pos = unreliable_message->m_position;
                                        pos[0] = cap.m_position[0];
                                        pos[1] = cap.m_position[1];
                                        pos[2] = cap.m_position[2];

                                        printf("Sending player position = %f, %f, %f\n",
                                                cap.m_position[0],
                                                cap.m_position[1],
                                                cap.m_position[2]);

                                        m_server.SendMessage(index_client, yojimbo::UNRELIABLE_UNORDERED_CHANNEL, unreliable_message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        void update(entt::registry &reg)
        {
#if 0
            {
                auto now = std::chrono::high_resolution_clock::now();
                auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_start).count();

                std::cout << "Send packet " << ms << " ms\n";

                m_start = now;
            }
#endif

            if (!m_server.IsRunning())
            {
                return;
            }

            // update server and process messages
            m_server.SendPackets();

            m_server.ReceivePackets();

            ProcessMessages();

            update_input();

            move_players();

            send_to_clients();

            {
#if 0
            const auto view = reg.view<TickNumber>();
            if (view.size() == 1) {
                auto& tickNumber = view.get<TickNumber>(view.front());
                if (tickNumber.m_value != TickNumber::null) {
#endif
                std::apply([&](auto &&...args) { (args.update(reg, fixed_dt), ...); }, m_systems);
#if 0
                }
            }
#endif
            }

            m_server.AdvanceTime(m_time);
        }

        PhysicsSystem &get_physics_system()
        {
            return std::get<PhysicsSystem>(m_systems);
        }

        yojimbo::Server m_server;

        //std::chrono::time_point<std::chrono::system_clock> m_start = std::chrono::high_resolution_clock::now();

#if 0
        using t = std::tuple<float, float, uint32_t>;

        std::array<ring_array<t, 10>, yojimbo::MaxClients>
                m_received_input_commands;
#endif

        tuple m_systems;

        double m_time{};

    private:
        static yojimbo::Server create_server(entt::registry &registry)
        {
            if (!InitializeYojimbo())
            {
                printf("error: failed to initialize Yojimbo!\n");
                assert(0);
            }

            yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);

            srand((unsigned int)time(nullptr));

            static constexpr uint8_t privateKey[yojimbo::KeyBytes]{};

            auto config = yojimbo::get_config();

            game_adapter.m_registry = &registry;

            return { yojimbo::GetDefaultAllocator(), privateKey, yojimbo::Address{ "127.0.0.1", ServerPort }, config, game_adapter, 0.0 };
        }
    };
} // namespace WFL

int main(int argc, char **argv)
{

    entt::registry registry;

#if 0
    std::cout << "Number of concurrent threads = " << std::thread::hardware_concurrency() << '\n';

    using namespace WFL;

    size_t max_static = 1ul << 20;

    if (argc == 2)
    {
        max_character = atoi(argv[1]);
    }
    else if (argc == 3)
    {
        max_character = atoi(argv[1]);
        max_static = atoi(argv[2]);
    }

    std::cout << "Number of maximum characters = " << max_character << '\n';
    std::cout << "Number of maximum statics = " << max_static << '\n';

    generate_entities(max_static + max_character, registry);

    std::cout << "Entities is generated.\n";
#endif

    {
        //std::vector<entt::entity> entities(max_character);

        /*
        {
            registry.create(entities.begin(), entities.end());

            debug_character = entities[100];

            spawn_capsules(registry, entities);
        }

        {
            entities.resize(max_static);
            registry.create(entities.begin(), entities.end());

            spawn_statics(registry, entities);
        }
        */
    }

    WFL::Server server { registry };

    {
        auto &terrCollision = server.get_physics_system().m_terrainCollision;

        namespace fs = std::filesystem;
        const fs::path workdir = fs::current_path();

        std::string path = std::string{ workdir } + "/terrainCollision.bin";

        std::cout << path << '\n';

        if (!fs::exists(path))
        {
            std::cout << "File of heightmap is not exist.\n";
            return 0;
        }

        std::cout << "File of heightmap is exist.\n";

        auto [res, vec] = WFL::read_vector(path);

        terrCollision.m_height_map = std::move(vec);
        terrCollision.m_resolution = res;

        std::cout << "Height map size = " << terrCollision.m_height_map.size() << '\n';

        assert(terrCollision.m_height_map.size() == 67108864ul);
    }

    std::cout << "Wait for event...\n";

    //assert(!server.m_movementSystem.m_group.empty());

    //std::cout << "Movement system group = " << server.m_movementSystem.m_group.size() << '\n';

    /*
    auto view = registry.view<Velocity, MovementComponent, CapsuleCollider, AABB>();
    std::cout << "Movement system view = " << view.size() << '\n';
    std::cout << "Movement system view1 =  " << registry.view<Velocity>().size() << '\n';
*/
    //std::cout << "Broadphase system = " << server.m_physicsSystem.m_broadPhase.m_group.size() << '\n';
    //std::cout << "Broadphase system pairs =" << server.m_physicsSystem.m_broadPhase.m_groupPair.size() << '\n';

#if 0
    constexpr size_t max_tick = 1000;

    std::vector<int long> vec;
    vec.reserve(max_tick);

    for (size_t i{ 0 }; i < max_tick; ++i)
    {
        Timer timer{};
        server.update(registry);
        vec.push_back(timer.elapsed());

        //std::cout << glm::to_string(registry.get<CapsuleCollider>(debug_character).m_position) << '\n';
    }

    std::cout << "Max = " << *std::max_element(vec.begin(), vec.end()) << " microseconds \n";
    std::cout << "Min = " << *std::min_element(vec.begin(), vec.end()) << " microseconds \n";
#else

    server.start();
    //server.run();

    while (true)
    {
        double currentTime = yojimbo_time();
        if (server.m_time <= currentTime)
        {
            server.update(registry);
            server.m_time += WFL::fixed_dt;
        }
        else
        {
            yojimbo_sleep(server.m_time - currentTime);
        }
    }

    server.stop();

#endif
}
