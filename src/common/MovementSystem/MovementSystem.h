// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <system.hpp>

#include <capsule_collider.hpp>
#include <components.hpp>
#include <constraint.hpp>
#include <direction.hpp>
#include <speed.hpp>
#include <velocity.hpp>

#include <iostream>

#include "glm/gtx/string_cast.hpp"

namespace WFL
{

#define DEBUG_PLATFORM 0
#define DEBUG_MOVEMENT 0

    struct MovementSystem : BaseSystem<MovementSystem>
    {
        void updatePlatform(entt::registry &registry, float dt)
        {
            auto view = registry.view<DirectionMovement, VelocityComponent, SpeedComponent, StaticCollider, PlatformComponent>();

            for (auto entity : view)
            {
                auto direction = glm::normalize(view.get<DirectionMovement>(entity));

                if (glm::any(glm::isnan(direction)))
                {
                    direction = {};
                }

                SpeedComponent &speed = view.get<SpeedComponent>(entity);
                StaticCollider &collider = view.get<StaticCollider>(entity);

                constexpr auto top_speed = 3.0f;

                for (int i{ 0 }; i < 3; ++i)
                {
                    if (direction[i] != 0)
                    {
                        speed[i] = top_speed;
                    }
                    else
                    {
                        speed[i] = 0;
                    }
                }

                auto &vel = view.get<VelocityComponent>(entity);
                vel = static_cast<vec3>(speed) * vec3::value_type{ dt } * direction;

#if DEBUG_PLATFORM
                    std::cout << "Platform" << '\n';
                    std::cout << "Direction = " << glm::to_string<vec3>(direction) << '\n';
                    std::cout << "Speed = " << glm::to_string<vec3>(speed) << '\n';
                    std::cout << "Velocity = " << glm::to_string<vec3>(vel) << '\n';
#endif

                    std::visit([&](auto &arg) {
                        arg.m_position += static_cast<vec3>(vel);
                    },
                            collider);
            }
        }

        void updateCapsuleWithConstraint(entt::registry &registry, float dt)
        {
            auto view = registry.view<DirectionMovement, VelocityComponent, SpeedComponent, MovementComponent, CapsuleCollider, ConstraintComponent>();

            for (auto entity : view)
            {
                vec3 direction = glm::normalize(view.get<DirectionMovement>(entity));

                if (glm::any(glm::isnan(direction)))
                {
                    direction = vec3{ 0 };
                }

                SpeedComponent &speed = view.get<SpeedComponent>(entity);
                MovementComponent &movement = view.get<MovementComponent>(entity);
                CapsuleCollider &capsule = view.get<CapsuleCollider>(entity);

                //constexpr float friction_factor = 0.2f;

                for (int i{ 0 }; i < 3; i += 2)
                {
                    if (direction[i] != 0)
                    {
                        speed[i] = player_max_speed;
                    }
                    else
                    {
                        speed[i] = 0;
                    }
                }

                {
                    auto ent = view.get<ConstraintComponent>(entity).m_value;
                    VelocityComponent vel = registry.get<VelocityComponent>(ent);
                    capsule.m_position += static_cast<vec3>(vel);
                }

                if (movement.m_jump) {
                    speed.y += 0.4;

                    registry.remove<ConstraintComponent>(entity);
                    movement.m_jump = 0;
                }

                //speed.y += gravity * dt;
                //speed.y += gravity * dt * dt;

                auto& vel = view.get<VelocityComponent>(entity);
                //vel = vec3{ speed.x * dt * direction.x, 0, speed.z * dt * direction.z };
                vel = speed * dt * direction;

#if DEBUG_MOVEMENT
                std::cout << "Capsule" << '\n';
                std::cout << "Direction = " << glm::to_string<vec3>(direction) << '\n';
                std::cout << "Speed = " << glm::to_string<vec3>(speed) << '\n';
                std::cout << "Velocity = " << glm::to_string<vec3>(vel) << '\n';
#endif

                capsule.m_position += static_cast<vec3>(vel);
            }
        }

        void updateCapsule(entt::registry &registry, float dt)
        {

            auto view = registry.view<DirectionMovement, VelocityComponent, SpeedComponent, MovementComponent, CapsuleCollider>(entt::exclude<ConstraintComponent>);

            for (auto entity : view)
            {
                

                const auto direction = [&view, &entity]() -> vec3 {
                    vec3 direction = glm::normalize(view.get<DirectionMovement>(entity));

                    if (glm::any(glm::isnan(direction)))
                    {
                        direction = vec3{ 0 };
                    }

                    return direction;
                }();

                SpeedComponent &speed = view.get<SpeedComponent>(entity);
                MovementComponent &movement = view.get<MovementComponent>(entity);
                CapsuleCollider &capsule = view.get<CapsuleCollider>(entity);

                //constexpr float friction_factor = 0.2f;

                if (speed.y == 0)
                {
                    for (int i{ 0 }; i < 3; i += 2)
                    {
                        if (direction[i] != 0)
                        {
                            speed[i] += 1.0f;
                        }
                        else
                        {
                            speed[i] = 0;
                        }
                    }

                    if (movement.m_jump) {
                        speed.y += 1.0;
                        movement.m_jump = 0;
                    }
                }
                else
                {
                    speed.x *= 0.98;
                    speed.z *= 0.98;
                }

                {
                    if (speed.x > player_max_speed)
                    {
                        speed.x = player_max_speed;
                    }

                    if (speed.z > player_max_speed)
                    {
                        speed.z = player_max_speed;
                    }
                }

                //speed.y += gravity * dt;
                speed.y += gravity * dt * dt;

                auto &vel = view.get<VelocityComponent>(entity);
                vel = vec3{ speed.x * dt * direction.x, speed.y, speed.z * dt * direction.z };

#if 0
                //DEBUG_MOVEMENT
                //#ifdef IS_SERVER
                std::cout << "Capsule" << '\n';
                std::cout << "Direction = " << glm::to_string<vec3>(direction) << '\n';
                std::cout << "Speed = " << glm::to_string<vec3>(speed) << '\n';
                std::cout << "Velocity = " << glm::to_string<vec3>(vel) << '\n';
                std::cout << "Position = " << glm::to_string<vec3>(capsule.m_position) << '\n';
#endif

                capsule.m_position += static_cast<vec3>(vel);
            }
        }

        void update(entt::registry &registry, float dt)
        {
            updatePlatform(registry, dt);
            updateCapsuleWithConstraint(registry, dt);
            updateCapsule(registry, dt);
        }
    };
#undef DEBUG_MOVEMENT

} // namespace WFL
