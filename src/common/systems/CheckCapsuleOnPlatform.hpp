#pragma once

#include "system.hpp"

#include <components.hpp>
#include <constraint.hpp>

#include <execution>

namespace WFL
{
    extern std::mutex s_mutex;

    template <bool is_multithreaded>
    struct CheckCapsuleOnPlatform : BaseSystem<CheckCapsuleOnPlatform<is_multithreaded>>
    {

        void update(entt::registry &registry, [[maybe_unused]] float dt = 0)
        {
            const auto view_aabb = registry.view<AABB>();
            auto constraint_view = registry.view<AABB, ConstraintComponent>();

            auto func = [&](const entt::entity entity) {
                const AABB &capsuleAABB = constraint_view.get<AABB>(entity);
                const ConstraintComponent &constraint = constraint_view.get<ConstraintComponent>(entity);

                const auto &constraint_entity = constraint.m_value;

                auto &aabb = view_aabb.get<AABB>(constraint_entity);

                if (!aabb.overlaps(capsuleAABB))
                {
                    if constexpr (is_multithreaded)
                    {
                        std::lock_guard<std::mutex> guard{ s_mutex };
                        registry.remove<ConstraintComponent>(entity);
                    }
                    else
                    {
                        registry.remove<ConstraintComponent>(entity);
                    }
                }
            };

            if constexpr (is_multithreaded)
            {
                std::for_each(std::execution::par, constraint_view.begin(), constraint_view.end(), func);
            }
            else
            {
                std::for_each(std::execution::seq, constraint_view.begin(), constraint_view.end(), func);
            }
        }
    };

    using CheckCapsuleOnPlatformMt = CheckCapsuleOnPlatform<true>;
    using CheckCapsuleOnPlatformSt = CheckCapsuleOnPlatform<false>;

} // namespace WFL
