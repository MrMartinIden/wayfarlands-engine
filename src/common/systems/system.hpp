#pragma once

#include <entt/entity/registry.hpp>

namespace WFL
{

    template <typename T>
    struct BaseSystem
    {
        BaseSystem() = default;
        BaseSystem(BaseSystem &&) = default;
        BaseSystem(const BaseSystem &) = delete;
        BaseSystem operator=(const BaseSystem &) = delete;

        void update(entt::registry &registry, float dt)
        {
            impl()->update(registry, dt);
        }

        decltype(auto) impl()
        {
            return static_cast<T *>(this);
        }
    };
} // namespace WFL
