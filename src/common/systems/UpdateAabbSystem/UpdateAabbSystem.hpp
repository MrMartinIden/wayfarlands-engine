#pragma once

#include <systems/system.hpp>

#include <capsule_collider.hpp>
#include <components.hpp>
#include <offset.hpp>

#include <execution>

namespace WFL
{
    template <bool is_multithreaded = true>
    struct UpdateAabbSystem : BaseSystem<UpdateAabbSystem<is_multithreaded>>
    {
        void update(entt::registry &registry, [[maybe_unused]] float dt = 0)
        {

            auto func = [&](auto exec) {
                {
                    auto capsule_view = registry.view<CapsuleCollider, const Offset2, AABB>();

                    std::for_each(exec, capsule_view.begin(), capsule_view.end(), [&](auto entity) {
                        CapsuleCollider capsule = capsule_view.get<CapsuleCollider>(entity);

                        /*
                        const Offset2 &offset = capsule_view.get<const Offset2>(entity);

                        const auto offsets = Offset2::decode(offset.m_value);

                        capsule.m_position += vec3{ offsets[0], 0, offsets[1] } * chunk_size;
                        */

                        auto &aabb = capsule_view.get<AABB>(entity);
                        aabb = capsule.getAABB();
                    });
                }

                {
                    auto static_view = registry.view<const StaticCollider, AABB, PlatformComponent>();

                    std::for_each(exec, static_view.begin(), static_view.end(), [&](auto entity) {
                        const StaticCollider &collider = static_view.get<const StaticCollider>(entity);
                        AABB &aabb = static_view.get<AABB>(entity);

                        std::visit([&](auto &arg) {
                            aabb = arg.getAABB();
                        },
                                collider);
                    });
                }
            };

            if constexpr (is_multithreaded)
            {
                func(std::execution::par);
            }
            else
            {
                func(std::execution::seq);
            }
        }
    };

    using UpdateAabbSystemMt = UpdateAabbSystem<true>;
    using UpdateAabbSystemSt = UpdateAabbSystem<false>;

} // namespace WFL
