// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#ifdef DEBUG
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#endif

namespace WFL
{
#ifdef DEBUG
    extern inline void debug(std::string str) { std::cout << str; }
#else
    template <class T>
    extern inline void debug(T)
    {
    }
#endif
} // namespace WFL
