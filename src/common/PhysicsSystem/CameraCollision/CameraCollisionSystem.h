#pragma once

#include <components.hpp>
#include <offset.hpp>
#include <ray.hpp>
#include <terrain.hpp>
#include <utility.hpp>

#include <PhysicsSystem.h>

namespace WFL
{
    struct CameraCollisionSystem final
    {
        void update([[maybe_unused]] entt::registry &registry)
        {

            if (m_physics_system == nullptr)
            {
                return;
            }

            auto cam_view = registry.view<Ray, Offset2, MortonPrimitive, DynamicTerrainComponent, PairComponent>();

            if (cam_view.size_hint() != 1)
            {
                return;
            }

            const auto cam_entity = cam_view.front();

#if 1
            //broad phase
            {

                const auto offset = cam_view.get<Offset2>(cam_entity).m_value;

                auto &ray = cam_view.get<Ray>(cam_entity);

                MortonPrimitive &primCode = cam_view.get<MortonPrimitive>(cam_entity);

                primCode = compute_global_code::generate(offset, ray.mediana());

                auto pair_view = registry.view<PairComponent>();

                {
                    auto &broad_phase = m_physics_system->m_broadPhase;

                    const AABB character_aabb = ray.getAABB();
                    const auto character_aabb_min = character_aabb.min();
                    const auto character_aabb_max = character_aabb.max();

                    //std::cout << '\t' << "character_aabb = " << character_aabb << '\n';

                    const auto &character_offset = cam_view.get<Offset2>(cam_entity).m_value;

                    auto temp_global_offset = Offset2::decode(character_offset);
                    const vec2 character_global_offset = { temp_global_offset[0], temp_global_offset[1] };

                    const auto character_range_global_code = compute_global_code::generate(character_global_offset,
                            character_aabb_min - broad_phase.m_maxOffset,
                            character_aabb_max + broad_phase.m_maxOffset);

                    assert(character_range_global_code[0] <= character_range_global_code[1]);

                    //debug_character(cam_entity);

                    const auto &static_group = broad_phase.m_static_group;

                    if (!static_group.empty())
                    {
                        AABB static_global_aabb{};
                        auto &pair = pair_view.get<PairComponent>(cam_entity);

                        auto &morton_prim = cam_view.get<MortonPrimitive>(cam_entity);
                        auto it_find = broad_phase.lower_bound(morton_prim);

                        //debug_lower_bound(*it_find);

                        auto find_pair = [&](auto start, auto end) -> void {
                            static_assert(std::is_same_v<decltype(start), decltype(end)>, "");

                            for (auto it = start; it != end; ++it)
                            {
                                const entt::entity static_entity = *it;

                                const auto &static_morton = static_group.get<MortonPrimitive>(static_entity);

                                if (inRange(character_range_global_code[0], character_range_global_code[1], static_morton.m_value))
                                {
                                    const auto &static_aabb = static_group.get<AABB>(static_entity);
#ifdef DEBUG_BROADPHASE
                                    std::cout << '\t' << "static_aabb = " << static_aabb << '\n';
#endif

                                    const auto &static_offset = static_group.get<Offset2>(static_entity).m_value;
                                    static_global_aabb = to_global_aabb(static_aabb, character_offset, static_offset);

                                    //std::cout << '\t' << "static_aabb = " << static_aabb << '\n';

                                    broad_phase.check_overlap(character_aabb_min, character_aabb_max, static_global_aabb, pair, static_entity);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        };

                        find_pair(std::make_reverse_iterator(it_find),
                                std::make_reverse_iterator(static_group.begin()));

                        find_pair(it_find, static_group.end());
                    }
                }
            }

            //terrain collision
            {

                auto &terrain = m_physics_system->m_terrainCollision;

                const auto &position = terrain.m_position;
                const auto &resolution = terrain.m_resolution;
                const auto &height_map = terrain.m_height_map;

                if (!height_map.empty())
                {

                    const auto &ray = cam_view.get<Ray>(cam_entity);
                    const auto capsule_offset = cam_view.get<Offset2>(cam_entity).m_value;

                    const vec3 global_position = to_global_pos(position, capsule_offset, 0);

                    //const vec3 rel_position = to_global(capsule.m_position, capsule_offset) - global_position;
                    const vec3 rel_position = ray.m_origin - global_position;

                    const int column0 = glm::floor(rel_position.x / g_chunk_size);
                    const int row0 = glm::floor(rel_position.z / g_chunk_size);

                    std::vector<vec3> vertices;
                    vertices.reserve(16);

                    auto add_vertices = [&resolution, &vertices, &height_map](int p_column, int p_row) {
                        const vec2 chunk_coord = vec2{ p_column, p_row } * g_chunk_size;

                        const int index = p_column + p_row * resolution;
                        assert(index >= 0);

                        vertices.emplace_back(vec3{ chunk_coord.x, height_map[index], chunk_coord.y });

                        vertices.emplace_back(vec3{ chunk_coord.x + g_chunk_size, height_map[index + 1], chunk_coord.y });
                        vertices.emplace_back(vec3{ chunk_coord.x, height_map[index + resolution], chunk_coord.y + g_chunk_size });
                        vertices.emplace_back(vec3{ chunk_coord.x + g_chunk_size, height_map[index + resolution + 1], chunk_coord.y + g_chunk_size });
                    };

                    {
                        const vec3 rel_position = ray.get_far_point() - global_position;

                        const int column1 = glm::floor(rel_position.x / g_chunk_size);
                        const int row1 = glm::floor(rel_position.z / g_chunk_size);

                        auto column_offset = column1 - column0;
                        [[maybe_unused]] auto column_sign = glm::sign(column_offset);

                        auto row_offset = row1 - row0;
                        [[maybe_unused]] auto row_sign = glm::sign(row_offset);

                        if (column0 != column1 && row0 != row1)
                        {
                            for (auto column = column0; column != column1 + column_sign; column += column_sign)
                            {
                                for (auto row = row0; row != row1 + row_sign; row += row_sign)
                                {
                                    add_vertices(column, row);
                                }
                            }
                        }
                    }

#if 0
                    {
                        static bool is_hint = false;
                        if (!is_hint)
                        {
                            std::cout << "position: " << glm::to_string(m_position) << '\n'
                                      << "heights: " << m_height_map[index] << ' '
                                      << m_height_map[index + 1] << ' '
                                      << m_height_map[index + m_resolution] << ' '
                                      << m_height_map[index + m_resolution + 1] << '\n'
                                      << "position of capsule: " << glm::to_string(capsule.m_position) << '\n'
                                      << "offset of capsule: " << capsule_offset << '\n';
                            is_hint = true;
                        }
                    }
#endif

                    for (auto &vertex : vertices)
                    {
                        vertex += global_position;
                    }

                    constexpr auto offset = vec3{ 1 };
                    //constexpr auto offset = vec3{ 0 };

                    if (vertices.empty())
                    {
                        return;
                    }

                    DynamicTerrainComponent terrComp{ std::move(vertices) };

                    AABB aabb = terrComp.getAABB();
                    aabb.expand(offset);

#if 0
                    {
                        ;
                        registry.emplace_or_replace<DebugShape<DynamicTerrainComponent>>(entity, terrComp);
                    }
#endif

                    auto &comp = cam_view.get<DynamicTerrainComponent>(cam_entity);

                    if (const auto capAABB = ray.getAABB(); capAABB.overlaps(aabb))
                    {
                        comp = terrComp;

                        std::cout << "Aabb is overlap." << std::endl;
                    }
                    else
                    {
                        comp.set_invalid();
                    }
                }
            }

            //todo
            //narrow phase
            {

                const auto platform_view = registry.view<PlatformComponent>();

                const auto cam_entity = cam_view.front();

                const auto &ray = cam_view.get<Ray>(cam_entity);
                const auto capsule_offset = cam_view.get<Offset2>(cam_entity).m_value;

                if (auto &prim = cam_view.get<DynamicTerrainComponent>(cam_entity);
                        prim.is_valid())
                {
                    const auto shapes = prim.get_shapes();

                    std::vector<float> res;
                    res.reserve(shapes.size() * 2);

                    for (const auto &shape : shapes)
                    {
                        const auto &convex0 = shape[0];
                        const auto &convex1 = shape[1];

                        const auto res0 = ray.triangle_intersection(
                                convex0.m_vertices[0] + convex0.m_position,
                                convex0.m_vertices[1] + convex0.m_position,
                                convex0.m_vertices[2] + convex0.m_position);

                        const auto res1 = ray.triangle_intersection(
                                convex1.m_vertices[0] + convex1.m_position,
                                convex1.m_vertices[1] + convex1.m_position,
                                convex1.m_vertices[2] + convex1.m_position);

                        if (res0 != 0)
                        {
                            res.push_back(res0);
                        }

                        if (res1 != 0)
                        {
                            res.push_back(res1);
                        }
                    }

                    auto it = std::min_element(res.begin(), res.end());

                    float value = 0;
                    if (it != res.end())
                    {
                        value = *it;
                        std::cout << value << std::endl;
                    }

                    registry.emplace_or_replace<HitResult>(cam_entity, value);

                    prim.set_invalid();
                }

                if (auto &pair = cam_view.get<PairComponent>(cam_entity);
                        !pair.empty())
                {
                    //std::cout << "pair.size() = " << pair.size() << std::endl;

                    for (size_t i{ 0 }; i != pair.size(); ++i)
                    {
                        const auto entity = pair.m_entities[i];

                        const auto &static_offset = registry.get<Offset2>(entity).m_value;
                        //auto collider = reg.try_get<StaticCollider>(entity);
                        //assert(collider != nullptr);

                        auto collider = registry.get<StaticCollider>(entity);

                        std::visit([&](auto &arg) {
                            using T = std::decay_t<decltype(arg)>;

                            auto prim = arg;
                            prim.m_position = to_global_pos(arg.m_position, capsule_offset, static_offset);

                            if constexpr (std::is_same_v<T, TriangleMesh>)
                            {
                                std::vector<float> res;
                                res.reserve(prim.count_triangle());
                                for (uint32_t i{ 0 }; i < prim.count_triangle(); ++i)
                                {
                                    const Triangle triangle = prim.get_triangle(i);

                                    const auto res1 = ray.triangle_intersection(
                                            triangle.m_vertices[0] + triangle.m_position,
                                            triangle.m_vertices[1] + triangle.m_position,
                                            triangle.m_vertices[2] + triangle.m_position);

                                    if (res1 != 0)
                                    {
                                        res.push_back(res1);
                                    }
                                }

                                auto it = std::min_element(res.begin(), res.end());

                                if (it != res.end())
                                {
                                    auto t = *it;
                                    std::cout << t << std::endl;

                                    auto *hit_result = registry.try_get<HitResult>(cam_entity);
                                    if (hit_result != nullptr)
                                    {
                                        if (hit_result->m_value == 0 || t < hit_result->m_value)
                                        {
                                            registry.replace<HitResult>(cam_entity, t);
                                        }
                                    }
                                    else
                                    {
                                        registry.emplace_or_replace<HitResult>(cam_entity, t);
                                    }
                                }
                            }
                            else if constexpr (std::is_same_v<T, BoxCollider>)
                            {
                                float t;
                                auto res = ray.box_intersect(prim.m_position, prim.m_extent, t);

                                if (res)
                                {
                                    auto *hit_result = registry.try_get<HitResult>(cam_entity);
                                    if (hit_result != nullptr)
                                    {
                                        if (hit_result->m_value == 0 || t < hit_result->m_value)
                                        {
                                            registry.replace<HitResult>(cam_entity, t);
                                        }
                                    }
                                    else
                                    {
                                        registry.emplace_or_replace<HitResult>(cam_entity, t);
                                    }

                                    //std::cout << "Intersection between line and box = " << t << std::endl;
                                }
                            }
                            else if constexpr (std::is_same_v<T, SphereCollider>)
                            {
                                vec2 dist;
                                auto res = ray.sphere_intersect(prim.m_position, prim.m_radius, dist[0], dist[1]);

                                if (res > 0)
                                {
                                    auto t = dist[0];

                                    if (auto *hit_result = registry.try_get<HitResult>(cam_entity);
                                            hit_result != nullptr)
                                    {
                                        if (hit_result->m_value == 0 || t < hit_result->m_value)
                                        {
                                            registry.replace<HitResult>(cam_entity, t);
                                        }
                                    }
                                    else
                                    {
                                        registry.emplace_or_replace<HitResult>(cam_entity, t);
                                    }

                                    //std::cout << "Intersection between line and sphere = " << t << std::endl;
                                }
                            }
                            else
                            {

                                if (
                                        //check(prim) &&
                                        platform_view.contains(entity))
                                {
                                    registry.emplace<ConstraintComponent>(cam_entity, entity);
                                }
                            }
                        },
                                collider);
                    }

                    pair.clear();
                }
            }

#endif
        }

        static constexpr float g_chunk_size = 0.5;

        PhysicsSystem *m_physics_system{ nullptr };
    };
} // namespace WFL
