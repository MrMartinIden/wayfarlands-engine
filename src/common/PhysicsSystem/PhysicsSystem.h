// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <system.hpp>

#include "BroadPhaseSystem.h"
#include "DebugPairSystem.h"
#include "NarrowPhaseSystem.h"
#include "TerrainCollisionSystem.h"

namespace WFL
{

#define DEBUG_PHYSICS 0

    struct PhysicsSystem : BaseSystem<PhysicsSystem>
    {
        explicit PhysicsSystem(entt::registry &reg) noexcept :
                m_broadPhase(reg),
                m_terrainCollision(reg),
                m_narrowPhase(reg)
        {
        }

        void update(entt::registry &reg, [[maybe_unused]] float dt = 0)
        {
#if DEBUG_PHYSICS
            {
                auto cap_view = reg.view<WFL::CapsuleCollider>();
                auto box_view = reg.view<WFL::BoxCollider>();
                if (!cap_view.empty())
                {
                    auto cap = cap_view.get<WFL::CapsuleCollider>(cap_view.front());

                    std::cout << "Capsule " << glm::to_string(cap.m_position) << '\n';
                }

                if (!box_view.empty())
                {
                    auto box = box_view.get<WFL::BoxCollider>(box_view.front());
                    std::cout << "Box " << glm::to_string(box.m_position) << '\n';
                }
            }
#endif

            m_broadPhase.update(reg);

            m_terrainCollision.update(reg);

#if 0
            {
                DebugPairSystem debugPair;
                auto count = debugPair.debug(reg);

                std::cout << "Count of pairs = " << count << '\n';
            }
#endif

            m_narrowPhase.update(reg);
        }

        BroadPhaseMt m_broadPhase;
        TerrainCollisionMt m_terrainCollision;
        NarrowPhaseMt m_narrowPhase;
    };

#undef DEBUG_PHYSICS

} // namespace WFL
