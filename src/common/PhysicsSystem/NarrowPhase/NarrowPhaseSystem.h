// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/fast_square_root.hpp>

#include <entt/entity/registry.hpp>

#include "MPR.h"

#include <components.hpp>
#include <constraint.hpp>
#include <debug_shape.hpp>
#include <offset.hpp>
#include <pair.hpp>
#include <speed.hpp>
#include <utility.hpp>

#include "tbb/parallel_for_each.h"

namespace WFL
{
    template <bool is_multithreaded>
    struct NarrowPhaseSystem final
    {
        NarrowPhaseSystem(entt::registry &reg) noexcept
                :
                m_group(reg.group<CapsuleCollider, SpeedComponent>(entt::get< //MovementComponent,
                                                                           PairComponent,
                                                                           Offset2,
                                                                           TerrainComponent>,
                        entt::exclude<ConstraintComponent>))
        {
        }

        void update(entt::registry &reg)
        {
            const auto platform_view = reg.view<PlatformComponent>();

            auto func = [&](const entt::entity &character_entity) {
                CapsuleCollider &capsule = m_group.get<CapsuleCollider>(character_entity);
                const auto &capsule_offset = m_group.get<Offset2>(character_entity).m_value;

                auto check = [&](const auto &collider) -> bool {
                    [[maybe_unused]] bool result = false;
                    vec3 mtv{ 0 };

                    if (auto [contact_info, res] = ccdMPRPenetration(capsule, collider); res)
                    {
                        auto &normal = contact_info.m_normal;
                        const auto &depth = contact_info.m_depth;

#if 0
                    {
                        auto point = findOrigin(capsule, collider);

                        reg.emplace_or_replace<DebugShape<support_t>>(character_entity, point);
                        reg.emplace_or_replace<DebugShape<contact_info_t>>(character_entity, contact_info);
                    }

#endif

                        normal = -normal;
                        mtv = normal * vec3::value_type{ depth };

                        const auto radians = [&normal]() {
                            auto radians = glm::acos(glm::dot(normal.y, vec3::value_type{ 1.0 }));

                            if (glm::isnan(radians))
                            {
                                radians = 0;
                            }

                            return radians;
                        }();

                        const auto ground_slope = glm::degrees(radians);
                        //std::cout << "Ground_slope = " << ground_slope << std::endl;

                        if (auto &speed = m_group.get<SpeedComponent>(character_entity);
                                ground_slope < player_max_stand_slope)
                        {
                            speed.y = 0;
                        }

                        else
                        {
                            speed.x *= 0.5;
                            speed.z *= 0.5;
                        }

                        /*
                        std::cout << "Ground_slope = " << ground_slope << std::endl
                                  << "Position capsule = " << glm::to_string(capsule.m_position) << std::endl
                                  << "Mtv = " << glm::to_string(mtv) << std::endl
                                  << std::endl;
                        */

                        result = true;
                    }

                    capsule.m_position += mtv;

                    return result;
                };

                {
                    TerrainComponent &prim = m_group.get<TerrainComponent>(character_entity);

                    if (prim.is_valid())
                    {

                        auto shapes = prim.get_shapes();

                        check(shapes[0]);
                        check(shapes[1]);

                        prim.set_invalid();
                    }
                }

                auto &pair = m_group.get<PairComponent>(character_entity);
                if (!pair.empty())
                {
                    const auto static_collider_view = reg.view<StaticCollider, Offset2>();

                    for (size_t i{ 0 }; i != pair.size(); ++i)
                    {
                        const auto entity = pair.m_entities[i];

                        const auto &static_offset = static_collider_view.get<Offset2>(entity).m_value;
                        //auto collider = reg.try_get<StaticCollider>(entity);
                        //assert(collider != nullptr);

                        const auto &collider = static_collider_view.get<StaticCollider>(entity);

                        std::visit([&](auto &arg) {
                            using T = std::decay_t<decltype(arg)>;

                            auto prim = arg;
                            prim.m_position = to_global_pos(arg.m_position, capsule_offset, static_offset);

                            if constexpr (std::is_same_v<T, TriangleMesh>)
                            {

                                for (uint32_t i{ 0 }; i < prim.count_triangle(); ++i)
                                {

                                    Triangle triangle = prim.get_triangle(i);
                                    check(triangle);
                                }
                            }
                            else
                            {

                                if (check(prim) && platform_view.contains(entity))
                                {
                                    reg.emplace<ConstraintComponent>(character_entity, entity);
                                }
                            }
                        },
                                collider);
                    }

                    pair.clear();
                }
            };

            if constexpr (is_multithreaded)
            {
                tbb::parallel_for_each(m_group.begin(), m_group.end(), func);
            }
            else
            {
                std::for_each(m_group.begin(), m_group.end(), func);
            }
        }

        using capsule_group_t = entt::basic_group<entt::entity, entt::exclude_t<ConstraintComponent>, entt::get_t<
                                                                                                              //MovementComponent,
                                                                                                              PairComponent, Offset2, TerrainComponent>,
                CapsuleCollider, SpeedComponent>;

        capsule_group_t m_group;
    };

    using NarrowPhaseMt = NarrowPhaseSystem<true>;
    using NarrowPhaseSt = NarrowPhaseSystem<false>;

} // namespace WFL
