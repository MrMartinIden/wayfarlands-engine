#pragma once

#include <glm/glm.hpp>

#include <glm/gtx/extended_min_max.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/epsilon.hpp>

#include <glm/gtc/constants.hpp>

#include <config.hpp>
#include <capsule_collider.hpp>
#include <aabb.hpp>

namespace WFL
{

    inline constexpr vec3 vec3_origin{ 0 };
    inline constexpr vec3::value_type mpr_tolerance = 0.0001;
    inline constexpr uint32_t max_iterations = 10000;

    struct contact_info_t
    {
        vec3 m_pos;
        vec3 m_normal;
        vec3::value_type m_depth;
    };

    struct support_t
    {
        vec3 v; //!< Support point in minkowski sum
        vec3 v1; //!< Support point in obj1
        vec3 v2; //!< Support point in obj2
    };

    struct simplex_t
    {
        support_t ps[4];
        int last; //!< index of last added point

        inline int size() const
        {
            return this->last + 1;
        }

        inline void set_size(int size)
        {
            this->last = size - 1;
        }

        inline support_t& get_point(uint32_t idx)
        {
            return this->ps[idx];
        }

        inline const support_t& get_point(uint32_t idx) const
        {
            return this->ps[idx];
        }
    }; 

    static inline bool ccdEq(vec3::value_type _a, vec3::value_type _b);

    inline bool ccdVec3Eq(const vec3 &a, const vec3 &b)
    {
#if 0
        return ccdEq(a[0], b[0]) && ccdEq(a[1], b[1]) && ccdEq(a[2], b[2]);
#else
        return glm::all(glm::equal(a, b));
#endif
    }

    inline bool ccdIsZero(vec3::value_type val)
    {
        return glm::abs(val) < glm::epsilon<vec3::value_type>();
    }

    bool ccdEq(vec3::value_type _a, vec3::value_type _b)
    {
#if 0
        auto ab = glm::abs(_a - _b);
        if (glm::abs(ab) < glm::epsilon<vec3::value_type>())
        {
            return 1;
        }

        auto a = glm::abs(_a);
        auto b = glm::abs(_b);
        if (b > a)
        {
            return ab < glm::epsilon<vec3::value_type>() * b;
        }
        else
        {
            return ab < glm::epsilon<vec3::value_type>() * a;
        }
#else
        return glm::epsilonEqual(_a, _b, glm::epsilon<vec3::value_type>());
#endif
    }

    template <class T1, class T2>
    inline support_t findOrigin(const T1 &obj1, const T2 &obj2)
    {
        support_t center = support_t{ obj1.m_position - obj2.m_position, obj1.m_position, obj2.m_position };

        /*
        center.v1 = obj1.m_position;
        center.v2 = obj2.m_position;
        center.v = center.v1 - center.v2;
        */

        return center;
    }

    template <class T1, class T2>
    static int discoverPortal(const T1 &obj1, const T2 &obj2, simplex_t &portal)
    {
        int cont;

        // vertex 0 is center of portal
        portal.get_point(0) = findOrigin(obj1, obj2);
        portal.set_size(1);

        if (ccdVec3Eq(portal.get_point(0).v, vec3_origin))
        {
            // Portal's center lies on origin (0,0,0) => we know that objects
            // intersect but we would need to know penetration info.
            // So move center little bit...
            vec3 v = { glm::epsilon<vec3::value_type>() * 10.f, 0.f, 0.f };
            portal.get_point(0).v += v;
        }

        // vertex 1 = support in direction of origin
        vec3 dir = portal.get_point(0).v;
        dir = glm::normalize(-dir);
        portal.get_point(1) = __Support(obj1, obj2, dir);
        portal.set_size(2);

        // test if origin isn't outside of v1
        auto dot = glm::dot(portal.get_point(1).v, dir);
        if (ccdIsZero(dot) || dot < 0.f)
        {
            return -1;
        }

        // vertex 2
        dir = glm::cross(portal.get_point(0).v, portal.get_point(1).v);

        if (ccdIsZero(glm::length2(dir)))
        {
            if (ccdVec3Eq(portal.get_point(1).v, vec3_origin))
            {
                // origin lies on v1
                return 1;
            }
            else
            {
                // origin lies on v0-v1 segment
                return 2;
            }
        }

        dir = glm::normalize(dir);
        portal.get_point(2) = __Support(obj1, obj2, dir);
        dot = glm::dot(portal.get_point(2).v, dir);
        if (ccdIsZero(dot) || dot < 0.f)
        {
            return -1;
        }

        portal.set_size(3);

        // vertex 3 direction
        vec3 va = portal.get_point(1).v - portal.get_point(0).v;
        vec3 vb = portal.get_point(2).v - portal.get_point(0).v;
        dir = glm::cross(va, vb);
        dir = glm::normalize(dir);

        // it is better to form portal faces to be oriented "outside" origin
        dot = glm::dot(dir, portal.get_point(0).v);
        if (dot > 0.f)
        {
            std::swap(portal.ps[1], portal.ps[2]);
            dir *= -1.f;
        }

        while ( portal.size() < 4)
        {
            portal.get_point(3) = __Support(obj1, obj2, dir);
            dot = glm::dot(portal.get_point(3).v, dir);
            if (ccdIsZero(dot) || dot < 0.f)
                return -1;

            cont = 0;

            // test if origin is outside (v1, v0, v3) - set v2 as v3 and
            // continue
            va = glm::cross(portal.get_point(1).v, portal.get_point(3).v);
            dot = glm::dot(va, portal.get_point(0).v);
            if (dot < 0.f && !ccdIsZero(dot))
            {
                portal.ps[2] = portal.get_point(3);
                cont = 1;
            }

            if (!cont)
            {
                // test if origin is outside (v3, v0, v2) - set v1 as v3 and
                // continue
                va = glm::cross(portal.get_point(3).v, portal.get_point(2).v);
                dot = glm::dot(va, portal.get_point(0).v);
                if (dot < 0.f && !ccdIsZero(dot))
                {
                    portal.ps[1] = portal.get_point(3);
                    cont = 1;
                }
            }

            if (cont)
            {
                va = portal.get_point(1).v - portal.get_point(0).v;
                vb = portal.get_point(2).v - portal.get_point(0).v;
                dir = glm::cross(va, vb);
                dir = glm::normalize(dir);
            }
            else
            {
                portal.set_size(4);
            }
        }

        return 0;
    }

    inline vec3 portalDir(const simplex_t &portal)
    {
        vec3 v2v1, v3v1;

        v2v1 = portal.get_point(2).v - portal.get_point(1).v;
        v3v1 = portal.get_point(3).v - portal.get_point(1).v;

        vec3 dir = glm::cross(v2v1, v3v1);
        dir = glm::normalize(dir);

        return dir;
    }

    template <class T1, class T2>
    inline support_t __Support(const T1 &obj1, const T2 &obj2, const vec3 &dir)
    {
        support_t supp;

        supp.v1 = obj1.support(dir);
        supp.v2 = obj2.support(-dir);

        supp.v = supp.v1 - supp.v2;

        return supp;
    }

    inline int portalCanEncapsuleOrigin(const support_t &v4, const vec3 &dir)
    {
        auto dot = glm::dot(v4.v, dir);
        return ccdIsZero(dot) || dot > 0.f;
    }

    inline int portalReachTolerance(const simplex_t &portal,
            const support_t &v4,
            const vec3 &dir)
    {
        vec3::value_type dv1, dv2, dv3, dv4;
        vec3::value_type dot1, dot2, dot3;

        // find the smallest dot product of dir and {v1-v4, v2-v4, v3-v4}

        dv1 = glm::dot(portal.get_point(1).v, dir);
        dv2 = glm::dot(portal.get_point(2).v, dir);
        dv3 = glm::dot(portal.get_point(3).v, dir);
        dv4 = glm::dot(v4.v, dir);

        dot1 = dv4 - dv1;
        dot2 = dv4 - dv2;
        dot3 = dv4 - dv3;

        dot1 = glm::min(glm::min(dot1, dot2), dot3);

        return ccdEq(dot1, mpr_tolerance) || dot1 < mpr_tolerance;
    }

    void expandPortal(simplex_t &portal, const support_t &v4);

    template <class T1, class T2>
    static int refinePortal(const T1 &obj1, const T2 &obj2, simplex_t &portal)
    {
        vec3 dir;
        support_t v4;

        while (1)
        {
            // compute direction outside the portal (from v0 throught v1,v2,v3
            // face)
            dir = portalDir(portal);

            auto portalEncapsulesOrigin = [&](const simplex_t &portal, const vec3 &dir) -> bool {
                auto dot = glm::dot(dir, portal.get_point(1).v);
                return ccdIsZero(dot) || dot > 0.f;
            };

            // test if origin is inside the portal
            if (portalEncapsulesOrigin(portal, dir))
            {
                return 0;
            }

            // get next support point
            v4 = __Support(obj1, obj2, dir);

            // test if v4 can expand portal to contain origin and if portal
            // expanding doesn't reach given tolerance
            if (!portalCanEncapsuleOrigin(v4, dir) || portalReachTolerance(portal, v4, dir))
            {
                return -1;
            }

            // v1-v2-v3 triangle must be rearranged to face outside Minkowski
            // difference (direction from v0).
            expandPortal(portal, v4);
        }

        return -1;
    }

    template <class T1, class T2>
    static bool ccdMPRIntersect(const T1 &obj1, const T2 &obj2)
    {
        simplex_t portal;

        // Phase 1: Portal discovery - find portal that intersects with origin
        // ray (ray from center of Minkowski diff to origin of coordinates)
        int res = discoverPortal(obj1, obj2, portal);
        if (res < 0)
        {
            return false;
        }

        if (res > 0)
        {
            return true;
        }

        // Phase 2: Portal refinement
        res = refinePortal(obj1, obj2, portal);
        return (res == 0);
    }

    static vec3 findPos(const simplex_t &portal)
    {
        vec3::value_type b[4];

        vec3 dir = portalDir(portal);

        // use barycentric coordinates of tetrahedron to find origin
        vec3 cross1_2 = glm::cross(portal.get_point(1).v, portal.get_point(2).v);
        b[0] = glm::dot(cross1_2, portal.get_point(3).v);

        vec3 vec = glm::cross(portal.get_point(3).v, portal.get_point(2).v);
        b[1] = glm::dot(vec, portal.get_point(0).v);

        vec = glm::cross(portal.get_point(0).v, portal.get_point(1).v);
        b[2] = glm::dot(vec, portal.get_point(3).v);

        vec = glm::cross(portal.get_point(2).v, portal.get_point(1).v);
        b[3] = glm::dot(vec, portal.get_point(0).v);

        vec3::value_type sum = b[0] + b[1] + b[2] + b[3];

        if (ccdIsZero(sum) || sum < 0.f)
        {
            b[0] = 0.f;

            vec = glm::cross(portal.get_point(2).v, portal.get_point(3).v);
            b[1] = glm::dot(vec, dir);
            vec = glm::cross(portal.get_point(3).v, portal.get_point(1).v);
            b[2] = glm::dot(vec, dir);
            b[3] = glm::dot(cross1_2, dir);

            sum = b[1] + b[2] + b[3];
        }

        vec3::value_type inv = vec3::value_type{1.0} / sum;

        vec3 p1 = vec3_origin;
        vec3 p2 = vec3_origin;
        for (uint8_t i = 0; i < 4; ++i)
        {
            vec = portal.get_point(i).v1;
            vec *= b[i];
            p1 += vec;

            vec = portal.get_point(i).v2;
            vec *= b[i];
            p2 += vec;
        }

        p1 *= inv;
        p2 *= inv;

        vec3 pos = (p1 + p2) * vec3::value_type{ 0.5 };

        return pos;
    }

    static vec3::value_type __ccdVec3PointSegmentDist2(const vec3 &P,
            const vec3 &x0,
            const vec3 &b,
            vec3 &witness)
    {
        // The computation comes from solving equation of segment:
        //      S(t) = x0 + t.d
        //          where - x0 is initial point of segment
        //                - d is direction of segment from x0 (|d| > 0)
        //                - t belongs to <0, 1> interval
        //
        // Than, distance from a segment to some point P can be expressed:
        //      D(t) = |x0 + t.d - P|^2
        //          which is distance from any point on segment. Minimization
        //          of this function brings distance from P to segment.
        // Minimization of D(t) leads to simple quadratic equation that's
        // solving is straightforward.
        //
        // Bonus of this method is witness point for free.

        vec3::value_type dist, t;
        vec3 d, a;

        // direction of segment
        d = b - x0;

        // precompute vector from P to x0
        a = x0 - P;

        t = -1.f * glm::dot(a, d);
        t /= glm::length2(d);

        if (t < 0.f || ccdIsZero(t))
        {
            dist = glm::distance2(x0, P);
            witness = x0;

        }
        else if (t > 1.f || ccdEq(t, 1.f))
        {
            dist = glm::distance2(b, P);
            witness = b;

        }
        else
        {
            witness = d;
            witness *= t;
            witness += x0;
            dist = glm::distance2(witness, P);
        }

        return dist;
    }

    static vec3::value_type ccdVec3PointTriDist2(const vec3 &P,
            const vec3 &x0,
            const vec3 &B,
            const vec3 &C,
            vec3& witness)
    {
        // Computation comes from analytic expression for triangle (x0, B, C)
        //      T(s, t) = x0 + s.d1 + t.d2, where d1 = B - x0 and d2 = C - x0 and
        // Then equation for distance is:
        //      D(s, t) = | T(s, t) - P |^2
        // This leads to minimization of quadratic function of two variables.
        // The solution from is taken only if s is between 0 and 1, t is
        // between 0 and 1 and t + s < 1, otherwise distance from segment is
        // computed.

        vec3::value_type s, t, dist, dist2;
        vec3 witness2;

        vec3 d1 = B - x0;
        vec3 d2 = C - x0;
        vec3 a = x0 - P;

        //auto u = glm::dot(a, a);
        auto v = glm::dot(d1, d1);
        auto w = glm::dot(d2, d2);
        auto p = glm::dot(a, d1);
        auto q = glm::dot(a, d2);
        auto r = glm::dot(d1, d2);

        auto d = w * v - r * r;
        if (ccdIsZero(d))
        {
            // To avoid division by zero for zero (or near zero) area triangles
            s = t = -1.;
        }
        else
        {
            s = (q * r - w * p) / d;
            t = (-s * r - q) / w;
        }

        if ((ccdIsZero(s) || s > 0.f) &&
                (ccdEq(s, 1.f) || s < 1.f) &&
                (ccdIsZero(t) || t > 0.f) &&
                (ccdEq(t, 1.f) || t < 1.f) &&
                (ccdEq(t + s, 1.f) || t + s < 1.f))
        {

            d1 *= s;
            d2 *= t;
            witness = x0;
            witness += d1;
            witness += d2;

            dist = glm::distance2(witness, P);
        }
        else
        {
            dist = __ccdVec3PointSegmentDist2(P, x0, B, witness);

            dist2 = __ccdVec3PointSegmentDist2(P, x0, C, witness2);
            if (dist2 < dist)
            {
                dist = dist2;
                witness = witness2;
            }

            dist2 = __ccdVec3PointSegmentDist2(P, B, C, witness2);
            if (dist2 < dist)
            {
                dist = dist2;
                witness = witness2;
            }
        }

        return dist;
    }

    template <class T1, class T2>
    static contact_info_t findPenetr(const T1 &obj1,
            const T2 &obj2,
            simplex_t &portal)
    {
        vec3::value_type depth;
        vec3 pdir;
        vec3 pos;

        vec3 dir;
        support_t v4;

        for (size_t iterations = 0;;)
        {
            // compute portal direction and obtain next support point
            dir = portalDir(portal);
            v4 = __Support(obj1, obj2, dir);

            // reached tolerance -> find penetration info
            if (portalReachTolerance(portal, v4, dir) || iterations > max_iterations)
            {
                depth = ccdVec3PointTriDist2(vec3_origin,
                        portal.get_point(1).v,
                        portal.get_point(2).v,
                        portal.get_point(3).v,
                        pdir);
                depth = glm::sqrt(depth);

#if 1
                if (ccdIsZero(depth))
                {
                    // If depth is zero, then we have a touching contact.
                    // So following findPenetrTouch(), we assign zero to
                    // the direction vector (it can actually be anything
                    // according to the decription of ccdMPRPenetration
                    // function).
                    pdir = vec3_origin;
                }
                else
                {
                    pdir = glm::normalize(pdir);
                }

#else
                auto isZero = ccdIsZero(depth);
                pdir = ccd_vec3_origin * isZero + glm::normalize(pdir) * (!isZero);
#endif

                // barycentric coordinates:
                pos = findPos(portal);

                return {pos, pdir, depth};
            }

            expandPortal(portal, v4);

            ++iterations;
        }

        return {pos, pdir, depth};
    }

    static contact_info_t findPenetrTouch(const simplex_t &portal)
    {
        // Touching contact on portal's v1 - so depth is zero and direction
        // is unimportant and pos can be guessed
        auto pos = (portal.get_point(1).v1 + portal.get_point(1).v2) * vec3::value_type{ 0.5 };

        return {pos, vec3_origin, vec3::value_type{0}};
    }

    static contact_info_t findPenetrSegment(simplex_t &portal)
    {
        // Origin lies on v0-v1 segment.
        // Depth is distance to v1, direction also and position must be
        // computed

        auto pos = (portal.get_point(1).v1 + portal.get_point(1).v2) * vec3::value_type{ 0.5 };

        auto dir = portal.get_point(1).v;
        auto depth = glm::length(dir);
        dir = glm::normalize(dir);

        return {pos, dir, depth};
    }

    template <class T1 = CapsuleCollider, class T2>
    static std::pair<contact_info_t, bool> ccdMPRPenetration(
            const T1 &obj1, const T2 &obj2)
    {
        simplex_t portal;

        contact_info_t contact_info;

        // Phase 1: Portal discovery
        int res = discoverPortal(obj1, obj2, portal);
        if (res < 0)
        {
            // Origin isn't inside portal - no collision.
            return std::make_pair(contact_info, false);;
        }
        else if (res == 1)
        {
            // Touching contact on portal's v1.
            contact_info = findPenetrTouch(portal);
        }
        else if (res == 2)
        {
            // Origin lies on v0-v1 segment.
            contact_info = findPenetrSegment(portal);
        }
        else if (res == 0)
        {
            // Phase 2: Portal refinement
            res = refinePortal(obj1, obj2, portal);
            if (res < 0)
            {
                return std::make_pair(contact_info, false);
            }

            // Phase 3. Penetration info
            contact_info = findPenetr(obj1, obj2, portal);
        }

        return std::make_pair(contact_info, true);
    }

} // namespace WFL
