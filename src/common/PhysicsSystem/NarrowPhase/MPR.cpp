#include "MPR.h"

namespace WFL
{

    void expandPortal(simplex_t &portal, const support_t &v4)
    {
        vec3 v4v0 = glm::cross(v4.v, portal.get_point(0).v);
        auto dot = glm::dot(portal.get_point(1).v, v4v0);
        if (dot > 0.f)
        {
            dot = glm::dot(portal.get_point(2).v, v4v0);
            if (dot > 0.f)
            {
                portal.ps[1] = v4;
            }
            else
            {
                portal.ps[3] = v4;
            }
        }
        else
        {
            dot = glm::dot(portal.get_point(3).v, v4v0);
            if (dot > 0.f)
            {
                portal.ps[2] = v4;
            }
            else
            {
                portal.ps[1] = v4;
            }
        }
    }

} // namespace WFL
