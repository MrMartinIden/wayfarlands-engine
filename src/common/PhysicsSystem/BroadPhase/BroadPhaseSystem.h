// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <constraint.hpp>
#include <morton_code.hpp>
#include <offset.hpp>
#include <pair.hpp>
#include <utility.hpp>

#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/range.hpp>

#include <tbb/parallel_for_each.h>

#include <iostream>
#include <execution>

namespace WFL
{

    template <typename T, typename = std::enable_if_t<std::is_unsigned_v<T>>>
    inline bool inRange(T min, T max, T x)
    {
        return min <= x && x <= max;
    }

    template <typename T>
    inline constexpr vec2 to_vec2(T&& v)
    {
        return { v.x, v.z };
    }

    struct compute_global_code
    {
        template <typename T = entt::identity>
        [[nodiscard]] static uint64_t generate(T p_pos = T {})
        {
            constexpr auto mortonBits = std::numeric_limits<uint64_t>::digits / 3;
            constexpr uint32_t mortonScale = (1 << mortonBits) - 1;

            constexpr auto size = global_size;

            vec3 pos = p_pos;

            if (!glm::all(glm::greaterThanEqual(pos, vec3{ 0 })))
            {
                std::cout << glm::to_string(pos) << '\n';
                assert(0);
            }

            //assert(glm::all(glm::greaterThanEqual(pos, vec3 { 0 })));

            pos /= size;
            pos *= mortonScale;

            assert(static_cast<double>(pos.x) <= mortonScale);
            assert(static_cast<double>(pos.y) <= mortonScale);
            assert(static_cast<double>(pos.z) <= mortonScale);

            const auto floor = glm::floor(pos);

            uint64_t code = libmorton::morton3D_64_encode(
                static_cast<uint_fast32_t>(floor.x),
                static_cast<uint_fast32_t>(floor.y),
                static_cast<uint_fast32_t>(floor.z));

            return code;
        }

        template <typename T = entt::identity>
        [[nodiscard]] static uint64_t generate(uint32_t offset, T pos = T{})
        {
            auto p = to_global(pos, offset);

            return generate(p);
        }

        template <typename T>
        [[nodiscard]] static inline std::array<uint64_t, 2> generate(const vec2& offset, T&& min, T&& max)
        {
            const vec2 chunk_origin = offset * chunk_size + half_chunk_size;

            constexpr auto dimension = 3u;

            vec2 local_pos, global_pos;

            std::array<uint64_t, 2> result;
            {
                constexpr vec3 compVec { 0 };
                auto pos = std::forward<T>(min);
                local_pos = to_vec2(pos);

                global_pos = chunk_origin + local_pos;

                vec3 p { global_pos[0], pos.y, global_pos[1] };
                p = glm::floor(p);

                const auto less = glm::lessThan(p, compVec);
                for (uint8_t i = 0; i < dimension; ++i) {
                    if (less[i]) {
                        p[i] = compVec[i];
                    }
                }

                result[0] = generate(p);
            }

            {
                constexpr vec3 compVec { global_size };
                auto pos = std::forward<T>(max);
                local_pos = to_vec2(pos);

                global_pos = chunk_origin + local_pos;

                vec3 p { global_pos[0], pos.y, global_pos[1] };
                p = glm::ceil(p);

                const auto greater = glm::greaterThan(p, compVec);
                for (uint8_t i = 0; i < dimension; ++i) {
                    if (greater[i]) {
                        p[i] = compVec[i];
                    }
                }

                result[1] = generate(p);
            }

            return result;
        }
    };

    template <bool is_multithreaded = true>
    struct BroadPhaseSystemNaive final
    {

        BroadPhaseSystemNaive(entt::registry &)
        {
        }

        BroadPhaseSystemNaive() = delete;

        void prepare() {}

        /**
         * @brief process
         *
         * @param bounds
         * @param maxOffset
         */
        template <uint8_t Variant = 1>
        void process(entt::registry &reg)
        {
            auto character_view = reg.view<AABB, CharacterComponent>();
            const auto static_view = reg.view<AABB, StaticComponent>();

            auto viewPair = reg.view<PairComponent>();

            auto func = [&](const entt::entity basisEntity) {
                const AABB& character_aabb = character_view.get<AABB>(basisEntity);
                auto [character_aabb_min, character_aabb_max] = character_aabb.min_max();

                auto findPairs = [&](auto start, auto end) {
                    for (auto it = start; it != end; ++it)
                    {
                        const entt::entity entity = *it;

                        auto overlaps = [](const vec3& a_min, const vec3& a_max, const vec3& b_min, const vec3& b_max) -> bool {
                            bool ret = glm::all(glm::greaterThanEqual(a_max, b_min)) && glm::all(glm::lessThanEqual(a_min, b_max));

                            return ret;
                        };

                        {
                            const auto& static_aabb = static_view.get<AABB>(entity);
                            if (auto [min, max] = static_aabb.min_max();
                                overlaps(character_aabb_min, character_aabb_max, min, max)) {
                                auto& pair = viewPair.get<PairComponent>(basisEntity);

                                pair.m_entities.at(pair.m_size++) = entity;
                            }
                        }
                    }
                };

                findPairs(static_view.begin(), static_view.end());
            };

            if constexpr (is_multithreaded)
            {
                std::for_each(std::execution::par, character_view.begin(), character_view.end(), func);
            }
            else
            {
                std::for_each(character_view.begin(), character_view.end(), func);
            }
        }

        /**
         * @brief update
         *
         * @param reg
         */
        inline void update(entt::registry &reg)
        {
            process(reg);
        }
    };

    /**
     * @brief Broad phase for calculations in the specified static space.
     * 
     * @tparam Edge The value of the sides of the global bounding volume (for width and length).
     * @tparam Height The value for the height of the global bounding volume.
     * @tparam Mt Flag for use multithreaded computing.
     */
    template <bool is_multithreaded>
    struct BroadPhaseSystem final
    {
        using character_group_t = entt::basic_group<entt::entity, entt::exclude_t<ConstraintComponent>, entt::get_t<>, AABB, MortonPrimitive, Offset2, CharacterComponent>;

        using static_group_t = entt::basic_group<entt::entity, entt::exclude_t<>, entt::get_t<AABB, MortonPrimitive, Offset2>, StaticComponent>;

        explicit BroadPhaseSystem(entt::registry &reg) noexcept :
                m_character_group(reg.group<AABB, MortonPrimitive, Offset2, CharacterComponent>(entt::exclude<ConstraintComponent>)),
                m_static_group(reg.group<StaticComponent>(entt::get<AABB, MortonPrimitive, Offset2>))
        {
            prepare(reg);
        }

        BroadPhaseSystem() = delete;

        /**
         * @brief Computes the maximum offset using the half diagonal of each box
         *
         * @return maxOffset
         */
        void compute_max_offset(entt::registry &reg)
        {
            vec3 maxOffset{ 0 };

            auto func = [&maxOffset](const AABB &prim) {
                const vec3 halfDiagonal = prim.diagonal() * vec3::value_type{ 0.5 };

                const auto vec = glm::greaterThan(halfDiagonal, maxOffset);

                for (uint8_t i = 0; i < 3; ++i)
                {
#define BRANCHLESS 0
#if !BRANCHLESS
                    if (vec[i])
                    {
                        maxOffset[i] = halfDiagonal[i];
                    }
#else
                    maxOffset[i] = halfDiagonal[i] * vec[i] + maxOffset[i] * !vec[i];
#endif
                }
            };

            auto aabb_view = reg.view<AABB>();

            aabb_view.each([&](const AABB &prim) {
                func(prim);
            });

            m_maxOffset = maxOffset;
        }

        /**
         * @brief compute_local_codes_for_char Compute the Morton code for center of
         * each AABB.
         *
         * @param bounds The overall bounds relative to which the centroid
         * offset of the AABB is calculated
         */
        void compute_codes_character()
        {
            auto calc = [&](auto exec) {
                std::for_each(exec, m_character_group.begin(), m_character_group.end(), [&](auto entity) {
                    const Offset2 &offset = m_character_group.get<Offset2>(entity);
                    const AABB &aabb = m_character_group.get<AABB>(entity);

                    MortonPrimitive &primCode = m_character_group.get<MortonPrimitive>(entity);
#ifdef DEBUG_BROADPHASE
                    std::cout << "compute_codes_character: " << glm::to_string(aabb.position()) << '\n';
#endif
                    
                    primCode = compute_global_code::generate(offset.m_value, aabb.position());
                });
            };

            if constexpr (is_multithreaded)
            {
                calc(std::execution::par);
            }
            else
            {
                calc(std::execution::seq);
            }
        }

        /**
         * @brief sort
         *
         */
        template <typename Sort = entt::std_sort>
        inline void sort_static(Sort algo = Sort{})
        {
            auto sort = [&]([[maybe_unused]] auto exec) -> void {
                auto less = [&](const MortonPrimitive &lhs, const MortonPrimitive &rhs) -> bool {
                    return lhs.m_value < rhs.m_value;
                };

                if constexpr (std::is_same_v<Sort, entt::std_sort>)
                {
                    m_static_group.sort<MortonPrimitive>(less, algo, exec);
                }
                else
                {
                    m_static_group.sort<MortonPrimitive>(less, algo);
                }
            };

            if constexpr (is_multithreaded)
            {
                sort(std::execution::par);
            }
            else
            {
                sort(std::execution::seq);
            }
        }

        template <typename Sort = entt::std_sort>
        inline void sort_dynamic(Sort algo = Sort{})
        {
            auto sort = [this, algo]([[maybe_unused]] auto exec) {
                auto less = [](const MortonPrimitive &lhs, const MortonPrimitive &rhs) -> bool {
                    return lhs.m_value < rhs.m_value;
                };

                if constexpr (std::is_same_v<Sort, entt::std_sort>)
                {
                    m_character_group.sort<MortonPrimitive>(less, algo, exec);
                }
                else
                {
                    m_character_group.sort<MortonPrimitive>(less, algo);
                }
            };

            if constexpr (is_multithreaded)
            {
                sort(std::execution::par);
            }
            else
            {
                sort(std::execution::seq);
            }
        }

        [[nodiscard]] inline decltype(auto) lower_bound(const MortonPrimitive& character_morton_prim)
        {
            const auto &character_global_code = character_morton_prim.m_value;

            const auto &static_group = m_static_group;

            auto it_find = std::lower_bound(static_group.begin(), static_group.end(), character_global_code, [&](auto &entity, decltype(character_global_code) basis_offset) -> bool {
                const MortonPrimitive &static_morton_prim = static_group.get<MortonPrimitive>(entity);

                auto code = static_morton_prim.m_value;

                return code < basis_offset;
            });

            if (auto end = static_group.end(); it_find == end)
            {
                it_find = end - 1;
            }

            return it_find;
        }

        inline void debug_character([[maybe_unused]] entt::entity character_entity)
        {
#ifdef DEBUG_BROADPHASE
            const auto& character_offset = m_character_group.get<Offset2>(character_entity).m_value;

            const auto& character_morton_code = m_character_group.get<MortonPrimitive>(character_entity).m_value;

            const AABB& character_aabb = m_character_group.get<AABB>(character_entity);
            auto [character_aabb_min, character_aabb_max] = character_aabb.min_max();

            auto temp_global_offset = Offset2::decode(character_offset);
            const vec2 character_global_offset = { temp_global_offset[0], temp_global_offset[1] };

            const auto character_range_global_code = compute_global_code::generate(character_global_offset,
                character_aabb_min - m_maxOffset,
                character_aabb_max + m_maxOffset);

            std::cout << "character_entity = " << entt::to_integral(character_entity) << std::endl;
            std::cout << '\t' << "character_offset = " << character_offset << std::endl;
            std::cout << '\t' << "character_range_global_code = [" << character_range_global_code[0] << ", " << character_range_global_code[1] << "]" << std::endl;
            std::cout << '\t' << "character_morton_code = " << character_morton_code << std::endl;
            std::cout << '\t' << "character_aabb = " << character_aabb << std::endl;
#endif
        }

        inline void debug_lower_bound([[maybe_unused]] entt::entity find_entity)
        {
#ifdef DEBUG_BROADPHASE
            std::cout << '\t' << "lower_bound_offset = " << m_static_group.get<MortonPrimitive>(find_entity).m_value << '\n';
#endif
        }

        inline void debug_static_offset([[maybe_unused]] entt::entity static_entity)
        {
#ifdef DEBUG_BROADPHASE
            std::cout << "static_entity = " << entt::to_integral(static_entity) << '\n';

            {
                const auto& static_offset = m_static_group.get<Offset2>(static_entity).m_value;
                const auto [offset_x, offset_z] = Offset2::decode(static_offset);
                std::cout << '\t' << "static_offset = " << static_offset << '\n';
                std::cout << '\t' << "static_offset_range = [" << offset_x << ", " << offset_z << ']' << std::endl;

                std::cout << '\t' << "static_morton_code = " << m_static_group.get<MortonPrimitive>(static_entity).m_value << std::endl;
            }
#endif
        }

        static inline void check_overlap(const vec3 &character_aabb_min, const vec3 &character_aabb_max, const AABB &static_global_aabb, PairComponent &pair, entt::entity static_entity)
        {
            auto overlaps = [](const vec3& a_min, const vec3& a_max, const vec3& b_min, const vec3& b_max) -> bool {
                bool ret = glm::all(glm::greaterThanEqual(a_max, b_min)) && glm::all(glm::lessThanEqual(a_min, b_max));

                return ret;
            };

            if (auto [min, max] = static_global_aabb.min_max();
                    overlaps(character_aabb_min, character_aabb_max, min, max))
            {

                //std::cout << "overlap" << std::endl;

                //debug_static_offset(static_entity);

#ifdef DEBUG_BROADPHASE
                std::cout << '\t' << "static_global_aabb = " << static_global_aabb << '\n';
#endif

#if 0
                std::cout << "pair.m_size = " << pair.m_size << std::endl;
                if (pair.m_size >= 14)
                {

                    assert(0);
                }
#endif

                pair.m_entities.at(pair.m_size++) = static_entity;
            }
        }

        /**
         * @brief process Find morton codes and AABB.
         *
         * @param bounds
         * @param maxOffset
         */
        void process([[maybe_unused]] entt::registry &reg)
        {
            auto pair_view = reg.view<PairComponent>();

            auto func = [&](const entt::entity character_entity) {
                const AABB &character_aabb = m_character_group.get<AABB>(character_entity);
                const auto character_aabb_min = character_aabb.min();
                const auto character_aabb_max = character_aabb.max();

                const auto &character_offset = m_character_group.get<Offset2>(character_entity).m_value;

                auto temp_global_offset = Offset2::decode(character_offset);
                const vec2 character_global_offset = { temp_global_offset[0], temp_global_offset[1] };

                const auto character_range_global_code = compute_global_code::generate(character_global_offset,
                    character_aabb_min - m_maxOffset,
                    character_aabb_max + m_maxOffset);

                assert(character_range_global_code[0] <= character_range_global_code[1]);

                debug_character(character_entity);

                const auto &static_group = m_static_group;

                //std::cout << "static_group.size() = " << static_group.size() << std::endl;

                //if (!static_group.empty())
                {
                    AABB static_global_aabb {};
                    auto &pair = pair_view.get<PairComponent>(character_entity);

                    auto &morton_prim = m_character_group.get<MortonPrimitive>(character_entity);
                    auto it_find = lower_bound(morton_prim);

                    debug_lower_bound(*it_find);

                    //std::cout << "Debug find pair" << std::endl;

                    auto find_pair = [&](auto start, auto end) -> void {
                        static_assert(std::is_same_v<decltype(start), decltype(end)>, "");

                        //std::cout << "count = " << end - start << std::endl;

                        for (auto it = start; it != end; ++it)
                        {
                            const entt::entity static_entity = *it;

                            const auto& static_morton = static_group.get<MortonPrimitive>(static_entity);

                            if (inRange(character_range_global_code[0], character_range_global_code[1], static_morton.m_value))
                            {
                                const auto &static_aabb = static_group.get<AABB>(static_entity);
#ifdef DEBUG_BROADPHASE
                                std::cout << '\t' << "static_aabb = " << static_aabb << '\n';
#endif

                                const auto& static_offset = static_group.get<Offset2>(static_entity).m_value;
                                static_global_aabb = to_global_aabb(static_aabb, character_offset, static_offset);

                                check_overlap(character_aabb_min, character_aabb_max, static_global_aabb, pair, static_entity);
                            }
                            else
                            {
                                break;
                            }
                        }
                    };

                    find_pair(std::make_reverse_iterator(it_find),
                            std::make_reverse_iterator(static_group.begin()));

                    find_pair(it_find, static_group.end());
                }
            };

            if (!m_static_group.empty()) {
                if constexpr (is_multithreaded) {

                    tbb::task_arena limited_arena { 12 };

                    limited_arena.execute([&] { tbb::parallel_for_each(m_character_group.begin(), m_character_group.end(), func); });

                    //std::for_each(std::execution::par, m_character_group.begin(), m_character_group.end(), func);
                } else {
                    std::for_each(std::execution::seq, m_character_group.begin(), m_character_group.end(), func);
                }
            }
        }

        void compute_codes_static()
        {
            m_static_group.each([&]([[maybe_unused]] auto entity, const AABB &aabb, MortonPrimitive &morton_prim, const Offset2 &offset) {
                const auto center = aabb.position();

                morton_prim.m_value = compute_global_code::generate(offset.m_value, center);
            });
        }

        template <uint8_t Var = 0>
        inline void prepare(entt::registry &registry)
        {
            compute_max_offset(registry);

            compute_codes_character();
            compute_codes_static();

            sort_static();

            if constexpr (Var == 0)
            {
                sort_dynamic();
            }
        }

        /**
         * @brief update
         *
         * @param reg
         */
        template <uint8_t Var = 0>
        inline void update(entt::registry &reg)
        {
            compute_codes_character();

            if constexpr (Var == 0)
            {
                sort_dynamic<entt::insertion_sort>();
            }

            process(reg);
        }

        character_group_t m_character_group;

        static_group_t m_static_group;

        vec3 m_maxOffset = vec3{ 0 };
    };

    using BroadPhaseMt = BroadPhaseSystem<true>;
    using BroadPhaseSt = BroadPhaseSystem<false>;

} // namespace WFL
