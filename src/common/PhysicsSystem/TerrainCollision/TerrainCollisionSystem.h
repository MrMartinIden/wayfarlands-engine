// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <capsule_collider.hpp>
#include <components.hpp>
#include <debug_shape.hpp>
#include <offset.hpp>
#include <utility.hpp>

#include "glm/gtx/string_cast.hpp"

#include <execution>
#include <iostream>
#include <mutex>

namespace WFL
{
    template <bool Mt = true>
    struct TerrainCollisionSystem final
    {

        TerrainCollisionSystem(entt::registry &reg) noexcept :
                m_group(reg.group<TerrainComponent>(entt::get<CapsuleCollider, Offset2>))
        {
        }

        void update([[maybe_unused]] entt::registry& registry)
        {
            if (m_height_map.empty())
            {
                return;
            }

            auto func = [&](const entt::entity entity) {
                const CapsuleCollider &capsule = m_group.get<CapsuleCollider>(entity);
                const auto &capsule_offset = m_group.get<Offset2>(entity).m_value;

                const vec3 global_position = to_global_pos(m_position, capsule_offset, 0);

                //const vec3 rel_position = to_global(capsule.m_position, capsule_offset) - global_position;
                const vec3 rel_position = capsule.m_position - global_position;

                const int column = glm::floor(rel_position.x / g_chunk_size);
                const int row = glm::floor(rel_position.z / g_chunk_size);

                const vec2 chunk_coord = vec2{ column, row } * g_chunk_size;

                const int index = column + row * m_resolution;
                assert(index >= 0);

                std::array<vec3, 4> vertices{ vec3{ chunk_coord.x, m_height_map[index], chunk_coord.y },
                    vec3{ chunk_coord.x + g_chunk_size, m_height_map[index + 1], chunk_coord.y },
                    vec3{ chunk_coord.x, m_height_map[index + m_resolution], chunk_coord.y + g_chunk_size },
                    vec3{ chunk_coord.x + g_chunk_size, m_height_map[index + m_resolution + 1], chunk_coord.y + g_chunk_size } };

#if 0
                {
                    static bool is_hint = false;
                    if (!is_hint)
                    {
                        std::cout << "position: " << glm::to_string(m_position) << '\n'
                                  << "heights: " << m_height_map[index] << ' '
                                  << m_height_map[index + 1] << ' '
                                  << m_height_map[index + m_resolution] << ' '
                                  << m_height_map[index + m_resolution + 1] << '\n'
                                  << "position of capsule: " << glm::to_string(capsule.m_position) << '\n'
                                  << "offset of capsule: " << capsule_offset << '\n';
                        is_hint = true;
                    }
                }
#endif

                for (auto &&vertex : vertices)
                {
                    vertex += global_position;
                }

                constexpr auto offset = vec3{ 1 };

                TerrainComponent terrComp{ vertices };

                AABB aabb = terrComp.getAABB();
                aabb.expand(offset);

#if 0
                {
                    ;
                    registry.emplace_or_replace<DebugShape<TerrainComponent>>(entity, terrComp);
                }
#endif

                TerrainComponent &comp = m_group.get<TerrainComponent>(entity);

                if (const auto capAABB = capsule.getAABB(); capAABB.overlaps(aabb))
                {
                    comp = terrComp;
                }
                else
                {
                    comp.set_invalid();
                }

            };

            if constexpr (Mt)
            {
                std::for_each(std::execution::par, m_group.begin(), m_group.end(), func);
            }
            else
            {
                std::for_each(std::execution::seq, m_group.begin(), m_group.end(), func);
            }
        }

        static constexpr float g_chunk_size = 0.5;

        using capsule_group_t = entt::basic_group<entt::entity, entt::exclude_t<>, entt::get_t<CapsuleCollider, Offset2>, TerrainComponent>;

        capsule_group_t m_group;

        std::vector<float> m_height_map;

#if 1
        const vec3 m_position = vec3{ -half_chunk_size, 0, -half_chunk_size };
#else
        vec3 m_position = vec3{ 0 };
#endif

        uint16_t m_resolution = 0;
    };

    using TerrainCollisionMt = TerrainCollisionSystem<true>;
    using TerrainCollisionSt = TerrainCollisionSystem<false>;

    template <class Terrain>
    inline void save_vector(const std::string &path, const Terrain &terrain)
    {
        std::ofstream file{ path, std::ios::out | std::ofstream::binary };

        const auto &my_vector = terrain.m_height_map;

        // Read resolution of terrain
        auto res = terrain.m_resolution;
        file.write(reinterpret_cast<const char *>(&res), sizeof(res));

        // Store size of the outer vector
        size_t size = my_vector.size();
        file.write(reinterpret_cast<const char *>(&size), sizeof(size));

        // Now write each vector one by one
        for (auto &v : my_vector)
        {
            // Store its contents
            file.write(reinterpret_cast<const char *>(&v), sizeof(float));
        }
        file.close();
    }

    inline decltype(auto) read_vector(const std::string &path)
    {
        std::ifstream file{ path, std::ios::in | std::ifstream::binary };

        // Read resolution of terrain
        uint16_t res = 0;
        file.read(reinterpret_cast<char *>(&res), sizeof(res));
        assert(res > 0);

        // Read size of the inner vector
        size_t size = 0;
        file.read(reinterpret_cast<char *>(&size), sizeof(size));
        assert(size > 0);

        std::vector<float> myVector;
        myVector.resize(size);

        for (size_t i{ 0 }; i < size; ++i)
        {
            float f = 0;
            file.read(reinterpret_cast<char *>(&f), sizeof(f));
            myVector[i] = f;
        }

        file.close();

        return std::make_tuple(res, myVector);
    }

} // namespace WFL
