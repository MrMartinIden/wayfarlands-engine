#pragma once

#include <array>
#include <cstdint>

namespace WFL
{
    template <typename T, std::size_t N = 2>
    struct ring_array
    {
        typedef typename std::array<T, N>::value_type value_type;
        typedef typename std::array<T, N>::reference reference;
        typedef typename std::array<T, N>::const_reference const_reference;
        typedef typename std::array<T, N>::size_type size_type;

        ~ring_array()
        {
            m_size = 0;
            m_front = 0;
        }

        void push(const T &v)
        {
            if (m_size == N)
            {
                front() = v;
                m_front = (m_front + 1) % N;
            }
            else
            {
                (*this)[m_size] = v;
                ++m_size;
            }
        }

        void pop()
        {
            if (!empty())
            {
                m_front = (m_front + 1) % N;

                --m_size;
            }
        }

        const_reference front() const
        {
            return m_array[m_front];
        }

        reference front()
        {
            return m_array[m_front];
        }

        bool empty() const
        {
            return (m_size == 0);
        }

        size_type size() const
        {
            return m_size;
        }

        const_reference operator[](size_type idx) const
        {
            assert(idx < N);

            return m_array[(m_front + idx) % N];
        }

        reference operator[](size_type idx)
        {
            return m_array[(m_front + idx) % N];
        }

        void dump()
        {
#if 0
            for (size_t i{ 0 }; i < m_size; ++i)
            {
                std::cout << (*this)[i] << ' ';
            }

            std::cout << '\n';
#endif
        }

        std::array<T, N> m_array;
        size_type m_front = 0;
        size_type m_size = 0;
    };
} // namespace WFL
