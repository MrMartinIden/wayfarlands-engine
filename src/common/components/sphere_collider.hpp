#pragma once

#include "collider.hpp"

#include "aabb.hpp"

namespace WFL
{

    struct SphereCollider final : BaseCollider
    {
        vec3::value_type m_radius;

        SphereCollider() = default;

        SphereCollider(const vec3 &p, const quat &q, vec3::value_type radius) noexcept :
                BaseCollider(p, q),
                m_radius(radius)
        {
        }

        vec3 support(vec3 dir) const
        {
            // find support in model space
            dir = glm::cross(dir, m_rotation);

            dir = glm::normalize(dir);

            dir *= m_radius;

            //convert support to world space
            dir = m_position + m_rotation * dir;

            return dir;
        }

        inline AABB getAABB() const
        {
            AABB aabb;
            aabb.m_position = m_position;
            aabb.m_half_size = vec3{ m_radius };

            return aabb;
        }
    };

} // namespace WFL
