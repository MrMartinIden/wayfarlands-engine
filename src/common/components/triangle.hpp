#pragma once

#include "aabb.hpp"

#include <numeric>

#include <glm/gtx/normal.hpp>
#include <glm/gtx/quaternion.hpp>

namespace WFL {

struct Triangle final {
    Triangle(const std::array<vec3, 3>& p_vertices) noexcept
        : m_vertices(p_vertices)
    {
        m_position = getInteriorPoint();

        std::for_each(m_vertices.begin(), m_vertices.end(), [this](vec3& vertex) {
            vertex -= m_position;
        });
    }

    AABB getAABB() const
    {
        AABB aabb;

        constexpr uint8_t dimension = 3;

        for (const auto &v : m_vertices)
        {

            //+ m_position;
            for (uint8_t i = 0; i < dimension; ++i) {
                if (v[i] < -aabb.m_half_size[i]) {
                    aabb.m_half_size[i] = -v[i];
                }

                if (v[i] > aabb.m_half_size[i]) {
                    aabb.m_half_size[i] = v[i];
                }
            }
        }

        return aabb;
    }

    inline vec3 getInteriorPoint() const
    {
        const vec3 sum = std::accumulate(m_vertices.begin(), m_vertices.end(), vec3 { 0 });

        auto interiorPoint = sum * (vec3::value_type { 1.0 } / m_vertices.size());

        return interiorPoint;
    }

    vec3 support(vec3 dir) const
    {
        constexpr quat m_rotation = glm::identity<quat>();

        vec3 p, supVec;

        dir = glm::cross(dir, m_rotation);

        float max = std::numeric_limits<float>::lowest();

        for (const auto& vertex : m_vertices) {
            p = vertex - m_position;

            float projection = glm::dot(dir, p);

            if (projection > max) {
                supVec = vertex;
                max = projection;
            }
        }

        // transform support vertex
        supVec = m_position + m_rotation * supVec;

        return supVec;
    }

    vec3 m_position;
    std::array<vec3, 3> m_vertices;
};
}
