#pragma once

#include "aabb.hpp"

namespace WFL
{
    template <typename T>
    struct DebugShape
    {
        T m_value;

        T &get()
        {
            return m_value;
        }

        const T &get() const
        {
            return m_value;
        }
    };
} // namespace WFL
