#pragma once

#include "constants.hpp"

#include <cstdint>
#include <array>

#include <entt/entt.hpp>

namespace WFL {

    struct PairComponent
    {
        uint16_t m_size = 0;
        std::array<entt::entity, max_contacts> m_entities;

        PairComponent() :
                m_size(0)
        {
        }

        inline bool empty()
        {
            return m_size == 0;
        }

        inline decltype(auto) size() const
        {
            return m_size;
        }

        inline void clear()
        {
            m_size = 0;
        }
    };
}
