#pragma once

#include "glm/glm.hpp"

namespace WFL
{

    //#define USE_DOUBLE

#ifdef USE_DOUBLE
    using vec2 = glm::highp_dvec2;
    using vec3 = glm::highp_dvec3;
    using quat = glm::highp_dquat;
#else
    using vec2 = glm::highp_fvec2;
    using vec3 = glm::highp_fvec3;
    using quat = glm::highp_fquat;
#endif

#undef USE_DOUBLE
} // namespace WFL
