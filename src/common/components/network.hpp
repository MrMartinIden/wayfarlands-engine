#pragma once

#include <glm/gtc/packing.hpp>

#include "config.hpp"

#include <iostream>
#include <memory>

namespace WFL
{
    static inline constexpr auto limit_distance = 2.f; //meter
    static inline constexpr auto limit_squared_distance = limit_distance * limit_distance;

    struct GhostPosition : vec3
    {
    };

    struct PlayerPosition : vec3
    {
        PlayerPosition(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }

        void operator=(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }
    };

    struct SteamID
    {
        std::uint64_t m_id;
    };

    struct StoppedAfterMovement
    {
        bool m_value{ false };
    };

} // namespace WFL
