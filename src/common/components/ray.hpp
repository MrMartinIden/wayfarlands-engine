#pragma once

#include "aabb.hpp"

namespace WFL
{
    struct HitResult
    {
        float m_value{ 0 };
    };

    struct Ray
    {
        vec3 m_origin{ 0 };
        vec3 m_direction{ 1 };
        vec3::value_type m_length{ 1 };

        AABB getAABB() const
        {
            AABB aabb;
            aabb.m_position = mediana();

            const auto far_point = get_far_point();

            const auto max = glm::max(m_origin, far_point);

            aabb.m_half_size = max + vec3{ 0, 1, 0 } - aabb.m_position;

            return aabb;
        }

        inline float triangle_intersection(const vec3 &v0,
                const vec3 &v1,
                const vec3 &v2) const
        {
            const auto &dir = this->m_direction;

            vec3 edge1 = v1 - v0;
            vec3 edge2 = v2 - v0;
            // Вычисление вектора нормали к плоскости
            vec3 pvec = glm::cross(dir, edge2);
            float det = glm::dot(edge1, pvec);

            constexpr auto epsilon = glm::epsilon<float>();

            // Луч параллелен плоскости
            if (det < epsilon && det > -epsilon)
            {
                return 0.0;
            }

            float inv_det = 1 / det;
            vec3 tvec = this->m_origin - v0;
            float u = glm::dot(tvec, pvec) * inv_det;
            if (u < 0.0 || u > 1.0)
            {
                return 0.0;
            }

            vec3 qvec = glm::cross(tvec, edge1);
            float v = glm::dot(dir, qvec) * inv_det;
            if (v < 0.0 || u + v > 1.0)
            {
                return 0.0;
            }

            return glm::dot(edge2, qvec) * inv_det;
        }

        inline bool box_intersect(const vec3 &box_position, const vec3 &box_extent, float &t) const
        {
            vec3 bounds[2] = { box_position - box_extent, box_position + box_extent };

            const auto invdir = vec3{ 1.0 } / m_direction;
            const auto sign = glm::lessThan(invdir, vec3{ 0 });

            vec3 tmin, tmax;
            tmin.x = (bounds[sign[0]].x - m_origin.x) * invdir.x;
            tmax.x = (bounds[1 - sign[0]].x - m_origin.x) * invdir.x;
            tmin.y = (bounds[sign[1]].y - m_origin.y) * invdir.y;
            tmax.y = (bounds[1 - sign[1]].y - m_origin.y) * invdir.y;

            if ((tmin.x > tmax.y) || (tmin.y > tmax.x))
            {
                return false;
            }

            if (tmin.y > tmin.x)
            {
                tmin.x = tmin.y;
            }

            if (tmax.y < tmax.x)
            {
                tmax.x = tmax.y;
            }

            tmin.z = (bounds[sign[2]].z - m_origin.z) * invdir.z;
            tmax.z = (bounds[1 - sign[2]].z - m_origin.z) * invdir.z;

            if ((tmin.x > tmax.z) || (tmin.z > tmax.x))
            {
                return false;
            }

            if (tmin.z > tmin.x)
            {
                tmin.x = tmin.z;
            }

            if (tmax.z < tmax.x)
            {
                tmax.x = tmax.z;
            }

            t = tmin.x;

            if (t < 0)
            {
                t = tmax.x;
                if (t < 0)
                {
                    return false;
                }
            }

            return true;
        }

        inline int sphere_intersect(const vec3 &spherePos, float sphereRadius,
                float &dist1, float &dist2) const
        {
            const auto &rayPos = this->m_origin;
            const vec3 &rayDir = this->m_direction;

            vec3 o_minus_c = rayPos - spherePos;

            float p = glm::dot(rayDir, o_minus_c);
            float q = glm::dot(o_minus_c, o_minus_c) - (sphereRadius * sphereRadius);

            float discriminant = (p * p) - q;
            if (discriminant < 0.0f)
            {
                return 0;
            }

            float dRoot = glm::sqrt(discriminant);
            dist1 = -p - dRoot;
            dist2 = -p + dRoot;

            return (discriminant > 1e-7) ? 2 : 1;
        }

        vec3::value_type half_length() const
        {
            return m_length * vec3::value_type{ 0.5 };
        }

        vec3 mediana() const
        {
            return m_origin + m_direction * half_length();
        }

        vec3 get_far_point() const
        {
            return (m_origin + m_direction * m_length);
        }

        vec3 get_camera_position() const
        {
            return (m_origin + m_direction * (m_length - vec3::value_type{ 0.5 }));
        }
    };
} // namespace WFL
