#pragma once

#include "config.hpp"

namespace WFL
{
    struct SpeedComponent : vec3
    {
        void operator=(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }
    };
} // namespace WFL
