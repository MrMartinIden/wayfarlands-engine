#pragma once

#include "config.hpp"

namespace WFL
{
    inline constexpr auto max_contacts = 15u;

    inline constexpr vec3::value_type player_max_speed = 5.0;

    inline constexpr vec3::value_type player_max_stand_slope = 45.0;

    inline constexpr vec3::value_type gravity = -9.8;

    inline constexpr vec3::value_type fixed_dt = 1 / 30.0;

    inline constexpr vec3::value_type chunk_size = 256;
    inline constexpr vec3::value_type half_chunk_size = chunk_size * vec3::value_type{ 0.5 };

    inline constexpr vec3 global_size = vec3{ 8192 / 2, 256, 8192 / 2 };
    static_assert (global_size.x == global_size.z, "X and Z values of global_size must be equal!");

    inline constexpr uint32_t count_chunk_x = global_size.x / chunk_size;

} // namespace WFL
