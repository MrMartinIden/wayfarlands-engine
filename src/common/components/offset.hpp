#pragma once

#include "constants.hpp"

#include <morton.h>

#include <glm/glm.hpp>

#include <array>
#include <cstdint>

namespace WFL
{
    template <uint8_t Count>
    struct OffsetComponent;

    template <>
    struct OffsetComponent<2>
    {
        OffsetComponent(uint32_t x)
            : m_value(x)
        {
        }

        OffsetComponent() = default;

        uint32_t m_value;

        static inline uint32_t encode(uint16_t offset_x, uint16_t offset_z)
        {
            return libmorton::morton2D_32_encode(offset_x, offset_z);
        }

        static inline std::array<uint16_t, 2> decode(const uint32_t &index)
        {
            std::array<uint_fast16_t, 2> offset;

            libmorton::morton2D_32_decode(index, offset[0], offset[1]);

            return { static_cast<uint16_t>(offset[0]), static_cast<uint16_t>(offset[1]) };
        }

        static inline bool is_valid(const int &offset_x, const int &offset_z)
        {
            return offset_x >= 0 && offset_z >= 0;
        }
    };

    using Offset2 = OffsetComponent<2>;
    using Offset3 = OffsetComponent<3>;
} // namespace WFL
