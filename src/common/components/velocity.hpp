#pragma once

#include "config.hpp"

#include <utility>

namespace WFL
{
    struct VelocityComponent : vec3
    {
        void operator=(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }
    };
} // namespace WFL
