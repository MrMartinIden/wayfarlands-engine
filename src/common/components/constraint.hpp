#pragma once

#include "entt/entt.hpp"

namespace WFL
{
    struct ConstraintComponent
    {
        ConstraintComponent(entt::entity ent) :
                m_value(ent)
        {
        }

        ConstraintComponent() = delete;

        entt::entity m_value;
    };
} // namespace WFL
