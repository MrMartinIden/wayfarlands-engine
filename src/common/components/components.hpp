// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2021 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <entt/entt.hpp>

#include <numeric>
#include <variant>

#include "aabb.hpp"
#include "collider.hpp"
#include "config.hpp"
#include "constants.hpp"
#include "convex.hpp"
#include "id.hpp"
#include "network.hpp"
#include "sphere_collider.hpp"
#include "triangle_mesh.hpp"

#include <glm/gtx/normal.hpp>

namespace WFL
{

    struct CharacterComponent
    {
    };

    struct StaticComponent
    {
    };

    struct PlatformComponent
    {
    };

    struct MovementComponent final
    {     
        uint8_t m_jump : 1;
    };

    struct BoxCollider final : BaseCollider
    {
        vec3 m_extent;

        BoxCollider() = default;

        BoxCollider(const vec3 &p, const quat &q, const vec3 &extent) noexcept :
                BaseCollider(p, q), m_extent(extent)
        {
        }

        vec3 support(vec3 dir) const
        {
            dir = glm::cross(dir, m_rotation);

            vec3 supVec = glm::sign(dir) * m_extent;

            supVec = m_position + m_rotation * supVec;

            return supVec;
        }

        inline AABB getAABB() const
        {
            constexpr vec3 x{ 1, 0, 0 };
            constexpr vec3 y{ 0, 1, 0 };
            constexpr vec3 z{ 0, 0, 1 };

            vec3 min{ support(-x).x, support(-y).y, support(-z).z };
            vec3 max{ support(x).x, support(y).y, support(z).z };

            {
                auto v0 = m_position - min;
                auto v1 = max - m_position;
                assert(glm::all(glm::epsilonEqual(v0, v1, 0.0001f)));
            }

            AABB aabb;
            aabb.m_position = m_position;
            aabb.m_half_size = m_position - min;

            assert(glm::all(glm::lessThanEqual(aabb.min(), aabb.max())));

            return aabb;
        }
    };



    struct Tetrahedron final
    {
        Tetrahedron(std::array<vec3, 4> m_vertices) noexcept
                :
                m_vertices(m_vertices)
        {
            m_position = getInteriorPoint();
        }

        Tetrahedron() = delete;

        AABB getAABB()
        {
            AABB aabb;

            constexpr auto vertex_count = 3u;
            constexpr auto dimension = 3u;

            for (auto i = 0u; i < vertex_count; ++i)
            {
                auto &vertex = m_vertices[i];
                for (auto j = 0u; j < dimension; ++j)
                {
                    const auto v = vertex[j] + aabb.position()[j];

                    if (v < -aabb.m_half_size[j])
                    {
                        aabb.m_half_size[j] = -v;
                    }

                    if (v > aabb.m_half_size[j])
                    {
                        aabb.m_half_size[j] = v;
                    }
                }
            }

            return aabb;
        }

        inline vec3 getInteriorPoint() const
        {
            // Compute an interior point. We're computing the mean point and
            // *not* some alternative such as the centroid or bounding box
            // center.

            vec3 sum = std::accumulate(m_vertices.begin(), m_vertices.end(), vec3{ 0 });

            return sum * (vec3::value_type{ 1.0 } / m_vertices.size());
        }

        vec3 support(vec3 dir) const
        {
            vec3 p, supVec;
            float projection;
            const auto center = getInteriorPoint();

            float max = std::numeric_limits<float>::lowest();

            for (const auto &vertex : m_vertices)
            {
                p = vertex - center;

                projection = glm::dot(dir, p);

                if (projection > max)
                {
                    supVec = vertex;
                    max = projection;
                }
            }

            // transform support vertex
            return supVec;
        }

        std::array<vec3, 4> m_vertices;
        vec3 m_position;
    };

    using StaticCollider = std::variant<BoxCollider, SphereCollider, Convex, TriangleMesh, Triangle>;

} // namespace WFL
