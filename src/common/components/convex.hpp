#pragma once

#include "aabb.hpp"
#include "collider.hpp"

#include <algorithm>
#include <numeric>
#include <vector>

namespace WFL
{

    struct Convex final : BaseCollider
    {
        Convex() = default;

        Convex(vec3 &p, quat &q,
                std::vector<vec3> &&vertices) noexcept
                :
                BaseCollider(p, q), m_vertices(vertices)
        {
            {
                constexpr float maxNum = std::numeric_limits<float>::max();
                m_min = vec3{ maxNum };
                for (const auto &v : m_vertices)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        if (v[i] < m_min[i])
                        {
                            m_min[i] = v[i];
                        }
                    }
                }

                m_min += m_position;
            }

            {
                constexpr float minNum = std::numeric_limits<float>::lowest();
                m_max = vec3{ minNum };
                for (const auto &v : m_vertices)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        if (v[i] > m_max[i])
                        {
                            m_max[i] = v[i];
                        }
                    }
                }

                m_max += m_position;
            }

            const vec3 sum = std::accumulate(vertices.begin(), vertices.end(), vec3{ 0 });

            m_interior_point = sum * (vec3::value_type{ 1.0 } / m_vertices.size());
            m_interior_point += m_position;
        }

        Convex(const std::vector<vec3> &vertices) noexcept
                :
                BaseCollider(), m_vertices(vertices)
        {
            {
                const vec3 sum = std::accumulate(vertices.begin(), vertices.end(), vec3{ 0 });
                m_position = sum * (vec3::value_type{ 1.0 } / m_vertices.size());
            }

            std::for_each(m_vertices.begin(), m_vertices.end(), [this](vec3 &vertex) {
                vertex = vertex - m_position;
            });

            {
                constexpr float maxNum = std::numeric_limits<float>::max();
                m_min = vec3{ maxNum };
                for (const auto &v : m_vertices)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        if (v[i] < m_min[i])
                        {
                            m_min[i] = v[i];
                        }
                    }
                }

                m_min += m_position;
            }

            {
                constexpr float minNum = std::numeric_limits<float>::lowest();
                m_max = vec3{ minNum };
                for (const auto &v : m_vertices)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        if (v[i] > m_max[i])
                        {
                            m_max[i] = v[i];
                        }
                    }
                }

                m_max += m_position;
            }

            m_interior_point = m_position;
        }

        std::vector<vec3> m_vertices;
        vec3 m_min, m_max;
        vec3 m_interior_point;

        inline AABB getAABB() const
        {
            AABB aabb;
            aabb.m_position = m_position;
            aabb.m_half_size = m_position - m_min;
            aabb.m_half_size = m_max - m_position;

            return aabb;
        }

        inline vec3 getInteriorPoint() const { return m_interior_point; }

        vec3 support(vec3 dir) const
        {
            vec3 p, supVec;
            float max, projection;
            const auto &center = getInteriorPoint();

            dir = glm::cross(dir, m_rotation);

            max = std::numeric_limits<float>::lowest();

            for (const auto &vertex : m_vertices)
            {
                p = vertex - center;

                projection = glm::dot(dir, p);

                if (projection > max)
                {
                    supVec = vertex;
                    max = projection;
                }
            }

            // transform support vertex
            supVec = m_position + m_rotation * supVec;
            return supVec;
        }
    };
} // namespace WFL
