#pragma once

#include <array>
#include <cstdint>
#include <numeric>

namespace WFL
{
    struct id_component
    {
        id_component() = delete;

        id_component(uint16_t value) :
                m_value(value)
        {
        }

        uint16_t m_value;
    };

    inline constexpr auto get_ids()
    {
        std::array<uint16_t, 100> arr{};

        std::iota(arr.begin(), arr.end(), 0);

        return arr;
    }

    inline constexpr auto character_ids = get_ids();
} // namespace WFL
