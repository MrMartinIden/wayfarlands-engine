#pragma once

#include "aabb.hpp"
#include "config.hpp"

#include <glm/gtx/normal.hpp>

#include <algorithm>
#include <numeric>
#include <vector>

namespace WFL
{
    class TerrainComponent final
    {
    public:
        struct Convex
        {
            Convex(const std::array<vec3, 6> &vertices) noexcept
                    :
                    m_vertices(vertices)
            {
                m_position = getInteriorPoint();

                std::for_each(m_vertices.begin(), m_vertices.end(), [this](vec3 &vertex) {
                    vertex -= m_position;
                });
            }

            std::array<vec3, 6> m_vertices;
            vec3 m_position;

            inline vec3 getInteriorPoint() const
            {
                const vec3 sum = std::accumulate(m_vertices.begin(), m_vertices.end(), vec3{ 0 });
                auto position = sum * (vec3::value_type{ 1.0 } / m_vertices.size());

                return position;
            }

            vec3 support(vec3 dir) const
            {
                constexpr quat m_rotation = glm::identity<quat>();

                vec3 p, supVec;
                float projection;
                const auto center = getInteriorPoint();

                dir = glm::cross(dir, m_rotation);

                float max = std::numeric_limits<float>::lowest();

                for (const auto &vertex : m_vertices)
                {
                    p = vertex - center;

                    projection = glm::dot(dir, p);

                    if (projection > max)
                    {
                        supVec = vertex;
                        max = projection;
                    }
                }

                // transform support vertex
                supVec = m_position + m_rotation * supVec;
                return supVec;
            }
        };

        TerrainComponent(const std::array<vec3, 4> &p_vertices) noexcept
        {
            std::copy(p_vertices.begin(), p_vertices.end(), m_top_vertices.begin());

            constexpr auto depth = 1.0f;

            const auto normal = glm::triangleNormal(m_top_vertices[0], m_top_vertices[1], m_top_vertices[2]);

            m_bottom_vertices[0] = m_top_vertices[0] + depth * normal;
            m_bottom_vertices[1] = m_top_vertices[1] + depth * normal;
            m_bottom_vertices[2] = m_top_vertices[2] + depth * normal;
            m_bottom_vertices[3] = m_top_vertices[3] + depth * normal;
        }

        TerrainComponent()
        {
            set_invalid();
        };

        std::array<Convex, 2> get_shapes() const
        {
            std::array<Convex, 2> shapes{ std::array<vec3, 6>{ m_top_vertices[0], m_top_vertices[1], m_top_vertices[2],
                                                  m_bottom_vertices[0], m_bottom_vertices[1], m_bottom_vertices[2] },
                std::array<vec3, 6>{ m_top_vertices[1], m_top_vertices[2], m_top_vertices[3],
                        m_bottom_vertices[1], m_bottom_vertices[2], m_bottom_vertices[3] } };

            return shapes;
        }

        inline vec3 getInteriorPoint() const
        {
            // Compute an interior point. We're computing the mean point and
            // *not* some alternative such as the centroid or bounding box
            // center.

            const vec3 sum = std::accumulate(m_top_vertices.begin(), m_top_vertices.end(), vec3{ 0 }) +
                             std::accumulate(m_bottom_vertices.begin(), m_bottom_vertices.end(), vec3{ 0 });

            vec3 interiorPoint = sum * (vec3::value_type{ 1.0 } / 8);

            return interiorPoint;
        }

        AABB getAABB() const
        {
            constexpr uint8_t dimension = 3;

            vec3 min = m_top_vertices.front();
            vec3 max = min;
            std::for_each(m_top_vertices.cbegin() + 1, m_top_vertices.cend(), [&](auto &vertex) {
                for (uint8_t i = 0; i < dimension; ++i)
                {
                    if (std::isgreater(vertex[i], max[i]))
                    {
                        max[i] = vertex[i];
                    }

                    if (std::isless(vertex[i], min[i]))
                    {
                        min[i] = vertex[i];
                    }
                }
            });

            std::for_each(m_bottom_vertices.cbegin(), m_bottom_vertices.cend(), [&](auto &vertex) {
                for (uint8_t i = 0; i < dimension; ++i)
                {
                    assert(!std::isnan(vertex[i]));

                    if (std::isgreater(vertex[i], max[i]))
                    {
                        max[i] = vertex[i];
                    }

                    if (std::isless(vertex[i], min[i]))
                    {
                        min[i] = vertex[i];
                    }
                }
            });

            AABB aabb;
            aabb.m_half_size = (max - min) * vec3::value_type{ 0.5 };
            aabb.m_position = max - aabb.m_half_size;

            assert(!glm::any(glm::isnan(aabb.m_position)));
            assert(!glm::any(glm::isnan(aabb.m_half_size)));

            return aabb;
        }

        bool is_valid() const
        {
            return m_top_vertices[0].x != std::numeric_limits<vec3::value_type>::lowest();
        }

        void set_invalid()
        {
            m_top_vertices[0].x = std::numeric_limits<vec3::value_type>::lowest();
        }

    private:
        std::array<vec3, 4> m_top_vertices;
        std::array<vec3, 4> m_bottom_vertices;
    };

    struct DynamicTerrainComponent final
    {
        struct Convex
        {
            std::array<vec3, 6> m_vertices;
            vec3 m_position;

            Convex(const std::array<vec3, 6> &vertices) noexcept
                    :
                    m_vertices(vertices)
            {
                m_position = getInteriorPoint();

                std::for_each(m_vertices.begin(), m_vertices.end(), [this](vec3 &vertex) {
                    vertex -= m_position;
                });
            }

            inline vec3 getInteriorPoint() const
            {
                const vec3 sum = std::accumulate(m_vertices.begin(), m_vertices.end(), vec3{ 0 });
                auto position = sum * (vec3::value_type{ 1.0 } / m_vertices.size());

                return position;
            }

            vec3 support(vec3 dir) const
            {
                constexpr quat m_rotation = glm::identity<quat>();

                vec3 p, supVec;
                float projection;
                const auto center = getInteriorPoint();

                dir = glm::cross(dir, m_rotation);

                float max = std::numeric_limits<float>::lowest();

                for (const auto &vertex : m_vertices)
                {
                    p = vertex - center;

                    projection = glm::dot(dir, p);

                    if (projection > max)
                    {
                        supVec = vertex;
                        max = projection;
                    }
                }

                // transform support vertex
                supVec = m_position + m_rotation * supVec;
                return supVec;
            }
        };

        DynamicTerrainComponent(std::vector<vec3> &&p_vertices) noexcept
                :
                m_top_vertices(p_vertices)
        {

            assert(m_top_vertices.size() > 0);

            constexpr auto depth = 1.0f;

            m_bottom_vertices.resize(m_top_vertices.size());

            for (size_t i = 0; i < m_top_vertices.size(); i += 4)
            {
                auto normal = glm::triangleNormal(m_top_vertices[i], m_top_vertices[i + 1], m_top_vertices[i + 2]);

                auto v = depth * normal;

                m_bottom_vertices[i] = m_top_vertices[i] + v;
                m_bottom_vertices[i + 1] = m_top_vertices[i + 1] + v;

                normal = glm::triangleNormal(m_top_vertices[i + 1], m_top_vertices[i + 2], m_top_vertices[i + 3]);
                v = depth * normal;

                m_bottom_vertices[i + 2] = m_top_vertices[i + 2] + v;
                m_bottom_vertices[i + 3] = m_top_vertices[i + 3] + v;
            }
        }

        DynamicTerrainComponent()
        {

            set_invalid();
        };

        decltype(auto) get_shapes() const
        {
            std::vector<std::array<Convex, 2>> shapes;

            if (m_top_vertices.empty())
            {
                return shapes;
            }

            shapes.reserve(m_top_vertices.size());

            for (size_t i = 0; i < m_top_vertices.size(); i += 4)
            {
                Convex convex0{ std::array<vec3, 6>{ m_top_vertices[i], m_top_vertices[i + 1], m_top_vertices[i + 2],
                        m_bottom_vertices[i], m_bottom_vertices[i + 1], m_bottom_vertices[i + 2] } };

                Convex convex1{ std::array<vec3, 6>{ m_top_vertices[i + 1], m_top_vertices[i + 2], m_top_vertices[i + 3],
                        m_bottom_vertices[i + 1], m_bottom_vertices[i + 2], m_bottom_vertices[i + 3] } };

                shapes.push_back({ convex0, convex1 });
            }

            return shapes;
        }

        inline vec3 getInteriorPoint() const
        {
            // Compute an interior point. We're computing the mean point and
            // *not* some alternative such as the centroid or bounding box
            // center.

            const vec3 sum = std::accumulate(m_top_vertices.begin(), m_top_vertices.end(), vec3{ 0 }) +
                             std::accumulate(m_bottom_vertices.begin(), m_bottom_vertices.end(), vec3{ 0 });

            vec3 interiorPoint = sum * (vec3::value_type{ 1.0 } / 8);

            return interiorPoint;
        }

        AABB getAABB() const
        {
            if (m_top_vertices.empty())
            {
                return {};
            }

            constexpr uint8_t dimension = 3;

            vec3 min = m_top_vertices.front();
            vec3 max = min;
            std::for_each(m_top_vertices.cbegin() + 1, m_top_vertices.cend(), [&](auto &vertex) {
                for (uint8_t i = 0; i < dimension; ++i)
                {
                    if (std::isgreater(vertex[i], max[i]))
                    {
                        max[i] = vertex[i];
                    }

                    if (std::isless(vertex[i], min[i]))
                    {
                        min[i] = vertex[i];
                    }
                }
            });

            std::for_each(m_bottom_vertices.cbegin(), m_bottom_vertices.cend(), [&](auto &vertex) {
                for (uint8_t i = 0; i < dimension; ++i)
                {
                    assert(!std::isnan(vertex[i]));

                    if (std::isgreater(vertex[i], max[i]))
                    {
                        max[i] = vertex[i];
                    }

                    if (std::isless(vertex[i], min[i]))
                    {
                        min[i] = vertex[i];
                    }
                }
            });

            AABB aabb;
            aabb.m_half_size = (max - min) * vec3::value_type{ 0.5 };
            aabb.m_position = max - aabb.m_half_size;

            assert(!glm::any(glm::isnan(aabb.m_position)));
            assert(!glm::any(glm::isnan(aabb.m_half_size)));

            return aabb;
        }

        bool is_valid() const
        {
            assert(!m_top_vertices.empty());

            return m_top_vertices.front().x != std::numeric_limits<vec3::value_type>::lowest();
        }

        void set_invalid()
        {
            m_top_vertices.resize(1);

            m_top_vertices.front().x = std::numeric_limits<vec3::value_type>::lowest();
        }

        bool empty()
        {
            return m_top_vertices.empty();
        }

        std::vector<vec3> m_top_vertices;
        std::vector<vec3> m_bottom_vertices;
    };

} // namespace WFL
