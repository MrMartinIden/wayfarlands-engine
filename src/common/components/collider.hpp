#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include "config.hpp"

namespace WFL
{
    struct BaseCollider
    {
        vec3 m_position = vec3{ 0 };
        quat m_rotation = glm::identity<quat>();

        BaseCollider() = default;

        BaseCollider(const vec3 &p, const quat &q) noexcept :
                m_position(p),
                m_rotation(q)
        {
        }
    };
} // namespace WFL
