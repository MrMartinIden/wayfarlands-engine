#pragma once

#include "collider.hpp"
#include "triangle.hpp"

#include "aabb.hpp"

#include <vector>

namespace WFL {

struct TriangleMesh final : BaseCollider {
    TriangleMesh(vec3 p, quat q,
        std::vector<std::array<vec3, 3>>&& triangles) noexcept
        : BaseCollider(p, q)
        , m_triangles(triangles)
    {
        calculateAABB();
    }

    TriangleMesh() = default;

    void calculateAABB()
    {
        constexpr auto dimension = 3u;

        for (const auto& triangle : m_triangles) {
            for (const auto& vertex : triangle) {

                for (auto i { 0u }; i < dimension; ++i) {
                    if (vertex[i] < m_min[i]) {
                        m_min[i] = vertex[i];
                    }

                    if (vertex[i] > m_max[i]) {
                        m_max[i] = vertex[i];
                    }
                }
            }
        }

        m_min += m_position;
        m_max += m_position;
    }

    inline Triangle get_triangle(uint32_t idx)
    {
        auto tr = m_triangles[idx];

        for (auto& vertex : tr) {
            vertex += m_position;
        }

        Triangle triangle { tr };
        return triangle;
    }

    inline decltype(auto) count_triangle()
    {
        return m_triangles.size();
    }

    inline AABB getAABB() const
    {
        AABB aabb;
        aabb.m_half_size = (m_max - m_min) * vec3::value_type { 0.5 };
        aabb.m_position = m_max - aabb.m_half_size;

        return aabb;
    }

    std::vector<std::array<vec3, 3>> m_triangles;

    vec3 m_min { std::numeric_limits<float>::max() };
    vec3 m_max { std::numeric_limits<float>::lowest() };
};

}
