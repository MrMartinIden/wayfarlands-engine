#pragma once

#include "config.hpp"

namespace WFL
{
    struct Direction : vec3
    {
        Direction() :
                vec3{ 1, 0, 0 }
        {
        }

        void operator=(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }
    };

    struct DirectionMovement : vec3
    {
        void operator=(const vec3 &v)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
        }
    };
} // namespace WFL
