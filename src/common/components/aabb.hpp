#pragma once

#include "config.hpp"

#include "glm/gtx/string_cast.hpp"

#include <ostream>
#include <tuple>

namespace WFL {

    struct AABB
    {
        AABB() = default;

        constexpr AABB(const vec3 &pos, const vec3 &half_size) :
                m_position(pos),
                m_half_size(half_size) {}

        bool operator==(const AABB& b) const
        {
            return ((b.m_position == m_position) && (b.m_half_size == m_half_size));
        }

        bool operator!=(const AABB &b) const
        {
            return (!(*this == b));
        }

        inline decltype(auto) min_max() const { return std::make_tuple(m_position - m_half_size, m_position + m_half_size); }

        inline vec3 min() const { return (m_position - m_half_size); }
        inline vec3 max() const { return (m_position + m_half_size); }

        inline vec3& position() { return m_position; }
        inline const vec3 &position() const { return m_position; }

        /**
         * @brief Returns the vector along the box diagonal from the minimum
         * point to the maximum point.
         * @return
         */
        inline constexpr vec3 diagonal() const { return m_half_size * vec3::value_type{ 2.0 }; }

        AABB extend(const AABB &b) const
        {
            AABB aabb;
            aabb.m_half_size = position() - glm::min(min(), b.min());
            aabb.m_half_size = glm::max(max(), b.max()) - position();
            return aabb;
        }

        inline AABB expand(const vec3 &v) const
        {
            AABB aabb = *this;
            aabb.m_half_size += v;

            return aabb;
        }

        inline bool overlaps(const AABB &b) const
        {
            bool result = glm::all(glm::greaterThanEqual(this->max(), b.min())) &&
                       glm::all(glm::lessThanEqual(this->min(), b.max()));

            return result;
        }

        inline bool is_inside(const AABB &b) const
        {
            bool result = glm::all(glm::lessThanEqual(b.max(), this->max())) &&
                       glm::all(glm::greaterThanEqual(b.min(), this->min()));

            return result;
        }

        inline bool is_inside(const vec3 &point) const
        {
            bool result = glm::all(glm::lessThanEqual(point, this->max())) &&
                       glm::all(glm::greaterThanEqual(point, this->min()));

            return result;
        }

        /**
         * @brief Returns the continuous position of a point relative to the
         * corners of the box, where a point at the minimum corner has offset
         * (0,0,0), a point at the maximum corner has offset (1,1,1), and so
         * forth.
         * @param p A valid position vector.
         *
         *
         */
        template <typename T>
        inline constexpr vec3 offset(T &&p) const
        {
            auto temp = std::forward<T>(p);

            assert(is_inside(temp));

            vec3 result = temp - min();

            const auto delta = diagonal();

#if 0
            for (uint32_t i {0}; i < 3; ++i)
            {
                if (auto& value = delta[i]; value > 0)
                {
                    result[i] /= value;
                }
            }
#else
            result /= delta;
#endif

            return result;
        }

        friend std::ostream &operator<<(std::ostream &os, const AABB &b)
        {
            os << glm::to_string(b.min()) << ", " << glm::to_string(b.max());
            return os;
        }

        vec3 m_position;
        vec3 m_half_size;
    };

}
