#pragma once

#include <cstdint>

namespace WFL
{

    struct MortonPrimitive
    {
        uint64_t m_value;

        MortonPrimitive() = default;

        MortonPrimitive(uint64_t p_value) :
                m_value(p_value)
        {
        }
    };

} // namespace WFL
