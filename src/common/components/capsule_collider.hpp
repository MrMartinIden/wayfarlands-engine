#pragma once

#include "collider.hpp"

#include "aabb.hpp"

namespace WFL
{

    struct CapsuleCollider final : BaseCollider
    {
        enum struct Axis : uint8_t
        {
            X,
            Y,
            Z
        };

        static constexpr uint8_t up_axis = static_cast<uint8_t>(Axis::Y);

        vec3::value_type m_radius;
        vec3::value_type m_halfHeight;

        CapsuleCollider() = default;

        CapsuleCollider(const vec3 &p, const quat &q, vec3::value_type radius, vec3::value_type halfHeight) noexcept :
                BaseCollider(p, q),
                m_radius(radius),
                m_halfHeight(halfHeight)
        {
        }

        vec3 support(vec3 dir) const
        {
#if 0
            // find support in model space
            dir = glm::cross(dir, m_rotation);

            vec3 supVec = glm::normalize(dir);

            supVec *= vec3::value_type{ m_radius };

            vec3 highestPoint = { 0, m_halfHeight, 0 };
            vec3 lowestPoint = { 0, -m_halfHeight, 0 };

            highestPoint += supVec;
            lowestPoint += supVec;

            supVec = (dir[up_axis] > 0) ? highestPoint : lowestPoint;

            //convert support to world space
            supVec = m_position + m_rotation * supVec;

            return supVec;
#else
            // find support in model space
            dir = glm::cross(dir, m_rotation);

            dir = glm::normalize(dir);

            dir *= vec3::value_type{ m_radius };

            vec3 highestPoint = { 0, m_halfHeight, 0 };
            vec3 lowestPoint = { 0, -m_halfHeight, 0 };

            highestPoint += dir;
            lowestPoint += dir;

            dir = (dir[up_axis] >= 0) ? highestPoint : lowestPoint;

            //convert support to world space
            dir = m_position + m_rotation * dir;

            return dir;
#endif
        }

        inline AABB getAABB() const
        {
            AABB aabb;
            aabb.m_position = m_position;
            aabb.m_half_size = vec3{ m_radius, m_halfHeight + m_radius, m_radius };

            return aabb;
        }
    };
} // namespace WFL
