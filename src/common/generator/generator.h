#pragma once

#include <utility.hpp>

#ifdef ENTT_ID_TYPE
static_assert(std::is_same_v<ENTT_ID_TYPE, std::uint64_t>, "ENTT_ID_TYPE must be uint64_t.");
#endif

namespace WFL
{
    static constexpr size_t default_max_character = 10000u;

    size_t max_character = default_max_character;

    inline constexpr auto number_of_pairs = 5;

    template <bool use_local = true>
    void generate_entities(size_t count, entt::registry &registry)
    {
        registry.reserve<DirectionMovement>(max_character);
        registry.reserve<CharacterComponent>(max_character);
        registry.reserve<MovementComponent>(max_character);

        registry.reserve<VelocityComponent>(max_character);
        registry.reserve<SpeedComponent>(max_character);
        registry.reserve<CapsuleCollider>(max_character);
        registry.reserve<TerrainComponent>(max_character);

        registry.reserve<StaticComponent>(count);
        registry.reserve<StaticCollider>(count);

        registry.reserve<AABB>(count + max_character);
        registry.reserve<MortonPrimitive>(count + max_character);

        assert(count > max_character);

        const uint32_t rows = glm::sqrt(max_character);
        assert(rows != 0);

        const auto chunk_size = (global_size.x * vec3::value_type { 0.8f }) / static_cast<vec3::value_type>(rows);

        auto spawn_capsules = [&](entt::registry& registry) {
            vec2 pos {};

            for (uint16_t i = 0; i < max_character; ++i)
            {
                pos = vec2{ static_cast<float>(i % rows), static_cast<float>(i / rows) };

                pos *= chunk_size;

                pos += vec2 { chunk_size } * vec2::value_type { 0.5 };

                vec3 position{ pos.x, 3, pos.y };

                if constexpr (use_local)
                {
                    auto [local_pos, offset] = to_local(position);

                    spawn_capsule(registry, Offset2::encode(offset[0], offset[1]), local_pos, 1.0f, 2.0f);
                }
                else
                {
                    spawn_capsule(registry, position, 1.0f, 2.0f);
                }
            }
        };

        auto spawn_statics = [&](entt::registry& registry, size_t size) {
            assert(size >= max_character * number_of_pairs);

            vec2 pos{};

            for (size_t k = 0; k < number_of_pairs; ++k)
            {
                for (size_t i = 0; i < max_character; ++i)
                {
                    pos = vec2{ static_cast<float>(i % rows), static_cast<float>(i / rows) };

                    pos *= chunk_size;

                    pos += vec2 { chunk_size } * vec2::value_type { 0.5 };

                    vec3 position{ pos.x, 2.f, pos.y };

                    if constexpr (use_local)
                    {
                        auto [local_pos, offset] = to_local(position);
                        spawn_static_box(registry, Offset2::encode(offset[0], offset[1]), local_pos, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });
                    }
                    else
                    {
                        spawn_box(registry, position, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });
                    }
                }
            }

            for (size_t i = 0; i < size - max_character * number_of_pairs; ++i)
            {
                vec3 position{ pos.x, half_chunk_size - 2, pos.y };

                if constexpr (use_local)
                {
                    auto [local_pos, offset] = to_local(position);
                    spawn_static_box(registry, Offset2::encode(offset[0], offset[1]), local_pos, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });
                }
                else
                {
                    spawn_box(registry, position, glm::identity<glm::quat>(), vec3{ 2, 3, 2 });
                }

            }
        };

        spawn_capsules(registry);
        spawn_statics(registry, count - max_character);
    }

} // namespace WFL
