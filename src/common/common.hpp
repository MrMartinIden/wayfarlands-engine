#pragma once

#include <cstdint>
#include <iostream>

#include "components/config.hpp"
#include "utility.hpp"

#include <common.h>
#include <shared.h>
#include <yojimbo.h>

#include <entt/entt.hpp>

namespace WFL
{

    struct EntitySteamID
    {
        uint64_t m_steam_id = static_cast<uint64_t>(-1);
        entt::entity m_entity{ entt::null };

        EntitySteamID() = default;

        EntitySteamID(uint64_t id,
                entt::entity entity) :
                m_steam_id(id), m_entity(entity) {}
    };

    [[maybe_unused]] static inline EntitySteamID array_entities[yojimbo::MaxClients];

    class GameAdapter : public yojimbo::Adapter
    {

    public:
        entt::registry *m_registry{ nullptr };
        yojimbo::Server *m_server{ nullptr };

        yojimbo::MessageFactory *CreateMessageFactory(yojimbo::Allocator &allocator) override
        {
            return YOJIMBO_NEW(allocator, yojimbo::TestMessageFactory, allocator);
        }

        void OnServerClientConnected(int clientIndex) override
        {
            std::cout << "connected client with id " << clientIndex << '\n';
#if 1
            assert(m_registry != nullptr);
            const auto position = vec3{ 10 };
            auto entity = WFL::spawn_capsule(*m_registry, 0, position);

            uint64_t clientId;
            yojimbo::random_bytes((uint8_t *)&clientId, 8);

            array_entities[clientIndex] = { clientId, entity };

#define ENABLE_SPAWN_PLAYERS_GHOST 0
#if ENABLE_SPAWN_PLAYERS_GHOST
            static constexpr auto num_players = yojimbo::PositionPlayersMessage::num_players;

            for (size_t i{ 0 }; i < num_players; ++i)
            {
                auto ent = WFL::spawn_capsule(*m_registry, 0, position);

                auto &direction = m_registry->get<Direction>(ent);

                if (i == 1)
                {
                    direction.x = 1;
                }
                else
                {
                    direction.z = 1;
                }
            }
#endif

            assert(m_server != nullptr);

            if (!m_server->CanSendMessage(clientIndex, yojimbo::RELIABLE_ORDERED_CHANNEL))
            {
                return;
            }

            yojimbo::TestConnectedMessage *reliable_message = (yojimbo::TestConnectedMessage *)m_server->CreateMessage(clientIndex, yojimbo::TEST_MESSAGE_CONNECTED);
            if (reliable_message != nullptr)
            {
                reliable_message->m_position = { position[0], position[1], position[2] };

                //unreliable_message->sequence = sequence++;
                printf("Sending connected message\n");
                m_server->SendMessage(clientIndex, yojimbo::RELIABLE_ORDERED_CHANNEL, reliable_message);
            }
#endif
        }

        void OnServerClientDisconnected(int clientIndex) override
        {
            std::cout << "disconnected client with id " << clientIndex << '\n';
#if 1
            assert(m_registry != nullptr);

            auto entity_steam_id = array_entities[clientIndex];
            m_registry->destroy(entity_steam_id.m_entity);

            entity_steam_id = {};
#endif
        }
    };

    static GameAdapter game_adapter;
} // namespace WFL
