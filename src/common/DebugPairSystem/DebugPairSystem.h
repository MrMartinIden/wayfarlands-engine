#pragma once

#include <pair.hpp>

namespace WFL
{
    struct DebugPairSystem
    {
        uint32_t debug(entt::registry &reg)
        {
            uint32_t countPair = 0;

            auto pair_view = reg.view<PairComponent>();

            pair_view.each([&countPair](PairComponent &pair) {
                if (pair.empty())
                {
                    return;
                }

                countPair += pair.m_size;
            });

            return countPair;
        }
    };
} // namespace WFL
