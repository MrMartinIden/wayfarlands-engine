#pragma once

#include <capsule_collider.hpp>
#include <components.hpp>
#include <direction.hpp>
#include <morton_code.hpp>
#include <offset.hpp>
#include <pair.hpp>
#include <ray.hpp>
#include <speed.hpp>
#include <terrain.hpp>
#include <velocity.hpp>

#include <glm/gtx/string_cast.hpp>

#include <bit>
#include <fstream>

namespace WFL
{

    inline decltype(auto) to_local(const vec3 &global_pos)
    {
        vec2 vec2_pos = vec2{ global_pos.x, global_pos.z };

        assert(glm::all(glm::greaterThanEqual(vec2_pos, vec2{ 0 })));

        const glm::vec2 offset = glm::floor(vec2_pos / chunk_size);

        assert(glm::all(glm::lessThan(offset, vec2{ count_chunk_x })) && "Offset must be less count of chunks");
        assert(glm::all(glm::greaterThanEqual(offset, vec2{ 0 })) && "Offset must be greater 0");

        assert(glm::all(glm::lessThanEqual(offset, vec2_pos)));

        vec2_pos = vec2_pos - offset * chunk_size - half_chunk_size;

        const auto pos = vec3{ vec2_pos[0], global_pos.y, vec2_pos[1] };

        return std::make_tuple(pos, std::array<int, 2>{ static_cast<int>(offset[0]), static_cast<int>(offset[1]) });
    }

    inline vec3 to_global(const vec3 &local_pos, const uint32_t &index)
    {

        auto [offset_x, offset_z] = Offset2::decode(index);
        const auto pos = vec3{ local_pos.x + half_chunk_size + offset_x * chunk_size,
            local_pos.y,
            local_pos.z + half_chunk_size + offset_z * chunk_size };

        return pos;
    }

    inline AABB to_global_aabb (const AABB& aabb, const uint32_t& character_offset, const uint32_t& static_offset)
    {
        if (character_offset == static_offset)
        {
            return aabb;
        }

        auto [offset_x0, offset_z0] = Offset2::decode(character_offset);

        auto [offset_x1, offset_z1] = Offset2::decode(static_offset);

        assert(offset_x0 < count_chunk_x && offset_z0 < count_chunk_x);
        assert(offset_x1 < count_chunk_x && offset_z1 < count_chunk_x);

        AABB result = aabb;
        {
            int delta_offset_x = static_cast<int>(offset_x1) - offset_x0;

            result.position().x += delta_offset_x * chunk_size;
        }

        {
            int delta_offset_z = static_cast<int>(offset_z1) - offset_z0;

            result.position().z += delta_offset_z * chunk_size;
        }

        assert(glm::all(glm::epsilonEqual(result.diagonal(), aabb.diagonal(), glm::epsilon<vec3::value_type>())));

        return result;
    }

    inline vec3 to_global_pos(const vec3 &pos, const uint32_t &character_offset, const uint32_t &static_offset)
    {
        if (character_offset == static_offset)
        {
            return pos;
        }

        auto [offset_x0, offset_z0] = Offset2::decode(character_offset);

        auto [offset_x1, offset_z1] = Offset2::decode(static_offset);

        assert(offset_x0 < count_chunk_x && offset_z0 < count_chunk_x);
        assert(offset_x1 < count_chunk_x && offset_z1 < count_chunk_x);

        vec3 result = pos;
        {
            int delta_offset_x = static_cast<int>(offset_x1) - offset_x0;

            result.x += delta_offset_x * chunk_size;
        }

        {
            int delta_offset_z = static_cast<int>(offset_z1) - offset_z0;

            result.z += delta_offset_z * chunk_size;
        }

        return result;
    }

    inline entt::entity spawn_capsule(entt::registry &registry, const vec3 &position, float radius = 0.5,
            float halfHeight = 1.0)
    {
        const auto entity = registry.create();

        const quat rotation = glm::identity<quat>();
        CapsuleCollider capsule{ position, rotation, radius, halfHeight - radius };

        registry.emplace<CapsuleCollider>(entity, capsule);
        registry.emplace<AABB>(entity, capsule.getAABB());
        registry.emplace<MortonPrimitive>(entity);

        registry.emplace<VelocityComponent>(entity);
        registry.emplace<SpeedComponent>(entity);
        registry.emplace<DirectionMovement>(entity);
        registry.emplace<CharacterComponent>(entity);
        registry.emplace<PairComponent>(entity);
        registry.emplace<MovementComponent>(entity, MovementComponent{ 0 });
        registry.emplace<TerrainComponent>(entity);

        return entity;
    }

    inline entt::entity spawn_camera(entt::registry &registry, uint32_t offset, const vec3 &local_pos, float radius = 0.5)
    {
        const auto entity = registry.create();

        assert(glm::all(glm::lessThanEqual(glm::abs(local_pos), vec3{ half_chunk_size, 256, half_chunk_size })));

        registry.emplace<MortonPrimitive>(entity);
        registry.emplace<Offset2>(entity, offset);

        auto &ray = registry.emplace<Ray>(entity);
        ray.m_length = radius;

        registry.emplace<AABB>(entity);

        registry.emplace<CharacterComponent>(entity);
        registry.emplace<PairComponent>(entity);
        registry.emplace<DynamicTerrainComponent>(entity, DynamicTerrainComponent{});

        return entity;
    }

    inline entt::entity spawn_capsule(entt::registry &registry, uint32_t offset, const vec3 &local_pos, float radius = 0.5,
            float halfHeight = 1.0)
    {
        const auto entity = registry.create();

        //std::cout << "Capsule " << entt::to_integral(entity) << " = " << glm::to_string(position) << std::endl;

        //assert(glm::all(glm::epsilonEqual(to_global(local_pos, Offset2::to_index(offset.first, offset.second)), position, glm::epsilon<vec3::value_type>())) );
        assert(glm::all(glm::lessThanEqual(glm::abs(local_pos), vec3{ half_chunk_size, 256, half_chunk_size })));

        registry.emplace<MortonPrimitive>(entity);

        registry.emplace<Offset2>(entity, offset);

        const quat rotation = glm::identity<quat>();
        CapsuleCollider capsule{ local_pos, rotation, radius, halfHeight - radius };

        registry.emplace<CapsuleCollider>(entity, capsule);
        registry.emplace<AABB>(entity, capsule.getAABB());

        registry.emplace<VelocityComponent>(entity);
        registry.emplace<SpeedComponent>(entity);
        registry.emplace<DirectionMovement>(entity);
        registry.emplace<Direction>(entity);
        registry.emplace<CharacterComponent>(entity);
        registry.emplace<PairComponent>(entity);
        registry.emplace<MovementComponent>(entity, MovementComponent{ 0 });
        registry.emplace<TerrainComponent>(entity);

        //registry.emplace<id_component>(entity, registry.view<id_component>().size());
        //registry.emplace<TickNumber>(entity);

        return entity;
    }

    inline entt::entity spawn_sphere(entt::registry &registry, uint32_t offset, const vec3 &local_pos, float radius = 1.0)
    {
        const auto entity = registry.create();

        //std::cout << "Capsule " << entt::to_integral(entity) << " = " << glm::to_string(position) << std::endl;

        //assert(glm::all(glm::epsilonEqual(to_global(local_pos, Offset2::to_index(offset.first, offset.second)), position, glm::epsilon<vec3::value_type>())) );
        assert(glm::all(glm::lessThanEqual(glm::abs(local_pos), vec3{ half_chunk_size, 256, half_chunk_size })));

        registry.emplace<MortonPrimitive>(entity);

        registry.emplace<Offset2>(entity, offset);

        const quat rotation = glm::identity<quat>();
        SphereCollider sphere{ local_pos, rotation, radius };

        registry.emplace<StaticComponent>(entity);
        registry.emplace<StaticCollider>(entity, sphere);

        registry.emplace<AABB>(entity, sphere.getAABB());

        //registry.emplace<id_component>(entity, registry.view<id_component>().size());
        //registry.emplace<TickNumber>(entity);

        return entity;
    }

    inline entt::entity spawn_static_box(entt::registry &registry, uint32_t offset, const vec3 &local_pos, const quat &rotation, const vec3 &extent)
    {
        const auto entity = registry.create();

        //std::cout << "Box " << entt::to_integral(entity) << " = " << glm::to_string(global_pos) << std::endl;

        assert(glm::all(glm::lessThanEqual(glm::abs(local_pos), vec3{ half_chunk_size, 256, half_chunk_size })));

        const BoxCollider box { local_pos, rotation, extent };

        registry.emplace<StaticComponent>(entity);
        registry.emplace<StaticCollider>(entity, box);
        registry.emplace<AABB>(entity, box.getAABB());

        const auto [offset_x, offset_z] = Offset2::decode(offset);

        assert(Offset2::is_valid(offset_x, offset_z));

        registry.emplace<MortonPrimitive>(entity);

        registry.emplace<Offset2>(entity, offset);

        return entity;
    }

    inline entt::entity spawn_platform(entt::registry &registry, uint32_t offset, const vec3 &local_pos, const quat &rotation, const vec3 &extent)
    {
        auto entity = registry.create();

        assert(glm::all(glm::lessThanEqual(glm::abs(local_pos), vec3{ half_chunk_size })));

        const BoxCollider box{ local_pos, rotation, extent };

        registry.emplace<StaticCollider>(entity, box);
        registry.emplace<StaticComponent>(entity);
        registry.emplace<PlatformComponent>(entity);
        registry.emplace<AABB>(entity, box.getAABB());
        registry.emplace<MortonPrimitive>(entity);
        registry.emplace<VelocityComponent>(entity);
        registry.emplace<SpeedComponent>(entity);
        registry.emplace<DirectionMovement>(entity);

        {
            auto [offset_x, offset_z] = Offset2::decode(offset);

            assert(Offset2::is_valid(offset_x, offset_z));

            registry.emplace<Offset2>(entity, offset);
        }

        return entity;
    }

    inline entt::entity spawn_box(entt::registry &registry, const vec3 &position, const quat &rotation, const vec3 &extent)
    {
        const auto entity = registry.create();

        BoxCollider box{ position, rotation, extent };

        registry.emplace<StaticComponent>(entity);
        registry.emplace<StaticCollider>(entity, box);
        registry.emplace<AABB>(entity, box.getAABB());

        registry.emplace<MortonPrimitive>(entity);

        return entity;
    }

} // namespace WFL
