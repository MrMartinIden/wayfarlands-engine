// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "CheckCapsuleOnPlatform.hpp"
#include "MovementSystem.h"
#include "PhysicsSystem.h"
#include "UpdateAabbSystem.hpp"

#include "systems/UpdatePosition.hpp"

#include "utility.hpp"

#include <common.hpp>

namespace WFL
{

    using tuple = std::tuple<MovementSystem,
            UpdateAabbSystemMt,
            CheckCapsuleOnPlatformMt,
            PhysicsSystem>;

    struct Client
    {

        Client(entt::registry &reg) :
                m_client(create_network_client()), m_systems({ MovementSystem{}, UpdateAabbSystemMt{}, CheckCapsuleOnPlatformMt{}, PhysicsSystem{ reg } })
        {
        }

        ~Client()
        {
            m_client.Disconnect();
        }

        static yojimbo::Client create_network_client();

        static bool *initialize();

        static void shutdown(bool *);

        void connect();

        void ProcessMessages(entt::registry &reg);

        void send_to_server(entt::registry &reg)
        {
            if (m_client.GetClientId() != 0 && is_spawned())
            {
                auto &client = m_client;

                if (!client.CanSendMessage(yojimbo::UNRELIABLE_UNORDERED_CHANNEL))
                {
                    return;
                }

                if (auto direction_view = reg.view<WFL::DirectionMovement, WFL::StoppedAfterMovement, WFL::CapsuleCollider>(); direction_view.size_hint() == 1)
                {
                    auto ent = direction_view.front();
                    const auto &dir = direction_view.get<WFL::DirectionMovement>(ent);
                    const auto &stopped_after_mov = direction_view.get<WFL::StoppedAfterMovement>(ent);
                    const auto &capsule = direction_view.get<WFL::CapsuleCollider>(ent);

                    //if (dir.x != 0 || dir.z != 0)
                    {
                        if (yojimbo::MessageInputCommand *unreliable_message = (yojimbo::MessageInputCommand *)client.CreateMessage(yojimbo::MESSAGE_INPUT);
                                unreliable_message != nullptr)
                        {
                            unreliable_message->m_value = { dir.x, dir.z };
                            const auto &v = unreliable_message->m_value;

                            if constexpr (MSG_INP_CMD_DEBUG)
                            {
                                static auto sequence = 0ul;
                                unreliable_message->m_sequence = sequence;

                                printf("Sending input = %f, %f, %lu\n", v[0], v[1],
                                        unreliable_message->m_sequence);

                                ++sequence;
                            }
                            else
                            {
                                printf("Sending direction = %f, %f\n", v[0], v[1]);
                            }

                            client.SendMessage(yojimbo::UNRELIABLE_UNORDERED_CHANNEL, unreliable_message);
                        }
                    }

                    {
                        if (stopped_after_mov.m_value)
                        {
                            if (auto *unreliable_message = (yojimbo::MessagePositionAfterStop *)client.CreateMessage(yojimbo::MESSAGE_POSITION_AFTER_STOP);
                                    unreliable_message != nullptr)
                            {
                                unreliable_message->m_position = { capsule.m_position.x, capsule.m_position.y, capsule.m_position.z };

                                client.SendMessage(yojimbo::UNRELIABLE_UNORDERED_CHANNEL, unreliable_message);
                            }
                        }
                    }
                }
            }
        }

        void update(entt::registry &reg, float dt)
        {
            if (m_client.GetClientId() != 0)
            {
                m_client.SendPackets();
                // update client

                m_client.ReceivePackets();

                if (is_connected())
                {
                    ProcessMessages(reg);
                }
            }

            std::apply([&](auto &&...args) { (args.update(reg, dt), ...); }, m_systems);

            if (m_client.GetClientId() != 0)
            {
                m_client.AdvanceTime(m_client.GetTime() + dt);
            }
        }

        bool is_spawned()
        {
            return (is_connected() && m_spawned);
        }

        bool is_connected()
        {
            return m_client.IsConnected();
        }

        PhysicsSystem &get_physics_system()
        {
            return std::get<PhysicsSystem>(m_systems);
        }

        yojimbo::Client m_client;
        tuple m_systems;
        double m_time;

        bool m_spawned = false;

        static std::unique_ptr<bool, decltype(&Client::shutdown)> m_initialized;
    };

} // namespace WFL
