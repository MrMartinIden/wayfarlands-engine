// Wayfarlands Engine - MMORPG Framework
// Copyright (c) 2019-2020 Anton Zavadsky
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "Client.h"

namespace WFL {

    std::unique_ptr<bool, decltype(&Client::shutdown)> Client::m_initialized{
        Client::initialize(), &Client::shutdown
    };

    yojimbo::Client Client::create_network_client()
    {
        auto config = yojimbo::get_config();

        yojimbo::Address address{ "0.0.0.0" };

        return yojimbo::Client{ yojimbo::GetDefaultAllocator(), address, config, game_adapter, 0.0 };
    }

    bool *Client::initialize()
    {
        if (!InitializeYojimbo())
        {
            printf("error: failed to initialize Yojimbo!\n");
            assert(0);
        }

        yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);

        srand((unsigned int)time(nullptr));

        return nullptr;
    }

    void Client::shutdown(bool *)
    {
        ShutdownYojimbo();
    }

    void Client::connect()
    {
        static const yojimbo::Address serverAddress("127.0.0.1", ServerPort);

        static constexpr uint8_t privateKey[yojimbo::KeyBytes]{};

        uint64_t clientId;
        yojimbo::random_bytes((uint8_t *)&clientId, 8);
        m_client.InsecureConnect(privateKey, clientId, serverAddress);
    }

    void Client::ProcessMessages(entt::registry &reg)
    {

#if 1
        auto &client = m_client;
        {
            if (!client.CanSendMessage(yojimbo::RELIABLE_ORDERED_CHANNEL))
            {
                return;
            }

            while (true)
            {
                // Receive UNRELIABLE messages
                yojimbo::Message *reliable_message = client.ReceiveMessage(yojimbo::RELIABLE_ORDERED_CHANNEL);

                if (reliable_message == nullptr)
                {
                    break;
                }

                //printf("Received reliable message\n");
                switch (reliable_message->GetType())
                {
                    case yojimbo::TEST_MESSAGE_CONNECTED:
                    {
                        yojimbo::TestConnectedMessage *testMessage = dynamic_cast<decltype(testMessage)>(reliable_message);
                        printf("Received spawn position %f, %f, %f\n",
                                testMessage->m_position[0],
                                testMessage->m_position[1],
                                testMessage->m_position[2]);

                        auto capsule_view = reg.view<CapsuleCollider>();
                        if (capsule_view.size() == 1)
                        {
                            auto entity = capsule_view.front();
                            auto &capsule = reg.get<CapsuleCollider>(entity);

                            capsule.m_position = { testMessage->m_position[0],
                                testMessage->m_position[1],
                                testMessage->m_position[2] };

                            auto &vel = reg.get<VelocityComponent>(entity);
                            auto &speed = reg.get<SpeedComponent>(entity);

                            speed = { 0, 0, 0 };
                            vel = { 0, 0, 0 };

                            m_spawned = true;
                        }
                    }
                    break;
                }
                client.ReleaseMessage(reliable_message);
            }
        }

        if (!client.CanSendMessage(yojimbo::UNRELIABLE_UNORDERED_CHANNEL))
        {
            return;
        }

        while (true)
        {
            // Receive UNRELIABLE messages
            yojimbo::Message *unreliable_message = client.ReceiveMessage(yojimbo::UNRELIABLE_UNORDERED_CHANNEL);

            if (unreliable_message == nullptr)
            {
                break;
            }

            //printf("Received unreliable message\n");
            switch (unreliable_message->GetType())
            {

#if ENABLE_SENDING_GHOST
                case yojimbo::MESSAGE_GHOST_POSITION:
                {
                    auto *testMessage = (yojimbo::MessageGhostPosition *)unreliable_message;
                    printf("Received ghost position %f, %f, %f\n",
                            testMessage->m_position[0],
                            testMessage->m_position[1],
                            testMessage->m_position[2]);

                    auto capsule_view = reg.view<CapsuleCollider>();
                    if (capsule_view.size() == 1)
                    {
                        auto entity = capsule_view.front();

                        reg.emplace_or_replace<GhostPosition>(entity, vec3{ testMessage->m_position[0],
                                                                              testMessage->m_position[1],
                                                                              testMessage->m_position[2] });
                    }
                    break;
                }
#endif
                case yojimbo::MESSAGE_POSITION_PLAYERS:
                {
                    auto *message = (yojimbo::PositionPlayersMessage *)unreliable_message;

                    SteamID steam_id{ message->m_id };

                    PlayerPosition players_pos = vec3{ message->m_position[0], message->m_position[1], message->m_position[2] };

#if 1
                    printf("Received player position %f, %f, %f\n",
                            players_pos[0],
                            players_pos[1],
                            players_pos[2]);
#endif

                    auto id_view = reg.view<SteamID>();

                    auto find_ent = std::find_if(id_view.begin(), id_view.end(), [&](auto ent) {
                        return (steam_id.m_id == id_view.get<SteamID>(ent).m_id);
                    });

                    if (find_ent != id_view.end())
                    {
                        reg.emplace_or_replace<SteamID>(*find_ent, steam_id);
                        reg.emplace_or_replace<PlayerPosition>(*find_ent, players_pos);
                    }
                    else
                    {
                        auto ent = reg.create();
                        reg.emplace<SteamID>(ent, steam_id);
                        reg.emplace<PlayerPosition>(ent, players_pos);
                    }

                    break;
                }
                default:
                    break;
            }

            client.ReleaseMessage(unreliable_message);
        }

#endif
    }
}
