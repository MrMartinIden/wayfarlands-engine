#pragma once

#include <systems/system.hpp>

namespace WFL
{
    namespace client
    {
        struct UpdatePosition final : BaseSystem<UpdatePosition>
        {
            void update([[maybe_unused]]entt::registry &reg, [[maybe_unused]] float dt = 0)
            {
            }
        };
    } // namespace client
} // namespace WFL
