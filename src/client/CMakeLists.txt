project(client)

add_library(client Client.cpp)
#target_compile_definitions(client PRIVATE -DCLIENT)
target_include_directories(client PUBLIC "."
    "systems/")

target_link_libraries(client PUBLIC
    WFL::MovementSystem
    WFL::PhysicsSystem
    WFL::UpdateAabbSystem
    WFL::CheckCapsuleOnPlatform
    WFL::Common
    yojimbo)
add_library(WFL::Client ALIAS client)
