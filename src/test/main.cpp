#include <PhysicsSystem/BroadPhase/BroadPhaseSystem.h>

#include <generator/generator.h>

int main()
{
    entt::registry registry;

    constexpr auto count = 150'000;

    WFL::generate_entities<true>(count, registry);

    WFL::BroadPhaseMt bp{ registry };

    bp.update(registry);

    return 0;
}
