# <h1 color="#ff2121"> Unfortunately the project is dead. If you are interested in such a tool and its development, write to @WillGodwin in telegram</h1>

<p align="center">
	<img alt="Veloren logo on a screenshot" src="https://sun9-45.userapi.com/impg/ILbeXcDVJogZzQ1A6uFJkMzLLH_Jay1ndH4U4w/r4c25vx_V-w.jpg?size=2048x640&quality=96&sign=db08a25373a82f35375b8835af3ebb85&type=album">
</p>

# What is Wayfarlands Engine?

Our team target is creating maximum performance and flexibility free-source MMORPG engine, based on DOD paradigm. 

In our opinion, the classical object-oriented paradigm is not suitable for creating highly loaded systems and multi-threaded data processing.

In the context of MMO games, this is a critical problem, and this is why:

Inheritance and polymorphism were created for the extensibility of your program. Almost all good developers know and understand these features and use them to create new objects based on existing ones. This makes it possible to create new mechanics, simply changing old ones, and create, for example, new objects based on existing objects.

But no matter how strange it sounds, inheritance kills extensibility - complex hierarchies cannot increase their complexity indefinitely for many reasons, just because it becomes very difficult to work with such code base. In addition, due to virtual methods and cache misses, performance is greatly reduced.

Adding new features will become more difficult with the growth of the code base. This will happen because often new features require rewriting and / or cardinal changes in the existing code. Thus, hierarchies of classes / objects require an increase in dependencies and inheritances, or require the creation of some new complex hierarchy. Again, this increases the complexity of the entire system and the load on code execution.

Multithreaded processing with complete utilization of CPU time is an absolutely necessary feature in the age of multi-core CPUs with 64+ cores. But OOD either makes it almost impossible to parallelize most tasks in the game, or makes such multithreading too expensive.

All of the above pushed us to create this project.
If you also need such a tool, then think about supporting us, please!
____
Wayfarlands is our forever MMO project. We plan to keep growing, expanding, and improving it with the support of the community. The development of the game should give us a lot of resources and opportunities for the development of the engine.

___
This project has a one simple target - maximum performance and flexibility MMORPG engine.

**Maximum flexibility**

Modular structure for easy use and modification.
No incapsulating, complex class structures etc.

**Maximum performance**

Full data orientated design of server part:
- [ ] Physics
- [ ] Artificial intelligence
- [ ] Game logic
- [ ] Network

# Dependencies
- [EnTT](https://github.com/skypjack/entt) is a header-only, tiny and easy to use library for game programming and much more written in modern C++, mainly known for its innovative entity-component-system (ECS) model. This library is the core of the engine.
- [glm](https://github.com/g-truc/glm) is a header only C++ mathematics library for graphics software based on the OpenGL Shading Language (GLSL) specifications.
- [libmorton](https://github.com/Forceflow/libmorton) is a C++11 header-only library with methods to efficiently encode/decode 64, 32 and 16-bit Morton codes and coordinates, in 2D and 3D.
- [tbb](https://github.com/01org/tbb.git) is a widely used C++ library for shared memory parallel programming and heterogeneous computing (intra-node distributed memory programming).
- [cpp-sort](https://github.com/Morwenn/cpp-sort.git) is a generic C++14 header-only sorting library.
- [googlebenchmark](https://github.com/google/benchmark.git) is a microbenchmark support library.
- [GTest](https://github.com/google/googletest.git) is Google's framework for writing C++ tests.

# References
- [Repository](https://github.com/dbartolini/data-oriented-design) with curated list of data oriented design resources.
- [Link](https://skypjack.github.io/) to the author’s blog of the EnTT library with its Back and Forth series of articles on the transition from OOP to ECS and methods for developing an ECS library.


