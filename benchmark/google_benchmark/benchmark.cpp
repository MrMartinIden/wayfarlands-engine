#include "benchmark/benchmark.h"

#include <generator/generator.h>

#include <PhysicsSystem/PhysicsSystem.h>
#include <UpdateAabbSystem/UpdateAabbSystem.hpp>

#include <glm/gtx/string_cast.hpp>

#include <iostream>
#include <thread>

//#include "cpp-sort/sorters.h"

using namespace WFL;

using Physics = PhysicsSystem;

template <uint8_t Var>
void BM_Generator(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    for (auto _ : state)
    {
        generate_entities<Var>(count, registry);

        state.PauseTiming();
        {
            registry.clear();
        }
        state.ResumeTiming();
    }
}

void BM_PhysicsSystem_Construct(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    for (auto _ : state)
    {
        Physics phys{ registry };
    }
}

void BM_PhysicsSystem_Update(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    Physics phys{ registry };

    auto &terrCollision = phys.m_terrainCollision;
    terrCollision.m_resolution = 8192 * 2;
    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    for (auto _ : state)
    {
        phys.update(registry);

        state.PauseTiming();
        {
            registry.clear();
            generate_entities(count, registry);
            phys.m_broadPhase.prepare(registry);
        }
        state.ResumeTiming();
    }
}

void BM_BroadPhase_Construct(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    for (auto _ : state)
    {
        BroadPhaseMt bp{ registry };
    }
}

void BM_BroadPhase_Prepare(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    for (auto _ : state)
    {
        bp.prepare(registry);
        state.PauseTiming();
        {
            registry.clear();
            generate_entities(count, registry);
        }
        state.ResumeTiming();
    }
}

void BM_BroadpPase_LowerBound(benchmark::State& state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp { registry };

    [[maybe_unused]] auto clear_pairs = [](entt::registry& registry) {
        auto viewPair = registry.view<PairComponent>();
        viewPair.each([](PairComponent& pair) {
            pair.clear();
        });
    };

    auto process = [&]() -> void {
        auto func = [&](const entt::entity character_entity) {
            auto& morton_prim = bp.m_character_group.get<MortonPrimitive>(character_entity);
            auto it_find = bp.lower_bound(morton_prim);
        };

        constexpr auto is_multithreaded = true;
        if (!bp.m_static_group.empty()) {
            if constexpr (is_multithreaded) {
                tbb::parallel_for_each(bp.m_character_group.begin(), bp.m_character_group.end(), func);
            } else {
                std::for_each(bp.m_character_group.begin(), bp.m_character_group.end(), func);
            }
        }
    };

    for (auto _ : state) {
        process();

        state.PauseTiming();
        {
#if 0
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
#else
            clear_pairs(registry);
#endif
        }
        state.ResumeTiming();
    }
}

#if 0
void BM_BroadPhase_FindPair(benchmark::State& state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp { registry };

    [[maybe_unused]] auto clear_pairs = [](entt::registry& registry) {
        auto viewPair = registry.view<PairComponent>();
        viewPair.each([](PairComponent& pair) {
            pair.clear();
        });
    };

    auto process = [](entt::registry& reg) -> void {
        auto pair_view = reg.view<PairComponent>();

        auto func = [&](const entt::entity character_entity) {
            const AABB& character_aabb = m_character_group.get<AABB>(character_entity);

            [[maybe_unused]] const auto& character_morton_code = m_character_group.get<MortonPrimitive>(character_entity).m_value;
            const auto& character_offset = m_character_group.get<Offset2>(character_entity).m_value;

            auto temp_global_offset = Offset2::decode(character_offset);
            const vec2 character_global_offset = { temp_global_offset[0], temp_global_offset[1] };

            const auto character_range_global_code = compute_global_code::generate(character_global_offset,
                character_aabb.min() - m_maxOffset,
                character_aabb.max() + m_maxOffset);

            assert(character_range_global_code[0] <= character_range_global_code[1]);

            debug_character(character_entity);

            const auto& static_group = m_static_group;

            //if (!static_group.empty())
            {
                AABB global_aabb {};
                auto& pair = pair_view.get<PairComponent>(character_entity);

                auto& morton_prim = m_character_group.get<MortonPrimitive>(character_entity);
                auto it_find = lower_bound(morton_prim);

                debug_lower_bound(*it_find);

                auto find_pair = [&](auto start, auto end) -> void {
                    static_assert(std::is_same_v<decltype(start), decltype(end)>, "");

                    for (auto it = start; it != end; ++it) {
                        const entt::entity static_entity = *it;

                        const auto& static_morton = static_group.get<MortonPrimitive>(static_entity);

                        debug_static_offset(static_entity);

                        if (inRange(character_range_global_code[0], character_range_global_code[1], static_morton.m_value)) {
                            const auto& static_aabb = static_group.get<AABB>(static_entity);
#ifdef DEBUG_BROADPHASE
                            std::cout << '\t' << "static_aabb = " << static_aabb << '\n';
#endif

#define USE_DEFAULT_AABB 1
#if USE_DEFAULT_AABB
                            const auto& static_offset = static_group.get<Offset2>(static_entity).m_value;
                            global_aabb = to_global_aabb(static_aabb, character_offset, static_offset);
#else
                            global_aabb = static_aabb;
#endif //USE_DEFAULT_AABB

#ifdef DEBUG_BROADPHASE
                            std::cout << '\t' << "global_aabb = " << global_aabb << '\n';
#endif

#define USE_BRANCHLESS 0
#if !USE_BRANCHLESS
                            if (character_aabb.overlaps(global_aabb)) {

                                //auto &pair = pair_view.get<PairComponent>(character_entity);

                                //assert(pair.m_size < max_contacts);
                                pair.m_entities.at(pair.m_size++) = static_entity;
                            }
#else
                            {
                                auto func = [&]() -> bool {
                                    //auto &pair = pair_view.get<PairComponent>(character_entity);

                                    //assert(pair.m_size < max_contacts);
                                    //pair.m_entities.at(pair.m_size++) = static_entity;
                                    pair.m_entities[pair.m_size++] = static_entity;

                                    return false;
                                };

                                character_aabb.overlaps(global_aabb) && func();
                            }
#endif //USE_BRANCHLESS
                        } else {
                            break;
                        }
                    }
                };

                find_pair(std::make_reverse_iterator(it_find),
                    std::make_reverse_iterator(static_group.begin()));

                find_pair(it_find, static_group.end());
            }
        };

        if (!m_static_group.empty()) {
            if constexpr (is_multithreaded) {
                std::for_each(std::execution::par, m_character_group.begin(), m_character_group.end(), func);
            } else {
                std::for_each(std::execution::seq, m_character_group.begin(), m_character_group.end(), func);
            }
        }
    };

    for (auto _ : state) {
        process(registry);

        state.PauseTiming();
        {
#if 0
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
#else
            clear_pairs(registry);
#endif
        }
        state.ResumeTiming();
    }
}

#endif

template <uint8_t Var>
void BM_BroadPhase_Update(benchmark::State& state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    [[maybe_unused]] auto clear_pairs = [](entt::registry &registry) {
        auto viewPair = registry.view<PairComponent>();
        viewPair.each([](PairComponent &pair) {
            pair.clear();
        });
    };

    for (auto _ : state) {
        bp.update<Var>(registry);

        state.PauseTiming();
        {
#if 0
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
#else
            clear_pairs(registry);
#endif
        }
        state.ResumeTiming();
    }
}

void BM_BroadPhase_Compute_morton_codes_dynamic(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    for (auto _ : state)
    {
        bp.compute_codes_character();

#if 0
        state.PauseTiming();
        {
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
        }
        state.ResumeTiming();
#endif
    }
}

void BM_BroadPhase_InsertionSort(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    for (auto _ : state)
    {
        bp.sort_static<entt::insertion_sort>();
        bp.sort_dynamic<entt::insertion_sort>();
    }
}

void BM_BroadPhase_Insertionsort_dynamic(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    for (auto _ : state)
    {
        bp.sort_dynamic<entt::insertion_sort>();
        state.PauseTiming();
        {
            bp.prepare(registry);
        }
        state.ResumeTiming();
    }
}

/*
void BM_BroadPhase_InsertionSort1(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.sort<cppsort::detail::insertion_sorter_impl>();
    }
}

void BM_BroadPhase_TimSort(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(static_cast<size_t>(count), registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.sort<cppsort::detail::tim_sorter_impl>();
    }
}

void BM_BroadPhase_PdqSort(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.sort<cppsort::detail::pdq_sorter_impl>();
    }
}

void BM_BroadPhase_VergeSort(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.sort<cppsort::detail::verge_sorter_impl>();
    }
}
*/

void BM_BroadPhase_StdSort_MultiThread(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    for (auto _ : state)
    {
        bp.sort_dynamic();
    }
}

void BM_BroadPhase_StdSort_SingleThread(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    constexpr bool is_multithreaded = false;

    WFL::BroadPhaseSystem<is_multithreaded> bp{ registry };

    for (auto _ : state)
    {
        bp.sort_dynamic();
    }
}

void BM_BroadPhase_ComputeRangeCode(benchmark::State& state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp { registry };

    [[maybe_unused]] auto clear_pairs = [](entt::registry& registry) {
        auto viewPair = registry.view<PairComponent>();
        viewPair.each([](PairComponent& pair) {
            pair.clear();
        });
    };

    auto process = [&]([[maybe_unused]] entt::registry& reg) {
        auto pair_view = reg.view<PairComponent>();

        auto func = [&](const entt::entity character_entity) {
            const AABB& character_aabb = bp.m_character_group.get<AABB>(character_entity);

            [[maybe_unused]] const auto& character_morton_code = bp.m_character_group.get<MortonPrimitive>(character_entity).m_value;
            const auto& character_offset = bp.m_character_group.get<Offset2>(character_entity).m_value;

            auto temp_global_offset = Offset2::decode(character_offset);
            const vec2 character_global_offset = { temp_global_offset[0], temp_global_offset[1] };

            const auto character_range_global_code = compute_global_code::generate(character_global_offset,
                character_aabb.min() - bp.m_maxOffset,
                character_aabb.max() + bp.m_maxOffset);
        };

        constexpr auto is_multithreaded = 1;

        if (!bp.m_static_group.empty()) {
            if constexpr (is_multithreaded) {
                tbb::parallel_for_each(bp.m_character_group.begin(), bp.m_character_group.end(), func);
            } else {
                std::for_each(bp.m_character_group.begin(), bp.m_character_group.end(), func);
            }
        }
    };

    for (auto _ : state) {
        process(registry);
        state.PauseTiming();
        {
#if 0
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
#else
            clear_pairs(registry);
#endif
        }
        state.ResumeTiming();
    }
}

void BM_BroadPhase_Process(benchmark::State& state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    [[maybe_unused]] auto clear_pairs = [](entt::registry &registry) {
        auto viewPair = registry.view<PairComponent>();
        viewPair.each([](PairComponent &pair) {
            pair.clear();
        });
    };

    for (auto _ : state)
    {
        bp.process(registry);
        state.PauseTiming();
        {
#if 0
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);
#else
            clear_pairs(registry);
#endif
        }
        state.ResumeTiming();
    }
}

void BM_TerrainCollision_Update(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    TerrainCollisionMt terrCollision{ registry };
    terrCollision.m_resolution = 8192 * 2;

    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    for (auto _ : state)
    {
        terrCollision.update(registry);
    }
}

void BM_NarrowPhase_Update(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry);

    BroadPhaseMt bp{ registry };

    TerrainCollisionMt terrCollision{ registry };
    terrCollision.m_resolution = 8192;
    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 2.f);

    NarrowPhaseMt np{ registry };

    bp.update(registry);
    terrCollision.update(registry);

    /*
    auto countPair = 0ul;

    auto viewPair = registry.view<PairComponent>();
    viewPair.each([&countPair](PairComponent &pair) {
        if (pair.m_entities.empty())
        {
            return;
        }

        countPair += pair.m_entities.size();
    });

    std::cout << "Count of pair: " << countPair << " for total entities = " << count << std::endl;
    */

    for (auto _ : state)
    {
        np.update(registry);

        state.PauseTiming();
        {
            registry.clear();
            generate_entities(count, registry);
            bp.prepare(registry);

            bp.update(registry);
            terrCollision.update(registry);
        }
        state.ResumeTiming();
    }
}

int main(int argc, char **argv)
{
    std::cout << "Number of concurrent threads = " << std::thread::hardware_concurrency() << std::endl;

    if (argc == 2)
    {
        max_character = atoi(argv[1]);
    }

    std::cout << "Max characters = " << max_character << std::endl;

    constexpr size_t min = 1ul << 17;

#define TO_STRING(a) \
    std::string(#a)

    auto registerBench = [](std::string_view name, auto func) {
        benchmark::RegisterBenchmark(name.data(), func)
            ->Arg(min)
            ->Arg(min << 1)
            ->Arg(min << 2)
            ->Arg(min << 3)
            ->Arg(min << 4)
            ->Arg(min << 5)
            ->Arg(min << 6)
            //->UseRealTime()
            ->Unit(benchmark::kMillisecond);
    };

    //registerBench(TO_STRING(BM_Generator<0>), BM_Generator<0>);
    //registerBench(TO_STRING(BM_Generator<1>), BM_Generator<1>);

    //registerBench(TO_STRING(BM_PhysicsSystem_Construct), BM_PhysicsSystem_Construct);

    //registerBench(TO_STRING(BM_PhysicsSystem_Update), BM_PhysicsSystem_Update);

    //registerBench(TO_STRING(BM_BroadPhase_Prepare), BM_BroadPhase_Prepare);

    //registerBench(TO_STRING(BM_BroadPhase_Compute_morton_codes_dynamic), BM_BroadPhase_Compute_morton_codes_dynamic);

    //registerBench(TO_STRING(BM_BroadpPase_LowerBound), BM_BroadpPase_LowerBound);
    //registerBench(TO_STRING(BM_BroadPhase_ComputeRangeCode), BM_BroadPhase_ComputeRangeCode);

    //bench(TO_STRING(BM_BroadPhase_Process), BM_BroadPhase_Process);

    //registerBench(TO_STRING(BM_BroadPhase_Update<0>), BM_BroadPhase_Update<0>);
    registerBench(TO_STRING(BM_BroadPhase_Update<1>), BM_BroadPhase_Update<1>);

    //registerBench(TO_STRING(BM_BroadPhase_InsertionSort), BM_BroadPhase_InsertionSort);
    //registerBench(TO_STRING(BM_BroadPhase_Insertionsort_dynamic), BM_BroadPhase_Insertionsort_dynamic);

    /*
    REGISTER_BENCH(BM_BroadPhase_InsertionSort1);
    REGISTER_BENCH(BM_BroadPhase_TimSort);
    REGISTER_BENCH(BM_BroadPhase_PdqSort);
    REGISTER_BENCH(BM_BroadPhase_VergeSort);
    */

    /*
    REGISTER_BENCH(BM_BroadPhase_StdSort_MultiThread);
    REGISTER_BENCH(BM_BroadPhase_StdSort_SingleThread);

    
    */

    //registerBench(TO_STRING(BM_TerrainCollision_Update), BM_TerrainCollision_Update);

    //registerBench(TO_STRING(BM_NarrowPhase_Update), BM_NarrowPhase_Update);

    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();

#undef TO_STRING
}
