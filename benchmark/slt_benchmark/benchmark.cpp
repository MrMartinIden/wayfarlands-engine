#include "sltbench/Bench.h"

#include <generator/generator.h>

#include <glm/gtx/string_cast.hpp>
#include <iostream>

#include "cpp-sort/sorters.h"

#include <PhysicsSystem/PhysicsSystem.h>

using namespace WFL;

using Physics = PhysicsSystem;

static const std::vector<size_t> my_args = { 1u << 17, 1u << 18, 1u << 19, 1u << 20, 1u << 21 };

using tuple = std::tuple<BroadPhaseMt, entt::registry>;
using tuple1 = std::tuple<NarrowPhaseMt, entt::registry>;

#if 0
tuple make_my_fixture(const size_t &arg)
{
    entt::registry registry;

    generate_entities(arg, registry);

    BroadPhase bp{ registry, global_aabb };

    return std::make_tuple(bp, std::move(registry));
}

void BM_BroadPhase_Update(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    auto &registry = std::get<1>(fix);

    bp.update(registry);
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_Update, make_my_fixture, my_args)
#endif

tuple1 make_my_fixture1(const size_t &arg)
{
    entt::registry registry;

    generate_entities(arg, registry);

    BroadPhaseMt bp{ registry };

    TerrainCollisionMt terrCollision{ registry };
    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    NarrowPhaseMt np { registry };

    bp.update(registry);
    terrCollision.update(registry);

    return std::make_tuple(np, std::move(registry));
}

void BM_NarrowPhase_Update(tuple1 &fix, const size_t &)
{
    auto &np = std::get<0>(fix);
    auto &registry = std::get<1>(fix);

    np.update(registry);
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_NarrowPhase_Update, make_my_fixture1, my_args)

/*
void BM_BroadPhase_InsertionSort_0(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    bp.sort<entt::insertion_sort>();
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_InsertionSort_0, make_my_fixture, my_args)

void BM_BroadPhase_InsertionSort_1(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    bp.sort<cppsort::detail::insertion_sorter_impl>();
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_InsertionSort_1, make_my_fixture, my_args)

void BM_BroadPhase_TimSort(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    bp.sort<cppsort::detail::tim_sorter_impl>();
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_TimSort, make_my_fixture, my_args)

void BM_BroadPhase_PdqSort(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    bp.sort<cppsort::detail::pdq_sorter_impl>();
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_PdqSort, make_my_fixture, my_args)

void BM_BroadPhase_VergeSort(tuple &fix, const size_t &)
{
    auto &bp = std::get<0>(fix);
    bp.sort<cppsort::detail::verge_sorter_impl>();
}
SLTBENCH_FUNCTION_WITH_FIXTURE_BUILDER_AND_ARGS(BM_BroadPhase_VergeSort, make_my_fixture, my_args)
*/

#if 0
void BM_BroadPhase_StdSort_MultiThread(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.sort();
    }
}

void BM_BroadPhase_StdSort_SingleThread(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhaseSystem<global_aabb, NumThreads::SingleThread> bp{ registry };

    for (auto _ : state)
    {
        bp.sort();
    }
}

void BM_BroadPhase_CalculateMortonCodes(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.calculateMortonCodes();
    }
}

void BM_BroadPhase_CalculateMortonCodes_For_Characters(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    for (auto _ : state)
    {
        bp.calculateMortonCodesForChar();
    }
}

template <uint8_t I>
void BM_BroadPhase_FindPairs(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    BroadPhase bp{ registry };

    glm::vec3 maxOffset{0};
    maxOffset = bp.calculateMaxOffset();

    for (auto _ : state)
    {
        //bp.process<I>(maxOffset);
        bp.process<I>(registry, maxOffset);
    }
}

void BM_TerrainCollision_Update(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    TerrainCollisionSystem terrCollision{ registry };
    terrCollision.m_resolution = 8192 * 2;
    terrCollision.m_position = glm::vec3{ 0 };

    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);

    for (auto _ : state)
    {
        terrCollision.update();
    }
}

void BM_NarrowPhase_Update(benchmark::State &state)
{
    size_t count = state.range(0);

    entt::registry registry;

    generate_entities(count, registry, 0);

    Physics phys{ registry };

    auto &bp = phys.m_broadPhase;
    bp.update(registry);

    auto &terrCollision = phys.m_terrainCollision;
    terrCollision.m_resolution = 8192 * 2;
    terrCollision.m_position = glm::vec3{ 0 };
    auto &height_map = terrCollision.m_height_map;
    height_map.resize(terrCollision.m_resolution * terrCollision.m_resolution, 4.f);
    terrCollision.update();

    auto countPair = 0u;

    /*
    bp.m_groupPair.each([&countPair](Pair &pair, auto) {
        if (pair.m_entities.empty())
        {
            return;
        }

        countPair += pair.m_entities.size();
    });
    */

    std::cout << "Count of pair: " << countPair << " for total entities = " << count << std::endl;

    auto &np = phys.m_narrowPhase;

    for (auto _ : state)
    {
        np.update(registry);
    }
}

int main(int argc, char **argv)
{
    if (argc == 2)
    {
        max_character = atoi(argv[1]);
    }

    std::cout << "Max characters = " << max_character << std::endl;

    constexpr auto min = 1u << 17;
    constexpr auto max = 1u << 21;

#define REGISTER_BENCH(func)                   \
    benchmark::RegisterBenchmark(#func, &func) \
            ->RangeMultiplier(2)               \
            ->Range(min, max)                  \
            ->Unit(benchmark::kMillisecond)

    /*
    benchmark::RegisterBenchmark("bench0", &bench<0>)
            ->Unit(benchmark::kMicrosecond);

    benchmark::RegisterBenchmark("bench1", &bench<1>)
            ->Unit(benchmark::kMicrosecond);
    */

    REGISTER_BENCH(BM_PhysicsSystem_Construct);

    REGISTER_BENCH(BM_PhysicsSystem_Update);

    REGISTER_BENCH(BM_BroadPhase_Update);

    REGISTER_BENCH(BM_BroadPhase_InsertionSort);

    REGISTER_BENCH(BM_BroadPhase_StdSort_SingleThread);
    REGISTER_BENCH(BM_BroadPhase_StdSort_MultiThread);

    REGISTER_BENCH(BM_BroadPhase_CalculateMortonCodes);

    REGISTER_BENCH(BM_BroadPhase_CalculateMortonCodes_For_Characters);

    REGISTER_BENCH(BM_BroadPhase_FindPairs<0>);
    REGISTER_BENCH(BM_BroadPhase_FindPairs<1>);
    

    REGISTER_BENCH(BM_TerrainCollision_Update);

    REGISTER_BENCH(BM_NarrowPhase_Update);

    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();

#undef REGISTER_BENCH
}

#endif

int main(int argc, char **argv)
{
    return ::sltbench::Main(argc, argv);
}
